//
//  NotificationCenterManager.swift
//  Driver
//
//  Created by Pallab on 24/04/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let didReceiveRideRequest = Notification.Name("didReceiveRideRequest")
    static let didBidAcceptedByPassenger = Notification.Name("bidAcceptedByPassenger")
}

@objc public protocol NotificationCenterSubscribable {
    
    @objc optional func onDidReceiveRideRequest(_ notification: Notification)
    
    @objc optional func onDidReceiveBidAcceptedByPassenger(_ notification: Notification)
    
}

public class NotificationCenterManager: NSObject {
    
    public static let shared = NotificationCenterManager()
    
    public func subscribeForGettingRideRequestNotification(subscriber: NotificationCenterSubscribable, notificationName: Notification.Name) {
        NotificationCenter.default.addObserver(subscriber, selector: #selector(subscriber.onDidReceiveRideRequest(_:)), name: notificationName, object: nil)
    }
    
    public func unSubscribeFromGettingRideRequestNotification(subscriber: NotificationCenterSubscribable, notificationName: Notification.Name) {
        NotificationCenter.default.removeObserver(subscriber, name: notificationName, object: nil)
    }
    
    public func subscribeForGettingBidAcceptedNotification(subscriber: NotificationCenterSubscribable, notificationName: Notification.Name) {
        NotificationCenter.default.addObserver(subscriber, selector: #selector(subscriber.onDidReceiveBidAcceptedByPassenger(_:)), name: notificationName, object: nil)
    }
    
    public func unSubscribeFromGettingBidAcceptedNotification(subscriber: NotificationCenterSubscribable, notificationName: Notification.Name) {
        NotificationCenter.default.removeObserver(subscriber, name: notificationName, object: nil)
    }
    
    /*
    public func postNotificationRideRequestReceived(rideRequest: RideRequest) {
        Utility.log("postNotificationRideRequestReceived called")
        NotificationCenter.default.post(name: .didReceiveRideRequest, object: nil, userInfo: ["rideRequest": rideRequest])
    }
    
    public func postNotificationBidAcceptedByPassenger(bidAcceptedByPassenger: RideRequest) {
        Utility.log("postNotificationBidAcceptedByPassenger called")
        NotificationCenter.default.post(name: .didBidAcceptedByPassenger, object: nil, userInfo: ["bidAcceptedByPassenger": bidAcceptedByPassenger])
    }
    */
    
}
