//
//  CustomLocationManager.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 15/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import CoreLocation

class CustomLocationManager: NSObject {
    
    public static let shared = CustomLocationManager()
    public var latitude: CLLocationDegrees = 0.0
    public var longitude: CLLocationDegrees = 0.0
    private var locationManager : CLLocationManager!
    
    override init() {
        super.init()
        self.performSelector(onMainThread: #selector(self.initLocationManager), with: nil, waitUntilDone: true)
    }
    
    @objc private func initLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters//set the updating location accuracy here
    }
    
    @objc private func deinitLocationManager() {
        self.locationManager = nil
    }
    
    deinit {
        self.performSelector(onMainThread: #selector(deinitLocationManager), with: nil, waitUntilDone: true)
    }
    
    public func requestForPermission() {
        DispatchQueue.global(qos: .background).async {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    public func getPermissionStatus() -> CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    public func startUpdatingLocation() {
        DispatchQueue.global(qos: .background).async {
            self.locationManager.startUpdatingLocation()
            self.locationManager.allowsBackgroundLocationUpdates = true
            self.locationManager.pausesLocationUpdatesAutomatically = false
        }
    }
    
    public func stopUpdatingLocation() {
        self.locationManager.stopUpdatingLocation()
    }
    
}


extension CustomLocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations.last!
        self.latitude = currentLocation.coordinate.latitude
        self.longitude = currentLocation.coordinate.longitude
        //Broadcast all over the app that the location changed
        /*
         NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: InsideAppNotificationNames.newLocationFoundInsideAppNotification.rawValue), object: nil, userInfo:
         ["updatedLocation":[
         "latitude": self.latitude,
         "longitude": self.longitude
         ]
         ])
         */
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Utility.log("Location fetching error \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            Utility.log("authorizedAlways")
        case .authorizedWhenInUse:
            Utility.log("authorizedWhenInUse")
            self.startUpdatingLocation()
        case .denied:
            Utility.log("denied")
        case .notDetermined:
            Utility.log("notDetermined")
        case .restricted:
            Utility.log("restricted")
        @unknown default:
            Utility.log("UnKnown")
        }
    }
}
