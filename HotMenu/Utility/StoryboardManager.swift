//
//  StoryboardManager.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 14/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation

import UIKit

enum AppStoryboard: String {
    
    //the names of all storyboards used in the application goes here
    case login = "Login"
    case register = "Register"
    case registration = "Registration"
    case main = "Main"
    case phoneNumber = "PhoneNumber"
    case verify = "VerificationView"
    case home = "Home"
    case rating = "Rating"
    case cart = "Cart"
    case search = "Search"
    case menu = "Menu"
    case gender = "GenderView"
    case forgotPassword = "ForgotPassword"
    case document = "Document"
    case countryPicker = "CountryPicker"
    case bidPoint = "BidPoint"
    case payment = "Payment"
    case profile = "Profile"
    case location = "Location"
    case myOrder = "MyOrder"
    
    
    private var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: .main)
    }
    
    public func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
}
