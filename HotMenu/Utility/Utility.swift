//
//  Utility.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 17/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import AVFoundation

struct Utility {
    
    public static func log(_ items: Any?) {
        #if DEBUG//log will work only for debug mode
            print(items ?? "Nil value")
        #endif
    }
    
    private static var player: AVAudioPlayer?
    
    public static func playSound() {
        if self.player == nil {
            guard let url = Bundle.main.url(forResource: "alert", withExtension: "mp3") else { return }
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                try AVAudioSession.sharedInstance().setActive(true)
                /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
                self.player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
                /* iOS 10 and earlier require the following line:
                 player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
                guard let player = player else { return }
                player.play()
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    public static func stopSound() {
        if self.player != nil {
            self.player?.stop()
            player = nil
        }
    }
    
    public static func removeNull(from string: String?) -> String? {
        var string = string
        if  (string?.count ?? 0) == 0 || (string == "<null>") || (string == "(null)") || (string == " ") || (string == "01/01/0001") || (string == "1/1/0001") || (string == "0001-01-01T00:00:00") {
            string = ""
        }
        return string
    }
    
    public static func convertToMile(meter: Double) -> Double {
        //1 Meter = 0.000621371192 Miles
        return meter * 0.000621371192
    }
    
    public static var lattitude = Double()
    public static var longitude = Double()
    public static var cartCount = Int()
}
