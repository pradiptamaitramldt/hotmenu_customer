//
//  String+Additions.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 14/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

extension String {
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.width)
    }
    
    
    func startIndexOfSubString(substring: String) -> Int? {
        if let range = self.lowercased().range(of: substring.lowercased()) {
            let startPos = self.distance(from: self.startIndex, to: range.lowerBound)
            return startPos
        } else {
            return nil
        }
    }
    
    func indexDistance(of character: Character) -> Int? {
        guard let index = firstIndex(of: character) else { return nil }
        return distance(from: startIndex, to: index)
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
//    func sha1() -> String {
//        let data = Data(self.utf8)
//        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
//        data.withUnsafeBytes {
//            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
//        }
//        let hexBytes = digest.map { String(format: "%02hhx", $0) }
//        return hexBytes.joined()
//    }
    
    func changeDateTimeStringFormat(inputTimeZone: TimeZone, outputTimeZone: TimeZone, inputFormat: String, outputFormat: String ) -> String {
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.timeZone = inputTimeZone
        inputDateFormatter.dateFormat = inputFormat
        let inputDateTime = inputDateFormatter.date(from: self)
        if inputDateTime == nil {
            return ""
        }
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.timeZone = outputTimeZone
        outputDateFormatter.dateFormat = outputFormat
        let outputDateTimeString = outputDateFormatter.string(from: inputDateTime!)
        return outputDateTimeString
    }
    
    //-----the following methods are for validation purpose-----
    func isValidEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        //Should have at least eight characters, including one uppercase character, one lowercase character, one number and one character that isn’t a letter or digit.
        // let passwordRegex = "^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=\\P{N}*\\p{N})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{8,}$"
        // return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
        
        //Should have six to eight characters, including one uppercase character, one lowercase character, one number and one character that isn’t a letter or digit.
        //"^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=\\P{N}*\\p{N})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{6,8}$"
        //let passwordRegex = "^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{6,8}$"
        //let passwordRegex = "^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{8,}$"
        //return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
        //Password should be minimum 6 characters long
        
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$" //"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$"//"^[a-zA-Z0-9\\s\\S]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    func isValidName() -> Bool {
        if self.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            //string blank
            return false
        }
        if self.rangeOfCharacter(from: .whitespaces) != nil {
            //string contains white spaces
            return false
        }
        let charset = CharacterSet(charactersIn: "1234567890~`!@#$%^&*()_=+[{}]|:;<,>?/")
        if self.rangeOfCharacter(from: charset) != nil {
            return false
        }
        return true
    }
    
    func isValidPhoneNumber() -> Bool {
        let phoneNumberRegex = "^[0-9]{6,12}$"
        //let phoneNumberRegex = "^((\\+)|(00))[0-9]{6,14}$"
        return NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex).evaluate(with: self)
    }
    
    func isValidCardNumber() -> Bool {
        let cardNumberRegex = "^[0-9]{16}$"
        return NSPredicate(format: "SELF MATCHES %@", cardNumberRegex).evaluate(with: self)
    }
    
    func isValidCardVerificationCode() -> Bool {
        let cardVerificationCodeRegex = "^[0-9]{3}$"
        return NSPredicate(format: "SELF MATCHES %@", cardVerificationCodeRegex).evaluate(with: self)
    }
    
    func isValidOTP() -> Bool {
        let otpRegex = "^[0-9]{4,}$"//4 characters OTP with only digits
        return NSPredicate(format: "SELF MATCHES %@", otpRegex).evaluate(with: self)
    }
    
    func toFloat() -> Float {
        let floatNumber = Float(self)
        return floatNumber ?? 0.0
    }
    
    func toInt() -> Int {
        let intNumber = Int(self)
        return intNumber ?? 0
    }
    
    func isValidWalletAmount() -> Bool {
        let num = Float(self)
        if num != nil {
            if num! >= 1 {
                return true
            } else {
                return false
            }
        }
        else {
            return false
        }
    }
    
    func isValidInteger() -> Bool {
        let num = Int(self)
        if num != nil {
            if num! >= 1 {
                return true
            } else {
                return false
            }
        }
        else {
            return false
        }
    }
    
    func isValidString() -> Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines) != ""
    }
    
    //-----
}

//MARK: - StringProtocol Extensions
extension StringProtocol where Index == String.Index {
    func index<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while start < endIndex, let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.lowerBound < range.upperBound ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    func ranges<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while start < endIndex, let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.lowerBound < range.upperBound  ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}

extension Collection {
    var pairs: [SubSequence] {
        var startIndex = self.startIndex
        let count = self.count
        let n = count/2 + count % 2
        return (0..<n).map { _ in
            let endIndex = index(startIndex, offsetBy: 2, limitedBy: self.endIndex) ?? self.endIndex
            defer { startIndex = endIndex }
            return self[startIndex..<endIndex]
        }
    }
}

extension Collection {
    func distance(to index: Index) -> Int { distance(from: startIndex, to: index) }
}

extension StringProtocol where Self: RangeReplaceableCollection {
    mutating func insert<S: StringProtocol>(separator: S, every n: Int) {
        for index in indices.dropFirst().reversed()
            where distance(to: index).isMultiple(of: n) {
            insert(contentsOf: separator, at: index)
        }
    }
    func inserting<S: StringProtocol>(separator: S, every n: Int) -> Self {
        var string = self
        string.insert(separator: separator, every: n)
        return string
    }
}
