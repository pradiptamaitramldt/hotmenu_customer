//
//  TimeInterval+Additions.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 15/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    func getDateTimeFromEpoch(outputTimeZone: TimeZone, outputFormat: String) -> Date {
        let inputDateTime = Date(timeIntervalSince1970: self/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = outputTimeZone
        dateFormatter.dateFormat = outputFormat
        let outputDateTimeString = dateFormatter.string(from: inputDateTime)
        let outputDateTime = dateFormatter.date(from: outputDateTimeString)
        return outputDateTime!
    }
    
    func getStringDateTimeFromEpoch(outputTimeZone: TimeZone, outputFormat: String) -> String {
        let inputDateTime = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = outputTimeZone
        dateFormatter.dateFormat = outputFormat
        let stringDate = dateFormatter.string(from: inputDateTime)
        return stringDate
    }
    
    func getStringDateTimeFromEpochAddingHours(outputTimeZone: TimeZone, outputFormat: String, addingHours: Double) -> String {
        let inputDateTime = Date(timeIntervalSince1970: self/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = outputTimeZone
        dateFormatter.dateFormat = outputFormat
        let outputDateTime = inputDateTime.addingTimeInterval(addingHours * 60 * 60)
        let stringDate = dateFormatter.string(from: outputDateTime)
        return stringDate
    }
    
    func getNumberOFDaysBetweenEpochAndCurrentDate() -> Int {
        let startDateTime = Date(timeIntervalSince1970: self/1000)
        let calendar = NSCalendar.current
        let startDate = calendar.startOfDay(for: startDateTime)
        let endDate = calendar.startOfDay(for: Date())
        let components: DateComponents = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
    
}
