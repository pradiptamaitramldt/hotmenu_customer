//
//  UIViewController+Additions.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 10/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    //this variable will identify a viewcontroller from the storyboard. the storyboard identifier will be in the form <UIViewController-subclass-name>
    class var storyboardID: String {
        return "\(self)"
    }
    
    //will intantiate a viewcontroller from the application storyboards and return the viewcontroller
    static func instantiateFromAppStoryboard(appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    public func addStatusBarView() {
        let statusBarView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 80))
        statusBarView.translatesAutoresizingMaskIntoConstraints = false
        //statusBarView.backgroundColor = Colors.statusBarColor
        statusBarView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        self.view.addSubview(statusBarView)
        self.view.sendSubviewToBack(statusBarView)
    }
    
    //show alertview with only one action
    public func showAlertWithTitleOkAction(_ title: String?, message: String?, okTitle: String, okAction: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle , style: .default) { (result : UIAlertAction) -> Void in
            okAction?()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //show alertview with ok and cancel action
    public func showAlertWithTitleOkCancelAction(_ title:String?, message:String?, okTitle: String?, cancelTitle: String?, okAction: @escaping () -> Void, cancelAction: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle ?? "OK", style: .default) { (result : UIAlertAction) -> Void in
            okAction()
        }
        let cancelAction = UIAlertAction(title: cancelTitle ?? "Cancel", style: .default) { (result : UIAlertAction) -> Void in
            cancelAction()
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func setBackBarButtonTitle(title: String = "") {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: nil)
    }
    
    //show activity indicator
    func showLoader() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    //hide activity indicator
    func hideLoader() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
