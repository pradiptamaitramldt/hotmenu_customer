//
//  UITextField+Additions.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 14/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue ?? .lightGray])
        }
    }
    
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding(_ padding: PaddingSide) {
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        switch padding {
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
    
    func addModeViewImage(_ viewMode: PaddingSide, image: UIImage) {
        switch viewMode {
        case .left(let spacing):
            let paddingView = UIImageView(frame: CGRect(x: 0, y: 0, width: spacing * 1.5, height: spacing))
            paddingView.image = image
            paddingView.contentMode = .left
            self.leftView = paddingView
            self.rightViewMode = .always
        case .right(let spacing):
            let paddingView = UIImageView(frame: CGRect(x: 0, y: 0, width: spacing * 1.5, height: spacing))
            paddingView.image = image
            self.rightView = paddingView
            paddingView.contentMode = .left
            self.rightViewMode = .always
        case .both(let spacing):
            let paddingView = UIImageView(frame: CGRect(x: 0, y: 0, width: spacing * 1.5, height: spacing))
            paddingView.image = image
            paddingView.contentMode = .left
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
    
    func addBottomBorder(borderColor: UIColor) {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = borderColor.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
    
}

/*
extension BindableTextField {
    
    @IBInspectable override var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue ?? .lightGray])
        }
    }
    
    /*
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding(_ padding: PaddingSide) {
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        switch padding {
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
    
    func addModeViewImage(_ viewMode: PaddingSide, image: UIImage) {
        switch viewMode {
        case .left(let spacing):
            let paddingView = UIImageView(frame: CGRect(x: 0, y: 0, width: spacing * 1.5, height: spacing))
            paddingView.image = image
            paddingView.contentMode = .left
            self.leftView = paddingView
            self.rightViewMode = .always
        case .right(let spacing):
            let paddingView = UIImageView(frame: CGRect(x: 0, y: 0, width: spacing * 1.5, height: spacing))
            paddingView.image = image
            self.rightView = paddingView
            paddingView.contentMode = .left
            self.rightViewMode = .always
        case .both(let spacing):
            let paddingView = UIImageView(frame: CGRect(x: 0, y: 0, width: spacing * 1.5, height: spacing))
            paddingView.image = image
            paddingView.contentMode = .left
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
    */
}
*/
