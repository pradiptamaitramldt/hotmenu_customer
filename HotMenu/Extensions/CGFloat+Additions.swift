//
//  CGFloat+Additions.swift
//  CellularBackupContacts
//
//  Created by Brainium on 04/11/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
