//
//  CountryPickerProtocols.swift
//  Global
//
//  Created by Mukesh Singh on 14/01/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
/*
protocol CountryPickerViewProtocol: ViewProtocol {
    
}

protocol CountryPickerPresenterProtocol: PresenterProtocol {
    //these methods must be implemented
    var countries: [Country] { get set }
    var countriesTemp: [Country] { get set }
    func loadCountriesJson()
    func didSelectCountry(country: Country)
    func filterCountryList(filterString: String)
}


*/
