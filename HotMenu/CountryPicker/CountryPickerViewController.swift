//
//  CountryPickerViewController.swift
//  Global
//
//  Created by Mukesh Singh on 14/01/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import UIKit

protocol CountryPickerViewControllerDelegate {
    func didSelectCountry(country: Country)
}

final class CountryPickerViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableViewCountryList: UITableView!
    @IBOutlet weak var searchBarFilter: UISearchBar!
    
    //MARK: - Other Variables
    var countries: [Country] = []
    var countriesTemp: [Country] = []
    var delegate : CountryPickerViewControllerDelegate?

    //MARK: - ViewController Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewCountryList.dataSource = self
        self.tableViewCountryList.delegate = self
        self.tableViewCountryList.backgroundColor = .white
        if #available(iOS 13.0, *) {
            self.searchBarFilter.overrideUserInterfaceStyle = .light
        }
        self.searchBarFilter.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadCountriesJson()
    }
    
    
    func loadCountriesJson() {
        if let url = Bundle.main.url(forResource: "countryCodes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data)
                self.countriesTemp = self.countries
                self.tableViewCountryList.reloadData()
            } catch {
                print("error:\(error)")
            }
        }
    }
    
    func didSelectCountry(country: Country) {
        guard let delegate = self.delegate else { return }
        delegate.didSelectCountry(country: country)
    }
    
    func filterCountryList(filterString: String) {
        self.countriesTemp = self.countries
        if filterString.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.countriesTemp = self.countriesTemp.filter {
                $0.name?.range(of: filterString, options: .caseInsensitive) != nil
            }
        }
        self.tableViewCountryList.reloadData()
        return
    }
    
    //MARK: - IBActions
    @IBAction func buttonCancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension CountryPickerViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterCountryList(filterString: searchText)
    }
}

extension CountryPickerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countriesTemp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CountryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "countryTableViewCell") as! CountryTableViewCell
        cell.labelCountryNameAndISDCode.text = self.countriesTemp[indexPath.row].name! + " (" + self.countriesTemp[indexPath.row].dialCode! + ")"
        cell.imageViewCountryFlag.image = UIImage(named: self.countriesTemp[indexPath.row].code?.lowercased() ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectCountry(country: self.countriesTemp[indexPath.row])
        //to prevent lagging in dismiss VC sometimes
        DispatchQueue.main.async {
            self.dismiss(animated: true) {
            }
        }
    }
}
