//
//  CustomView.swift
//  Passenger
//
//  Created by brainium on 12/03/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class CustomView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    private var shadowLayer: CAShapeLayer!

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func addShadowAndCornerRadius(cornerRadius: CGFloat = 5, shadowColor: UIColor = .lightGray) {
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = UIColor.white.cgColor

            shadowLayer.shadowColor = shadowColor.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 2.0, height: 2.0)
            shadowLayer.shadowOpacity = 0.5
            shadowLayer.shadowRadius = 2

            layer.insertSublayer(shadowLayer, at: 0)
            //layer.insertSublayer(shadowLayer, below: nil) // also works
        }
    }
    
    func  addCornerRadiusShadowOnTopLeftRight() {
           shadowLayer = CAShapeLayer()
           shadowLayer.bounds = self.frame
           shadowLayer.position = self.center
           shadowLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [ .topLeft,.topRight], cornerRadii: CGSize(width: 40, height: 40)).cgPath

           shadowLayer.backgroundColor = UIColor.white.cgColor
           shadowLayer.fillColor = UIColor.white.cgColor
           //Here I'm masking the textView's layer with rectShape layer
           //self.layer.mask = shadowLayer
           shadowLayer.shadowColor = UIColor.black.cgColor
           shadowLayer.shadowOffset = CGSize(width: 6.0, height: 6.0)
           shadowLayer.shadowOpacity = 0.7
           shadowLayer.shadowRadius = 4.0
           self.layer.mask = shadowLayer
    }
    
    func setDefaultBackgroundColor() {
        self.backgroundColor = .white
    }
}

class SeperatorView: CustomView {
    
    override func awakeFromNib() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupStyle()
    }
    
    private func setupStyle() {
        //backgroundColor = .clear
        self.backgroundColor = UIColor(named: "")
    }
    
}

class NavigationBarView: CustomView {
    
    override func awakeFromNib() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupStyle()
    }
    
    private func setupStyle() {
        self.backgroundColor = UIColor(named: "")
    }
    
}
class CustomGradientView: CustomView {
    
    var leftColor = UIColor(named: "ButtonLeftGradient")
    var rightColor = UIColor(named: "ButtonRightGradient")
    
    var customImage : UIImage? {
        didSet {
            updateImageView()
        }
    }
    
    let gradientLayer = CAGradientLayer()
    let customImageView = UIImageView()

    override var frame: CGRect {
        didSet {
            gradientLayer.frame = bounds
            layer.layoutIfNeeded()
        }
    }

    convenience init(leftColor: UIColor, rightColor: UIColor, image: UIImage) {
        self.init()

        self.leftColor = leftColor
        self.rightColor = rightColor
        customImage = image
        updateImageView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInitializer()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultInitializer()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradientLayer.frame = self.bounds
    }
    
    func defaultInitializer() {

        gradientLayer.frame = bounds
        gradientLayer.colors = [leftColor!.cgColor, rightColor!.cgColor]
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
        layer.layoutIfNeeded()

        addSubview(customImageView)
        customImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    func updateImageView() {
        customImageView.image = customImage
        customImageView.alpha = 0.2
        layoutSubviews()
    }
    
  
    
}
