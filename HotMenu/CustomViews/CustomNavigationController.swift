//
//  CustomNavigationController.swift
//  Passenger
//
//  Created by brainium on 13/03/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

class InitialNavigationController: CustomNavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarHidden(true, animated: false)
    }
    
}

class HomeNavigationController: CustomNavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarHidden(true, animated: false)
    }
    
}
