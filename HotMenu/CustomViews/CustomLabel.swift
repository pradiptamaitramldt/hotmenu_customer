//
//  CustomLabel.swift
//  Passenger
//
//  Created by brainium on 12/03/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {
    
    override func awakeFromNib() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupStyle()
    }
    
    public func setupStyle() {
        backgroundColor = .clear
        self.textColor = UIColor(named: "BlackColor")
        self.font = UIFont(name: "", size: 17)
    }
    
    public func setTextColor(_ color: UIColor) {
        self.textColor = color
    }
    
    public func setTextAlignment(_ alignment: NSTextAlignment) {
        self.textAlignment = alignment
    }
    
    public func setTextFont(fontWeight: FontWeight, ofSize: CGFloat = 17) {
        switch fontWeight {
        case .bold:
            self.font = UIFont(name: "CeraPro-Bold", size: ofSize)
        case .medium:
            self.font = UIFont(name: "CeraPro-Medium", size: ofSize)
        case .light:
            self.font = UIFont(name: "CeraPro-Light", size: ofSize)
        case .semibold:
            self.font = UIFont(name: "CeraPro-Bold", size: ofSize)
        default:
            self.font = UIFont(name: "CeraPro-Regular", size: ofSize)
        }
    }
    
}
