//
//  CustomTextField.swift
//  Passenger
//
//  Created by brainium on 13/03/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

enum Direction {
    case Left
    case Right
}

class CustomTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // add image to textfield
    func withImage(direction: Direction, image: UIImage , placeholderText : String){
        self.backgroundColor = UIColor.init(named: "TextFieldBgColor")
        self.textColor = UIColor.init(named: "BlackColor")
        self.attributedPlaceholder = NSAttributedString(string: placeholderText,
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
    
        let imageView = UIImageView(frame: CGRect(x: 25.0, y: 8.0, width: 24.0, height: 24.0))
        //let image = UIImage(named: image)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        

        let view = UIView(frame: CGRect(x: 0, y: 0, width:60, height: 40))
        view.addSubview(imageView)
        view.backgroundColor = .clear
        
        if(Direction.Left == direction){
            self.leftViewMode = .always
            self.leftView = view
        }else{
            self.rightViewMode = .always
            self.rightView = view
        }
    }
    
    func withImage(direction: Direction, image: UIImage?, placeholder: String) {
        self.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        let imageView = UIImageView(frame: CGRect(x: 5.0, y: 5.0, width: 25.0, height: 25.0))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        view.addSubview(imageView)
        view.backgroundColor = .clear
        if (Direction.Left == direction) {
            self.leftViewMode = .always
            self.leftView = view
        } else {
            self.rightViewMode = .always
            self.rightView = view
        }
    }
    
    func setFont(fontWeight: FontWeight, size: CGFloat) {
        switch fontWeight {
        case .bold:
            self.font = UIFont(name: "CeraPro-Bold", size: size)
        case .medium:
            self.font = UIFont(name: "CeraPro-Medium", size: size)
        case .light:
            self.font = UIFont(name: "CeraPro-Light", size: size)
        case .semibold:
            self.font = UIFont(name: "CeraPro-Bold", size: size)
        default:
            self.font = UIFont(name: "CeraPro-Regular", size: size)
        }
    }
    
    func withImageForMobile(direction: Direction, image: UIImage , placeholderText : String){
        self.backgroundColor = UIColor.init(named: "TextFieldBgColor")
        self.textColor = UIColor.init(named: "BlackColor")
        self.attributedPlaceholder = NSAttributedString(string: placeholderText,
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
    
        let imageView = UIImageView(frame: CGRect(x: 10.0, y: 8.0, width: 24.0, height: 24.0))
        //let image = UIImage(named: image)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        

        let view = UIView(frame: CGRect(x: 0, y: 0, width:40, height: 40))
        view.addSubview(imageView)
        view.backgroundColor = .clear
        
        if(Direction.Left == direction){
            self.leftViewMode = .always
            self.leftView = view
        }else{
            self.rightViewMode = .always
            self.rightView = view
        }
    }
     open var fontSize : CGFloat = CGFloat(17) {
           didSet {
               self.font = UIFont(name: "Muli-Regular", size: fontSize)
           }
       }
    
    func setDefaultTextFieldColor(){
        self.textColor = UIColor.init(named: "BlackColor")
    }
    
    public func addDropDownArrow() {
        self.rightViewMode = .always
        let rightView = UIView(frame: CGRect(x: self.bounds.width - 30, y: 0, width: 30, height: self.bounds.height))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: self.bounds.height))
        imageView.image = UIImage(named: "IconDropDownArrow")
        imageView.contentMode = .scaleAspectFit
        rightView.addSubview(imageView)
        self.rightView = rightView
    }
}



class UnderLineTextField: UITextField {

    let border = CALayer()

    open var lineColor : UIColor = UIColor.lightGray {
        didSet {
            border.borderColor = lineColor.cgColor
        }
    }

    open var selectedLineColor : UIColor = UIColor.black {
        didSet {
            
        }
    }

    open var lineHeight : CGFloat = CGFloat(1.0) {
        didSet {
            border.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
        }
    }
    
    open var fontSize : CGFloat = CGFloat(17) {
        didSet {
            self.font = UIFont(name: "Muli-Regular", size: fontSize)
        }
    }

    required init?(coder aDecoder: (NSCoder?)) {
        super.init(coder: aDecoder!)
        //self.delegate = self
        border.borderColor = lineColor.cgColor
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])


        border.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = lineHeight
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        self.font = UIFont(name: "Muli-Regular", size: 17)
        self.textColor = .black
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        }
    }

    override func draw(_ rect: CGRect) {
        border.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        border.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
        //self.delegate = self
    }
    
    public func setHighLightedUnderLine() {
        border.borderColor = selectedLineColor.cgColor
    }
    
    public func setNormalUnderLine() {
        border.borderColor = lineColor.cgColor
    }
    
    public func addDropDownArrow() {
        self.rightViewMode = .always
        self.rightView = UIImageView(image: UIImage(named: "IconDropDownArrow"))
    }
    
}

