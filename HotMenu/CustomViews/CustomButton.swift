//
//  CustomButton.swift
//  Passenger
//
//  Created by brainium on 12/03/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    override func awakeFromNib() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupStyle()
    }
    
    private func setupStyle() {
        self.backgroundColor = .clear
        self.tintColor = .clear
        self.titleLabel?.font = UIFont(name: "", size: 17)
    }
    
    public func setTitleAlignment(_ alignment: UIControl.ContentHorizontalAlignment) {
        self.contentHorizontalAlignment = alignment
    }
    
    public func setTitleFont(fontWeight: FontWeight, ofSize: CGFloat = 17) {
        switch fontWeight {
        case .bold:
            self.titleLabel?.font = UIFont(name: "CeraPro-Bold", size: ofSize)
        case .medium:
            self.titleLabel?.font = UIFont(name: "CeraPro-Medium", size: ofSize)
        case .light:
            self.titleLabel?.font = UIFont(name: "CeraPro-Light", size: ofSize)
        case .thin:
            self.titleLabel?.font = UIFont(name: "CeraPro-Thin", size: ofSize)
        default:
            self.titleLabel?.font = UIFont(name: "CeraPro-Regular", size: ofSize)
        }
    }
    
    public func setNormalTitle(_ title: String) {
        self.setTitle(title, for: .normal)
    }
    
    public func setNormalTitleColor(_ color: UIColor?) {
        self.setTitleColor(color ?? UIColor.black, for: .normal)
    }
    
    public func setRoundedCorner() {
        self.layer.cornerRadius = self.bounds.size.height / 2
        self.layer.masksToBounds = true
    }
    
    func customizeButtonFont(fullText: String, mainText: String, creditsText: String) {
        
        let fontBig = UIFont(name:"CeraPro-Light", size: 17.0)
        let fontSmall = UIFont(name:"CeraPro-Regular", size: 19.0)
        let attributedString = NSMutableAttributedString(string: fullText, attributes: nil)
        
        let bigRange = (attributedString.string as NSString).range(of: mainText)
        let creditsRange = (attributedString.string as NSString).range(of: creditsText)
        
        attributedString.setAttributes([ NSAttributedString.Key.font: fontBig!, NSAttributedString.Key.foregroundColor: UIColor.white], range: bigRange)
        
        attributedString.setAttributes([NSAttributedString.Key.font: fontSmall!, NSAttributedString.Key.foregroundColor: UIColor.init(named: "YellowColor")!], range: creditsRange)
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}

class CustomGradientButton: CustomButton {
    
    var leftColor = UIColor(named: "ButtonLeftGradient")
    var rightColor = UIColor(named: "ButtonRightGradient")
    
    var customImage : UIImage? {
        didSet {
            updateImageView()
        }
    }
    
    let gradientLayer = CAGradientLayer()
    let customImageView = UIImageView()
    
    override var frame: CGRect {
        didSet {
            gradientLayer.frame = bounds
            layer.layoutIfNeeded()
        }
    }
    
    convenience init(leftColor: UIColor, rightColor: UIColor, image: UIImage) {
        self.init()
        
        self.leftColor = leftColor
        self.rightColor = rightColor
        customImage = image
        updateImageView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInitializer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultInitializer()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradientLayer.frame = self.bounds
    }
    
    func defaultInitializer() {
        
        gradientLayer.frame = bounds
        gradientLayer.colors = [leftColor!.cgColor, rightColor!.cgColor]
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
        layer.layoutIfNeeded()
        
        addSubview(customImageView)
        customImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func updateImageView() {
        customImageView.image = customImage
        customImageView.alpha = 0.2
        layoutSubviews()
    }
    
    public override func setRoundedCorner() {
        self.layer.cornerRadius = self.bounds.size.height / 2
        self.layer.masksToBounds = true
    }
    
}

