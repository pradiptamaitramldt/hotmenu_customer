//
//  LocationCell.swift
//  LocationTacking
//
//  Created by BrainiumSSD on 08/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    //MARK:- IBoutlet
    @IBOutlet var iconImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
