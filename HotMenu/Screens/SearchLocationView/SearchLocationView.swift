//
//  SearchLocationView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 11/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

protocol SelecetdPlacesDelegate {
    func locationSelected(currentLocation : Location, ofType:String )
    func selectedLocationType(ofType : String, selectedIndex : Int)
}

class SearchLocationView: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var textFieldLocationSearch: UITextField!
    @IBOutlet weak var controlBack: UIControl!
    @IBOutlet weak var tableViewSearchResult: UITableView!
    
    // MARK: - Other Variables
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    var geocoder = CLGeocoder()
    var locationType = String(),didTextType = false
    var selectedPlaceDelegate:SelecetdPlacesDelegate?
    //   var fetcher = GMSAutocompleteFetcher()
    //  var filter = GMSAutocompleteFilter()
    var searchResultArray = NSMutableArray()
    var titleArray = ["Get Current Location","Home","Work","Other"]
    var addessArray = ["Using GPS","Sk Md Ekbal Tower -1 , 6th floor Godrej Waterside, Sector V Kolkata- 70009, Mobile: 8181467223","Sk Md Ekbal Tower -1 , 6th floor Godrej Waterside, Sector V Kolkata- 70009, Mobile: 8181467223","Sk Md Ekbal Tower -1 , 6th floor Godrej Waterside, Sector V Kolkata- 70009, Mobile: 8181467223"]
    var iconImageArray = ["current_location_icon","home_icon","office_icon","other_icon"]
    
    private var webService = LocationAPI()
    private var addressListRequest = AddressListRequest()
    var addressList: [AddressListResponseData] = []
    var googleSearchAddress: [Prediction] = []
    var block: ((_ addressID: String, _ address: String, _ landmark: String, _ houseNumber: String, _ lat: String, _ long: String, _ typeID: String, _ type: String, _ comingFrom: String)->Void)?
    var lattitude: String = ""
    var longitude: String = ""
    var comingFrom: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        //   buttonCurrentLocation.setCirculer()
        locationManager.delegate = self
        textFieldLocationSearch.delegate = self
        tableViewSearchResult.register(UINib(nibName: "LocationCellView", bundle: nil), forCellReuseIdentifier: "LocationCell")
        tableViewSearchResult.dataSource = self
        tableViewSearchResult.delegate = self
        //  fetcher = GMSAutocompleteFetcher()
        //  filter = GMSAutocompleteFilter()
        //  fetcher.delegate = self
        currentLocation = CLLocation(latitude: 0.0, longitude: 0.0)
        self.addresList()
    }
    
    func didCurrentLocationSelected() {
        
        geocoder.reverseGeocodeLocation(currentLocation!, completionHandler: { placemarks, error in
            //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
            if error == nil && (placemarks?.count ?? 0) > 0 {
                let placemark = placemarks?[0]
                let currentAddress = Utility.removeNull(from: "\(placemark?.name ?? ""),\(placemark?.locality ?? ""),\(placemark?.subAdministrativeArea ?? "")")
                let getLocation = Location(
                    values: "",
                    locationName: currentAddress,
                    locationSecondaryName: currentAddress,
                    locationLatitude: self.currentLocation!.coordinate.latitude,
                    locationLongitude: self.currentLocation!.coordinate.longitude)
                self.selectedPlaceDelegate?.locationSelected(currentLocation: getLocation, ofType: "CurrentLocation")
                self.dismiss(animated: true)
                self.navigationController?.popViewController(animated: true)
            } else {
                print("\(error.debugDescription)")
            }
        })
    }
    
    func addresList() {
        self.dataStartedLoading()
        self.addressListRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.webService.addressList(addressListRequest: self.addressListRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.addressList = data
                
                DispatchQueue.main.async {
                    self.tableViewSearchResult.reloadData()
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func goolgePlaces(url: String) {
        self.dataStartedLoading()
        self.webService.googlePlaces(googleAPI: url) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.predictions else {
                    return
                }
                
                self.googleSearchAddress = data
//                if self.googleSearchAddress.count > 0 {
//                    self.getPlaceDataByPlaceID(placeID: self.googleSearchAddress[0].placeID ?? "")
//                }
                
                DispatchQueue.main.async {
                    self.tableViewSearchResult.reloadData()
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func getPlaceDataByPlaceID(placeID: String, data: Prediction)
    {
        //  pPlaceID = "ChIJXbmAjccVrjsRlf31U1ZGpDM"
        let placesClient = GMSPlacesClient.shared()
        placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
        
            if let place = place {
                print("Place name \(place.name ?? "")")
                print("Place address \(place.formattedAddress!)")
                print("Place placeID \(String(describing: place.placeID))")
                print("Place attributions \(String(describing: place.attributions))")
                print("\(place.coordinate.latitude)")
                print("\(place.coordinate.longitude)")
                self.lattitude = "\(place.coordinate.latitude)"
                self.longitude = "\(place.coordinate.longitude)"
                
                self.block?("", data.predictionDescription ?? "", place.name ?? "", "", self.lattitude, self.longitude, "", Strings.addressTypeOther, "")
                
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            } else {
                print("No place details for \(placeID)")
            }
        })
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
            let str = self.textFieldLocationSearch.text?.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed)
            
            #if DEBUG
        let placeApiStr = "https://maps.googleapis.com/maps/api/place/autocomplete/json?&key=\(Strings.googlePlaceAPIKey)&input=\(str!)&components=country:ind"
            #else
                let placeApiStr = "https://maps.googleapis.com/maps/api/place/autocomplete/json?&key=\(Strings.googlePlaceAPIKey)&input=\(str!)&components=country:ind"
            #endif
            
        self.goolgePlaces(url: placeApiStr)
        }
    
    // MARK: - IBACTION
    
    @IBAction func buttonBackAction(_ sender: Any) {
        textFieldLocationSearch.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension SearchLocationView : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //         fetcher.delegate = self
        //        let updatedText = textField.text
        //        if updatedText?.count == 0 {
        //            fetcher.sourceTextHasChanged("")
        //        } else {
        //            if updatedText?.count ?? 0 > 50 {
        //                return false
        //            }
        //            let newString = textField.text
        //            // [self queryGetLocationName: newString];
        //            fetcher.sourceTextHasChanged(newString)
        //        }
        let updatedText = textField.text
        if updatedText?.count == 0 {
            didTextType = false
        } else {
            didTextType = true
        }
        tableViewSearchResult.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return true
    }
}
extension SearchLocationView : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        currentLocation = location
        locationManager.stopUpdatingLocation()
    }
}

extension SearchLocationView : GMSAutocompleteFetcherDelegate {
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        
        if predictions.count != 0 {
            var resultsStr = ""
            searchResultArray.removeAllObjects()
            for prediction in predictions {
                resultsStr += "\(prediction.attributedPrimaryText.string)\n"
                //NSLog(@"Result '%@' with placeID %@", prediction.attributedFullText.string, prediction.placeID);
                // searchResultArray.append() //lat long = 0.0 for now only
                // searchResultArray.removeAllObjects()
                searchResultArray.add(Location(values: prediction.placeID, locationName: prediction.attributedPrimaryText.string, locationSecondaryName: prediction.attributedFullText.string, locationLatitude: 0.0, locationLongitude: 0.0))
                
                tableViewSearchResult.reloadData()
            }
        } else {
        }
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        
    }
    
}
extension SearchLocationView : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if didTextType {
            return self.googleSearchAddress.count + 1
        }else {
            return addressList.count + 1
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: LocationCell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") as! LocationCell
        if didTextType {
            if indexPath.row == 0 {
                cell.iconImage.image = UIImage(named: "current_location_icon")
                cell.titleLabel.text = titleArray[0]
                cell.addressLabel.text = addessArray[0]
            }else{
                var centerNameStr: String?
                var fullAddressStr: String?
                if self.googleSearchAddress.count > 0 {
                    centerNameStr = self.googleSearchAddress[indexPath.row - 1].structuredFormatting?.mainText
                    fullAddressStr = self.googleSearchAddress[indexPath.row - 1].structuredFormatting?.secondaryText
                }
                cell.titleLabel.text = centerNameStr
                cell.addressLabel.text = fullAddressStr
                cell.iconImage.image = UIImage(named: "location_icon")
            }
        }else{
            if indexPath.row == 0 {
                cell.iconImage.image = UIImage(named: "current_location_icon")
                cell.titleLabel.text = titleArray[0]
                cell.addressLabel.text = addessArray[0]
            }else{
                var addressType: String = ""
                var fullAddressStr: String = ""
                if self.addressList.count > 0 {
                    addressType = self.addressList[indexPath.row - 1].addressType?.type ?? ""
                    fullAddressStr = "\(self.addressList[indexPath.row - 1].flatOrHouseOrBuildingOrCompany ?? ""), \(self.addressList[indexPath.row - 1].fullAddress ?? ""), \(self.addressList[indexPath.row - 1].landmark ?? "")"
                    cell.titleLabel.text = addressType
                    cell.addressLabel.text = fullAddressStr
                    if addressType == Strings.addressTypeHome {
                        cell.iconImage.image = UIImage(named: "home")
                    }else if addressType == Strings.addressTypeWork {
                        cell.iconImage.image = UIImage(named: "office_icon")
                    }else {
                        cell.iconImage.image = UIImage(named: "other_icon")
                    }
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 75
    //    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            didCurrentLocationSelected()
        }else {
            if self.didTextType {
//                self.selectedPlaceDelegate?.selectedLocationType(ofType: titleArray[indexPath.row - 1], selectedIndex: indexPath.row - 1)
                self.textFieldLocationSearch.resignFirstResponder()
                
                let data = self.googleSearchAddress[indexPath.row - 1]
                self.getPlaceDataByPlaceID(placeID: data.placeID ?? "", data: data)
                
            }else{
                let address = self.addressList[indexPath.row - 1]
                block?(address.id ?? "", address.fullAddress ?? "", address.landmark ?? "", address.flatOrHouseOrBuildingOrCompany ?? "", "\(address.location?.coordinates?.last ?? 0.0)", "\(address.location?.coordinates?.first ?? 0.0)", address.addressType?.id ?? "", address.addressType?.type ?? "", self.comingFrom)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
extension SearchLocationView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
