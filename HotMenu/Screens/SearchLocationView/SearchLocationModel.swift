//
//  SearchLocationModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 11/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct AddressListRequest {
    
    var userID: String
        
    init() {
        self.userID = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        
        return tempDictionary
    }
}

// MARK: - AddressListResponseModel
struct AddressListResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [AddressListResponseData]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct AddressListResponseData: Codable {
    let location: LocationData?
    let isDefault: Bool?
    let landmark, id: String?
    let addressType: AddressType?
    let fullAddress, flatOrHouseOrBuildingOrCompany, userID, createdAt: String?
    let updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case location, isDefault, landmark
        case id = "_id"
        case addressType, fullAddress, flatOrHouseOrBuildingOrCompany
        case userID = "userId"
        case createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - AddressType
struct AddressType: Codable {
    let id, type: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type
    }
}

// MARK: - Location
struct LocationData: Codable {
    let coordinates: [Double]?
    let type: String?
}

// MARK: - GooglePlaceResponseModel
struct GooglePlaceResponseModel: Codable {
    let predictions: [Prediction]?
    let status: String?
}

// MARK: - Prediction
struct Prediction: Codable {
    let predictionDescription: String?
    let matchedSubstrings: [MatchedSubstring]?
    let placeID, reference: String?
    let structuredFormatting: StructuredFormatting?
    let terms: [Term]?
    let types: [String]?

    enum CodingKeys: String, CodingKey {
        case predictionDescription = "description"
        case matchedSubstrings = "matched_substrings"
        case placeID = "place_id"
        case reference
        case structuredFormatting = "structured_formatting"
        case terms, types
    }
}

// MARK: - MatchedSubstring
struct MatchedSubstring: Codable {
    let length, offset: Int?
}

// MARK: - StructuredFormatting
struct StructuredFormatting: Codable {
    let mainText: String?
    let mainTextMatchedSubstrings: [MatchedSubstring]?
    let secondaryText: String?

    enum CodingKeys: String, CodingKey {
        case mainText = "main_text"
        case mainTextMatchedSubstrings = "main_text_matched_substrings"
        case secondaryText = "secondary_text"
    }
}

// MARK: - Term
struct Term: Codable {
    let offset: Int?
    let value: String?
}
