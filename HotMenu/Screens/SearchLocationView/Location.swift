//
//  Location.swift
//  OnDemandDeliveryApplication
//
//  Created by BrainiumSSD on 28/07/20.
//  Copyright © 2020 EEGRAB. All rights reserved.
//

import Foundation
class Location {
    
    var locationId : String?
    var locationName: String?
    var locationLatitude: Double?
    var locationLongitude: Double?
    var locationSecondaryName: String?
    var order: Int?
    init(values locationId: String?, locationName: String?, locationLatitude: Double, locationLongitude: Double) {
       
        self.locationId = locationId
        self.locationName = locationName
        self.locationLatitude = locationLatitude
        self.locationLongitude = locationLongitude
        order = 1 //default value
    }

    init(values locationId: String?, locationName: String?, locationSecondaryName: String?, locationLatitude: Double, locationLongitude: Double) {
        
        self.locationId = locationId
        self.locationName = locationName
        self.locationSecondaryName = locationSecondaryName
        self.locationLatitude = locationLatitude
        self.locationLongitude = locationLongitude
        order = 1 //default value
    }
}

