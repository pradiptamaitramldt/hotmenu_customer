//
//  Models.swift
//  Driver
//
//  Created by Pallab on 04/06/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

struct ResponseError: Codable {
    
    //let field : String?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        //case field = "field"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        //field = try values.decodeIfPresent(String.self, forKey: .field) ?? ""
        message = try values.decodeIfPresent(String.self, forKey: .message) ?? "Some error occured!"
    }
    
}

struct ResponseSuccess: Codable {
    
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message) ?? "Some error occured!"
    }
    
}
