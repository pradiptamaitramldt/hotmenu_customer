//
//  LeftMenuTableViewCell.swift
//  Passenger
//
//  Created by Pradipta Maitra on 25/06/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuNameLabel: CustomLabel!
    @IBOutlet weak var imageViewItmeIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
