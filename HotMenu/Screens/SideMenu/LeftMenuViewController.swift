//
//  LeftMenuViewController.swift
//  Driver
//
//  Created by Pallab on 04/06/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import SDWebImage

func IS_IPAD() -> Bool {
    switch UIDevice.current.userInterfaceIdiom {
    case .phone: // It's an iPhone
        return false
    case .pad: // It's an iPad
        return true
    case .unspecified: // undefined
        return false
    default:
        return false
    }
}

public let screenWidth = UIScreen.main.bounds.width
public let screenHeight = UIScreen.main.bounds.height

struct MenuItem {
    var iconName: String?
    var title: String
    var subItems: [MenuSubItem]
    var isExpanded: Bool
    var hasSubItems: Bool
    var isSubItem: Bool
    var menuItemIndexOfSubItem: Int
    
    init() {
        self.iconName = ""
        self.title = ""
        self.subItems = []
        self.isExpanded = false
        self.hasSubItems = false
        self.isSubItem = false
        self.menuItemIndexOfSubItem = -1
    }
    
    init(iconName: String?, title: String, subItems: [MenuSubItem], isExpanded: Bool, hasSubItems: Bool, isSubItem: Bool, menuItemIndexOfSubItem: Int = -1) {
        self.iconName = iconName
        self.title = title
        self.subItems = subItems
        self.isExpanded = isExpanded
        self.hasSubItems = hasSubItems
        self.isSubItem = isSubItem
        self.menuItemIndexOfSubItem = menuItemIndexOfSubItem
    }
}

struct MenuSubItem {
    var iconName: String?
    var title: String
    
    init() {
        iconName = ""
        title = ""
    }
    
    init(iconName: String?, title: String) {
        self.iconName = iconName
        self.title = title
    }
}

protocol LeftMenuViewControllerDelegate: class {
    func clickedMenuHeader()
    func clickedIndexPath(index: Int)
    func clickedLogout()
}

class LeftMenuViewController: UIViewController {
    
    @IBOutlet weak var menuContainerView: UIView!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var leftMenuTableView: UITableView!
    
    @IBOutlet weak var imageViewUserImage: UIImageView!
    @IBOutlet weak var labelUserName: CustomLabel!
    @IBOutlet weak var labelUserPhone: CustomLabel!
    @IBOutlet weak var buttonLogout: CustomButton!
    @IBOutlet weak var buttonMenuHeader: UIButton!
    @IBOutlet weak var buttonChangeLanguage: CustomButton!
    @IBOutlet weak var viewTermsBG: CustomView!
    @IBOutlet weak var viewFAQBG: CustomView!
    @IBOutlet weak var viewPrivacyBG: CustomView!
    @IBOutlet weak var viewVersionBG: CustomView!
    @IBOutlet weak var labelTerms: CustomLabel!
    @IBOutlet weak var labelFAQ: CustomLabel!
    @IBOutlet weak var labelPrivacy: CustomLabel!
    @IBOutlet weak var labelVersion: CustomLabel!
    
    var viewController: UIViewController?
    var contentArr = [[[String: Any]]]()
    let cellHeight: CGFloat = IS_IPAD() ? 75 : 60
    var menuItemsCount: Int = 0
    
    public static weak var delegate: LeftMenuViewControllerDelegate?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.menuContainerView.transform = CGAffineTransform(translationX: -screenWidth * 0.7, y: 0)
        self.alphaView.alpha = 0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeMenu))
        tapGesture.numberOfTapsRequired = 1
        self.alphaView.addGestureRecognizer(tapGesture)
        
        
        let dic1 = [
            "image": "package",
            "text": "My Order",
        ]
        let dic2 = [
            "image": "heart",
            "text": "Favourite Dishes",
        ]
        let dic3 = [
            "image": "bell_menu",
            "text": "Notification",
        ]
        let dic4 = [
            "image": "map-pin",
            "text": "My Address",
        ]
        let dic5 = [
            "image": "credit-card",
            "text": "Saved Cards",
        ]
        
        let arr1: [[String: Any]] = [ dic1, dic2, dic3, dic4, dic5]
        
        contentArr = [arr1]
        
        leftMenuTableView.delegate = self
        leftMenuTableView.dataSource = self
        
        leftMenuTableView.reloadData()
        
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaultsManager.shared.getStringValue(forKey: .profileImage) != Strings.blankString {
            let profilePicture = UserDefaultsManager.shared.getStringValue(forKey: .profileImage)
            self.imageViewUserImage.sd_setImage(with: URL(string: profilePicture), placeholderImage: nil, options: .progressiveLoad, completed: nil)
        }
        
        let fullName = "\(UserDefaultsManager.shared.getStringValue(forKey: .fullName))"
        self.labelUserName.text = fullName
        self.labelUserName.textColor = .white
        self.labelUserName.textAlignment = .center
        let fullPhone = "\(UserDefaultsManager.shared.getStringValue(forKey: .isdCode)) \(UserDefaultsManager.shared.getStringValue(forKey: .phone))"
        self.labelUserPhone.text = fullPhone
        self.labelUserPhone.textColor = .white
        self.labelUserPhone.textAlignment = .center
    }
    
    func setup() {
        
        self.labelTerms.setTextFont(fontWeight: .regular , ofSize: 14)
        self.labelTerms.text = Strings.terms_condition
        self.labelTerms.textColor = UIColor(red: 62.0/255, green: 63.0/255, blue: 104.0/255, alpha: 1.0)
        
        self.labelFAQ.setTextFont(fontWeight: .regular , ofSize: 14)
        self.labelFAQ.text = Strings.faqs
        self.labelFAQ.textColor = UIColor(red: 62.0/255, green: 63.0/255, blue: 104.0/255, alpha: 1.0)
        
        self.labelPrivacy.setTextFont(fontWeight: .regular , ofSize: 14)
        self.labelPrivacy.text = Strings.privacy
        self.labelPrivacy.textColor = UIColor(red: 62.0/255, green: 63.0/255, blue: 104.0/255, alpha: 1.0)
        
        self.labelVersion.setTextFont(fontWeight: .regular , ofSize: 14)
        self.labelVersion.text = Strings.version
        self.labelVersion.textColor = UIColor(red: 62.0/255, green: 63.0/255, blue: 104.0/255, alpha: 1.0)
    }
    
    // MARK: Menu Closing
    
    @objc func closeMenu()
    {
        self.menuClosingGlobal()
    }
    
    func menuClosingGlobal() {
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.transform = CGAffineTransform(translationX: -screenWidth * 0.8, y: 0)
            self.alphaView.alpha = 0
        }) { (_) in
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
    // MARK: Menu push
    
    func menuClosingForPush(withIndexPath indexPathForRow: IndexPath) {
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.transform = CGAffineTransform(translationX: -screenWidth * 0.8, y: 0)
            self.alphaView.alpha = 0
        }) { (_) in
            self.view.removeFromSuperview()
            self.removeFromParent()
            
            self.push(withIndexPath: indexPathForRow)
        }
    }
    
    func push(withIndexPath indexPath: IndexPath) {
        let viewControllers: [UIViewController] = viewController!.navigationController!.viewControllers as [UIViewController];
        
        let viewControllerCurrent: UIViewController = viewControllers[viewControllers.count - 1]
        print("last object \(viewControllerCurrent)")
        
        if indexPath.row == 0 {
            //My Order
            //My Address
            if viewControllerCurrent.isKind(of: MyOrderController.self) {
                
            } else {
                let myOrderOBJ = UIStoryboard(name: "MyOrder", bundle: nil).instantiateViewController(withIdentifier: "MyOrderController") as! MyOrderController
                myOrderOBJ.previousClass = "SideMenu"
                viewController!.navigationController?.pushViewController(myOrderOBJ, animated: true)
            }
            
        } else if indexPath.row == 1 {
            //Favourite Menu
            if viewControllerCurrent.isKind(of: FavouriteView.self) {
                
            } else {
                let favouriteOBJ = UIStoryboard(name: "Favourite", bundle: nil).instantiateViewController(withIdentifier: "FavouriteView") as! FavouriteView
                viewController!.navigationController?.pushViewController(favouriteOBJ, animated: true)
            }
        } else if indexPath.row == 2 {
            //add Card
            //            if viewControllerCurrent.isKind(of: AddCardViewController.self) {
            //
            //            } else {
            //                let loginOBJ = paymentStoryboard.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
            //                viewController!.navigationController?.pushViewController(loginOBJ, animated: true)
            //            }
            
        }else if indexPath.row == 3 {
            //My Address
            if viewControllerCurrent.isKind(of: AddressListView.self) {
                
            } else {
                let locationOBJ = UIStoryboard(name: "Location", bundle: nil).instantiateViewController(withIdentifier: "AddressListView") as! AddressListView
                viewController!.navigationController?.pushViewController(locationOBJ, animated: true)
            }
        } else if indexPath.row == 4 {
            //settings
            //            if viewControllerCurrent.isKind(of: SettingsViewController.self) {
            //
            //            } else {
            //                let loginOBJ = otherStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            //                viewController!.navigationController?.pushViewController(loginOBJ, animated: true)
            //            }
        } else if indexPath.row == 5 {
            // feedback
            //            if viewControllerCurrent.isKind(of: FeedbackViewController.self) {
            //
            //            } else {
            //                let loginOBJ = otherStoryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
            //                viewController!.navigationController?.pushViewController(loginOBJ, animated: true)
            //            }
        }else if indexPath.row == 6 {
            // logout
            
            //            showActionSheet(WithParentViewController: viewController!)
        }
        
    }
    
    @IBAction func buttonTermsAction(_ sender: Any) {
    }
    @IBAction func buttonFAQAction(_ sender: Any) {
    }
    @IBAction func buttonPrivacyAction(_ sender: Any) {
    }
    @IBAction func buttonVersionAction(_ sender: Any) {
    }
    
    
    // MARK: Show Left Menu
    
    static func showLeftMenu(onParentViewController parentViewController: UIViewController, selected: @escaping (_ value: AnyObject?, _ index: Int?) -> Void) {
        
        let classObj = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        
        classObj.showMenu(onParentViewController: parentViewController)
    }
    
    func showMenu(onParentViewController parentViewController: UIViewController) {
        
        self.view.frame = UIScreen.main.bounds
        UIApplication.shared.windows.first!.addSubview(self.view)
        parentViewController.addChild(self)
        self.didMove(toParent: parentViewController)
        parentViewController.view.bringSubviewToFront(self.view)
        
        viewController = parentViewController
        
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.transform = .identity
            self.alphaView.alpha = 0.7
        }) { (_) in
            
        }
    }
    
    @IBAction func buttonMenuHeaderClicked(_ sender: UIButton) {
        
        self.menuClosingGlobal()
        let viewControllers: [UIViewController] = viewController!.navigationController!.viewControllers as [UIViewController];
        
        let viewControllerCurrent: UIViewController = viewControllers[viewControllers.count - 1]
        if !viewControllerCurrent.isKind(of: ProfileSettingsView.self) {
            let profileObj = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileSettingsView") as! ProfileSettingsView
            viewController!.navigationController?.pushViewController(profileObj, animated: true)
        }
    }
    
    @IBAction func buttonLogoutClicked(_ sender: CustomButton) {
        self.menuClosingGlobal()
        showActionSheet(WithParentViewController: viewController!)
    }
    
    @IBAction func buttonChangeLanguageClicked(_ sender: CustomButton) {
        
    }
    
    func showActionSheet(WithParentViewController parentController: UIViewController) {
        var actions: [(String, UIAlertAction.Style)] = []
        actions.append(("YES", UIAlertAction.Style.default))
        actions.append(("NO", UIAlertAction.Style.default))
        
        
        //self = ViewController
        Alerts.showActionsheet(viewController: parentController, title: Strings.logout, message: Strings.logoutWarning, actions: actions) { (index) in
            print("call action \(index)")
            
            print(index)
            
            self.pushToAViewController(withViewcontroller: parentController, withindex: index)
        }
    }
    
    func pushToAViewController(withViewcontroller parentViewController: UIViewController,withindex selectedIndex: Int) {
        
        if selectedIndex == 0 {
            UserDefaultsManager.shared.clearAllValuesExceptDeviceToken()
            let dashboardVC = (viewController?.navigationController!.viewControllers.filter { $0 is LoginView }.first!)!
            viewController?.navigationController!.popToViewController(dashboardVC, animated: true)
        }
    }
}

private typealias TableviewDelegateAndDatasource = LeftMenuViewController
extension TableviewDelegateAndDatasource : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArr[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell", for: indexPath) as! LeftMenuTableViewCell
        
        var rowArray = [[String: Any]]()
        
        rowArray = contentArr[indexPath.section]
        
        cell.menuNameLabel.text = rowArray[indexPath.row]["text"] as? String
        cell.menuNameLabel.textColor = UIColor(red: 62.0/255, green: 63.0/255, blue: 104.0/255, alpha: 1.0)
        cell.imageViewItmeIcon.image = UIImage(named: (rowArray[indexPath.row]["image"] as? String)!)!
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.menuClosingForPush(withIndexPath: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}
class Alerts {
    static func showActionsheet(viewController: UIViewController, title: String, message: String, actions: [(String, UIAlertAction.Style)], completion: @escaping (_ index: Int) -> Void) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: IS_IPAD() ? .alert  : .actionSheet )
        for (index, (title, style)) in actions.enumerated() {
            let alertAction = UIAlertAction(title: title, style: style) { (_) in
                completion(index)
            }
            alertViewController.addAction(alertAction)
        }
        viewController.present(alertViewController, animated: true, completion: nil)
    }
}
