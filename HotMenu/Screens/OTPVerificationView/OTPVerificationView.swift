//
//  OTPVerificationView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 12/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class OTPVerificationView: UITableViewController {

    @IBOutlet weak var labelTitle: CustomLabel!
    @IBOutlet weak var labelInfo: CustomLabel!
    @IBOutlet weak var labelTimer: CustomLabel!
    @IBOutlet weak var textField_otp1: CustomTextField!
    @IBOutlet weak var textField_otp2: CustomTextField!
    @IBOutlet weak var textField_otp3: CustomTextField!
    @IBOutlet weak var textField_otp4: CustomTextField!
    @IBOutlet weak var textField_otp5: CustomTextField!
    @IBOutlet weak var textField_otp6: CustomTextField!
    @IBOutlet weak var buttonSendOTP: CustomButton!
    @IBOutlet weak var buttonResendOtp: CustomButton!
    
    var timer: Timer?
    var timerCount: Int = 60
    var registrationResponseData: RegistrationResponseData?
    private var webService = OTPVerifyAPI()
    private var otpVerifyRequest = OTPVerifyRequest()
    private var resendOTPVerifyRequest = ResendOTPVerifyRequest()
    private var webServiceForgot = ForgotPasswordAPI()
    private var forgotPasswordRequest = ForgotPasswordRequest()
    var previousView: String = ""
    
    var email: String = ""
    var otp: String = ""
    var createOtp : String = ""
    var phone: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView.init(image: UIImage(named: "register_bg"))
        tableView.backgroundView?.contentMode = .scaleAspectFill
        tableView.contentInset = UIEdgeInsets(top: -44, left: 0, bottom: 0, right: 0)
        
        self.otpVerifyRequest.delegate = self
        
        self.setup()
    }
    
    func setup() {
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimerLabel) , userInfo: nil, repeats: true)
        
        self.labelTitle.setTextFont(fontWeight: .regular , ofSize: 22)
        self.labelTitle.text = "Verification Code"
        self.labelTitle.textColor = .white
        self.labelTitle.textAlignment = .center
        
        var phone_number: String = ""
        if self.phone == "" {
            phone_number = "NA"
        }else{
            phone_number = self.phone
        }
        self.labelInfo.setTextFont(fontWeight: .light , ofSize: 12)
        self.labelInfo.text = "Please enter the verification code\nsent to \(phone_number)"
        self.labelInfo.textColor = .white
        self.labelInfo.numberOfLines = 0
        self.labelInfo.textAlignment = .center
        
        self.labelTimer.text = "Timer: 60"
        self.labelTimer.setTextFont(fontWeight: .regular , ofSize: 22)
        self.labelTimer.textColor = UIColor.init(named: "YellowColor")
        self.labelTimer.textAlignment = .center
        
        self.textField_otp1.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textField_otp1.fontSize =  25
        self.textField_otp1.textColor = .white
        self.textField_otp1.delegate = self
        self.textField_otp1.tintColor = .white
        
        self.textField_otp2.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textField_otp2.fontSize =  25
        self.textField_otp2.textColor = .white
        self.textField_otp2.delegate = self
        self.textField_otp2.tintColor = .white
        
        self.textField_otp3.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textField_otp3.fontSize =  25
        self.textField_otp3.textColor = .white
        self.textField_otp3.delegate = self
        self.textField_otp3.tintColor = .white
        
        self.textField_otp4.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textField_otp4.fontSize =  25
        self.textField_otp4.textColor = .white
        self.textField_otp4.delegate = self
        self.textField_otp4.tintColor = .white
        
        self.textField_otp5.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textField_otp5.fontSize =  25
        self.textField_otp5.textColor = .white
        self.textField_otp5.delegate = self
        self.textField_otp5.tintColor = .white
        
        self.textField_otp6.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textField_otp6.fontSize =  25
        self.textField_otp6.textColor = .white
        self.textField_otp6.delegate = self
        self.textField_otp6.tintColor = .white
        
        self.textField_otp1.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.textField_otp2.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.textField_otp3.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.textField_otp4.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.textField_otp5.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.textField_otp6.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        let text1 = self.textField_otp1.text ?? ""
        let text2 = self.textField_otp2.text ?? ""
        let text3 = self.textField_otp3.text ?? ""
        let text4 = self.textField_otp4.text ?? ""
        let text5 = self.textField_otp5.text ?? ""
        let text6 = self.textField_otp6.text ?? ""
        self.createOtp = text1 + text2 + text3 + text4 + text5 + text6
        
        self.buttonSendOTP.setNormalTitle(Strings.verifyButton)
        self.buttonSendOTP.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonSendOTP.cornerRadius = 10
        self.buttonSendOTP.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonSendOTP.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
        self.buttonResendOtp.isEnabled = false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let text = textField.text
        
        if text!.count >= 1{
            switch textField{
            case textField_otp1:
                textField_otp2.becomeFirstResponder()
            case textField_otp2:
                textField_otp3.becomeFirstResponder()
            case textField_otp3:
                textField_otp4.becomeFirstResponder()
            case textField_otp4:
                textField_otp5.becomeFirstResponder()
            case textField_otp5:
                textField_otp6.becomeFirstResponder()
            case textField_otp6:
                textField_otp6.resignFirstResponder()
            default:
                break
            }
        }
    }
    
    private func validateOTP(enteredOTP: String) {
        if self.otp == enteredOTP {
            self.dataStartedLoading()
            
            self.webService.otpVerify(otpVerifyRequest: self.otpVerifyRequest) { (success, message, verifyMobileResponse, error) in
                self.dataFinishedLoading()
                if success {
                    guard let successLoginResponse = verifyMobileResponse, let successLoginResponseData = successLoginResponse.responseData else {
                        return
                    }
                    
                    print("OTP Verify successful")
                    DispatchQueue.main.async {
                        let welcomeView = WelcomeView.instantiateFromAppStoryboard(appStoryboard: .verify)
                        self.navigationController?.pushViewController(welcomeView, animated: true)
                    }
                } else {
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
                }
            }
        } else {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: ErrorMessages.invalidOTP)
        }
    }
    
    @objc func updateTimerLabel() {
        self.timerCount -= 1
        if self.timerCount == 0 {
            self.timer?.invalidate()
            self.timer = nil
            self.buttonResendOtp.isEnabled = true
        }
        self.labelTimer.text = "Timer: \(self.timerCount)"
    }

    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonResendOTPAction(_ sender: Any) {
        if self.previousView == Strings.forgot_password {
            self.forgotPasswordRequest.email = self.email
            self.forgotPasswordRequest.userType = Strings.userTypeCustomer
            self.dataStartedLoading()
            self.webServiceForgot.forgotPassword(forgotPasswordRequest: self.forgotPasswordRequest) { (success, message, loginResponse, error) in
                self.dataFinishedLoading()
                if success {
                    guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                        return
                    }
                    
                    let message = successLoginResponse.message
                    
                    DispatchQueue.main.async {
                        let successAlert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
                        
                        successAlert.addAction(UIAlertAction(title: Strings.ok, style: .default) { (action:UIAlertAction!) in
                            
                        })
                        
                        self.present(successAlert, animated: true)
                    }
                    
                    Utility.log("Finally it's done")
                } else {
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
                }
            }
        }else{
            self.resendOTPVerifyRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
            self.dataStartedLoading()
            self.webService.resendOTPVerify(otpVerifyRequest: self.resendOTPVerifyRequest) { (success, message, verifyMobileResponse, error) in
                self.dataFinishedLoading()
                if success {
                    guard let successLoginResponse = verifyMobileResponse, let successLoginResponseData = successLoginResponse.responseData else {
                        return
                    }
                    
                    print("OTP Verify successful")
                    
                    let message = successLoginResponse.message
                    
                    DispatchQueue.main.async {
                        let successAlert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
                        
                        successAlert.addAction(UIAlertAction(title: Strings.ok, style: .default) { (action:UIAlertAction!) in
                            
                        })
                        self.present(successAlert, animated: true)
                    }
                } else {
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
                }
            }
        }
    }
    
    @IBAction func buttonSendOTPAction(_ sender: Any) {
        let text1 = self.textField_otp1.text ?? ""
        let text2 = self.textField_otp2.text ?? ""
        let text3 = self.textField_otp3.text ?? ""
        let text4 = self.textField_otp4.text ?? ""
        let text5 = self.textField_otp5.text ?? ""
        let text6 = self.textField_otp6.text ?? ""
        self.createOtp = text1 + text2 + text3 + text4 + text5 + text6
        if self.previousView == Strings.forgot_password {
            if self.otp == self.createOtp {
                DispatchQueue.main.async {
                    let resetPasswordView = ResetPasswordView.instantiateFromAppStoryboard(appStoryboard: .login)
                    resetPasswordView.email = self.email
                    self.navigationController?.pushViewController(resetPasswordView, animated: true)
                }
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "OTP is not valid")
            }
        }else{
            self.otpVerifyRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
            self.otpVerifyRequest.sid = UserDefaultsManager.shared.getStringValue(forKey: .sid)
            self.otpVerifyRequest.verification_code = self.createOtp
            self.otpVerifyRequest.push_mode = "S"
            self.otpVerifyRequest.validateData()
        }
    }
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OTPVerificationView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
extension OTPVerificationView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension OTPVerificationView : OTPVerifyRequestDelegate {
    func invalidOTPVerifyData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func otpVerifyValidationPassed() {
        self.validateOTP(enteredOTP: self.createOtp)
    }
}
