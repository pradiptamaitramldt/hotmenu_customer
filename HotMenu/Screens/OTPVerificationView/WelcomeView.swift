//
//  WelcomeView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 13/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps

class WelcomeView: UIViewController {

    @IBOutlet weak var imageView_welcome: UIImageView!
    @IBOutlet weak var buttonNext: CustomButton!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var labelWelcome: CustomLabel!
    
    var locationManager: CLLocationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    func setup() {
        
        self.viewBG.applyGradient(colors: [UIColor.init(named: "TopGradientColor")!, UIColor.init(named: "MiddleGradientColor")!, UIColor.init(named: "BottomGradientColor")!])
        
        let userName = UserDefaultsManager.shared.getStringValue(forKey: .fullName)
        
        if userName != "" {
            let fullNameArr = userName.components(separatedBy: " ")
            let firstName: String = fullNameArr[0]
            self.labelWelcome.setTextFont(fontWeight: .regular , ofSize: 45)
            self.labelWelcome.text = "Hi \(firstName), Welcome to"
            self.labelWelcome.textColor = UIColor.init(named: "WhiteColor")
        }
        
        self.buttonNext.setNormalTitle(Strings.gpsButton)
        self.buttonNext.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonNext.cornerRadius = 10
        self.buttonNext.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonNext.setTitleFont(fontWeight: .medium, ofSize: 17.0)
    }
    
    private func getCurerntLocation() {
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            self.locationManager.requestAlwaysAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                locationManager.startUpdatingLocation()
            }
        } else if status == .denied {
            self.createSettingsAlertController(title: Strings.AppName, message: Strings.locationEnableErrorMsg)
        } else {
            self.locationManager.requestAlwaysAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    func createSettingsAlertController(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func buttonSkipAction(_ sender: Any) {
        let homeView = HomeView.instantiateFromAppStoryboard(appStoryboard: .home)
        self.navigationController?.pushViewController(homeView, animated: true)
    }
    
    @IBAction func buttonGPSAction(_ sender: Any) {
        self.getCurerntLocation()
        
    }
}

extension WelcomeView : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        locationManager.stopUpdatingLocation()
        Utility.lattitude = locValue.latitude
        Utility.longitude = locValue.longitude
        
        let homeView = HomeView.instantiateFromAppStoryboard(appStoryboard: .home)
        self.navigationController?.pushViewController(homeView, animated: true)
    }
}
