//
//  OTPVerifyModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 14/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct OTPVerifyRequest {
    
    var userID: String
    var sid: String
    var verification_code: String
    var device_token: String
    var app_type: String
    var push_mode: String
    
    var delegate: OTPVerifyRequestDelegate?
    
    init() {
        self.userID = ""
        self.sid = ""
        self.verification_code = ""
        self.device_token = ""
        self.app_type = ""
        self.push_mode = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.verification_code != "" {
            delegate.otpVerifyValidationPassed()
        }else{
            delegate.invalidOTPVerifyData(message: ErrorMessages.invalidOTP)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        if UserDefaultsManager.shared.getStringValue(forKey: .deviceToken) != "" {
            tempDictionary["deviceToken"] = UserDefaultsManager.shared.getStringValue(forKey: .deviceToken)
        } else {
            tempDictionary["deviceToken"] = "12345"
        }
        tempDictionary["appType"] = Strings.appType
        tempDictionary["pushMode"] = self.push_mode
        tempDictionary["userId"] = self.userID
        tempDictionary["verificationCode"] = self.verification_code
        tempDictionary["sid"] = self.sid
        
        return tempDictionary
    }
}

protocol OTPVerifyRequestDelegate {
    
    func invalidOTPVerifyData(message: String)
    
    func otpVerifyValidationPassed()
}

// MARK: - OTPVerifyResponseModel
struct OTPVerifyResponseModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String?
    let responseData: OTPVerifyResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct OTPVerifyResponseData: Codable {
    let userDetails: OTPVerifyUserDetails
    let authToken: String
}

// MARK: - UserDetails
struct OTPVerifyUserDetails: Codable {
    let fullName, email, countryCode, phone: String
    let socialID, customerID, loginID: String
    let profileImage: String
    let userType, loginType: String

    enum CodingKeys: String, CodingKey {
        case fullName, email, countryCode, phone
        case socialID = "socialId"
        case customerID = "customerId"
        case loginID = "loginId"
        case profileImage, userType, loginType
    }
}
struct ResendOTPVerifyRequest {
    
    var userID: String
    
    var delegate: OTPVerifyRequestDelegate?
    
    init() {
        self.userID = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        
        var tempDictionary: [String: Any] = [:]
        tempDictionary["userId"] = self.userID
        return tempDictionary
    }
}

// MARK: - ResendOTPVerifyResponseModel
struct ResendOTPVerifyResponseModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String?
    let responseData: ResendOTPResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct ResendOTPResponseData: Codable {
    let userID, sid: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case sid
    }
}
