//
//  OrderItemCell.swift
//  AddCard
//
//  Created by BrainiumSSD on 10/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit

class OrderItemCell: UITableViewCell {

    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var imageViewNonVeg: UIImageView!
    @IBOutlet weak var labelMenuTitle: CustomLabel!
    @IBOutlet weak var labelItemCount: CustomLabel!
    @IBOutlet weak var labelPrice: CustomLabel!
    @IBOutlet weak var labelOrderID: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
