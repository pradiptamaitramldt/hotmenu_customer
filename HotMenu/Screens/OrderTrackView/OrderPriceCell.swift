//
//  OrderPriceCell.swift
//  AddCard
//
//  Created by BrainiumSSD on 10/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit

protocol CancelOrderDelegate {
    func cancelOrder(_ sender : UIButton)
}

class OrderPriceCell: UITableViewCell {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var labelCartTotalTitle: CustomLabel!
    @IBOutlet weak var labelCartTotlaValue: CustomLabel!
    @IBOutlet weak var labelTaxTitle: CustomLabel!
    @IBOutlet weak var labelTaxValue: CustomLabel!
    @IBOutlet weak var labelDeliveryTitle: CustomLabel!
    @IBOutlet weak var labelDeliveryValue: CustomLabel!
    @IBOutlet weak var lavelTotalCartTitle: CustomLabel!
    @IBOutlet weak var labelTotalCartValue: CustomLabel!
    @IBOutlet weak var buttonCancelOrder: CustomButton!
    
    var delegate : CancelOrderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonCancelOrderAction(_ sender: Any) {
        self.delegate?.cancelOrder(sender as! UIButton)
    }
    
}
