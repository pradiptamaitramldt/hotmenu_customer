//
//  OrderProgressCell.swift
//  AddCard
//
//  Created by BrainiumSSD on 10/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit

class OrderProgressCell: UITableViewCell {

    @IBOutlet weak var dot1: UIImageView!
    @IBOutlet weak var labelOrderStatus1: CustomLabel!
    @IBOutlet weak var labelOrderDate1: CustomLabel!
    @IBOutlet weak var line1: UIImageView!
    @IBOutlet weak var dot2: UIImageView!
    @IBOutlet weak var labelOrderStatus2: CustomLabel!
    @IBOutlet weak var line2: UIImageView!
    @IBOutlet weak var labelOrderDate2: CustomLabel!
    @IBOutlet weak var dot3: UIImageView!
    @IBOutlet weak var labelOrderStatus3: CustomLabel!
    @IBOutlet weak var line3: UIImageView!
    @IBOutlet weak var labelOrderDate3: CustomLabel!
    @IBOutlet weak var dot4: UIImageView!
    @IBOutlet weak var labelOrderStatus4: CustomLabel!
    @IBOutlet weak var labelOrderDate4: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
