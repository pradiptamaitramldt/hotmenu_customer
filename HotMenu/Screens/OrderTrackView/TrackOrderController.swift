//
//  TrackOrderController.swift
//  AddCard
//
//  Created by BrainiumSSD on 10/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit
import AMShimmer

class TrackOrderController: UIViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var table_view: UITableView!
    
    private var webService = MyOrderAPI()
    private var trackOrderRequest = TrackOrderRequest()
    
    var menuOrderList: [TrackOrderMenu] = []
    var orderStatus: [OrderLog] = []
    var timer: Timer?
    var finalAmount: String = ""
    var cartTotal: String = ""
    
    var orderId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.table_view.delegate = self
        self.table_view.dataSource = self
        DispatchQueue.main.async {
            AMShimmer.start(for: self.table_view)
        }
        
        self.trackOrder()
        self.timer = Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(self.trackOrder) , userInfo: nil, repeats: true)
    }
    
    func setup() {
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.trackOrderHeader
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
    }
    
    @objc func trackOrder() {
//        self.dataStartedLoading()
        self.trackOrderRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.trackOrderRequest.orderID = self.orderId
        self.webService.trackOrder(trackOrderRequest: self.trackOrderRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.table_view)
            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.menuOrderList = data.cartID?.menus ?? []
                self.orderStatus = data.orderLog ?? []
                self.cartTotal = "\(data.cartID?.cartTotal ?? 0)"
                self.finalAmount = "\(data.finalAmount ?? 0)"
                
                DispatchQueue.main.async {
                    self.table_view.reloadData()
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func cancelOrder() {
        self.dataStartedLoading()
        self.trackOrderRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.trackOrderRequest.orderID = self.orderId
        self.webService.cancelOrder(trackOrderRequest: self.trackOrderRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func convertDateFormat(dateTime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let orderDate = dateFormatter.date(from: dateTime)!
        print(orderDate)
        dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
        let finalOderDateStr = dateFormatter.string(from: orderDate)
        return finalOderDateStr
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true )
    }
    
}
extension TrackOrderController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 150
        }
        else if indexPath.section == 1{
            return 260
        }
        else {
           return 320
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.menuOrderList.count
        }else if section == 1 {
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let cell: RouteCell = tableView.dequeueReusableCell(withIdentifier: "RouteCell") as! RouteCell
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderItemCell") as! OrderItemCell
                 
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            let itemImage = self.menuOrderList[indexPath.row].menuID?.itemID?.menuImage ?? ""
            if itemImage != "" {
                cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
            }
            cell.imageViewMenuItem.contentMode = .scaleAspectFill
            
            cell.labelMenuTitle.text = self.menuOrderList[indexPath.row].menuID?.itemID?.itemName
            cell.labelItemCount.text = "\(self.menuOrderList[indexPath.row].menuQuantity ?? 0) items"
            cell.labelPrice.text = "₹ \(self.menuOrderList[indexPath.row].menuAmount ?? 0)"
            cell.labelOrderID.text = "Order Id: \(self.orderId)"
            
            cell.labelMenuTitle.setTextFont(fontWeight: .medium, ofSize: 14)
            cell.labelItemCount.setTextFont(fontWeight: .regular, ofSize: 11)
            cell.labelPrice.setTextFont(fontWeight: .regular, ofSize: 11)
            cell.labelOrderID.setTextFont(fontWeight: .regular, ofSize: 12)
            
            cell.labelMenuTitle.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelItemCount.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelPrice.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelOrderID.textColor = UIColor.init(named: "HomeTextLightColor")
            
            let menuType = self.menuOrderList[indexPath.row].menuID?.itemID?.type
            if menuType == "NON VEG" {
                cell.imageViewNonVeg.image = UIImage(named: "nonveg")
            }else{
                cell.imageViewNonVeg.image = UIImage(named: "veg")
            }
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderProgressCell") as! OrderProgressCell
           
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            cell.labelOrderStatus1.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelOrderStatus2.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelOrderStatus3.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelOrderStatus4.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelOrderDate1.setTextFont(fontWeight: .light, ofSize: 10)
            cell.labelOrderDate2.setTextFont(fontWeight: .light, ofSize: 10)
            cell.labelOrderDate3.setTextFont(fontWeight: .light, ofSize: 10)
            cell.labelOrderDate4.setTextFont(fontWeight: .light, ofSize: 10)
            
            cell.labelOrderStatus1.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelOrderStatus2.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelOrderStatus3.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelOrderStatus4.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelOrderDate1.textColor = UIColor.init(named: "HomeTextLightColor")
            cell.labelOrderDate2.textColor = UIColor.init(named: "HomeTextLightColor")
            cell.labelOrderDate3.textColor = UIColor.init(named: "HomeTextLightColor")
            cell.labelOrderDate4.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelOrderStatus1.text = "Order Placed"
            cell.labelOrderStatus2.text = "Order Confirmed"
            cell.labelOrderStatus3.text = "Order is ready at the restaurant"
            cell.labelOrderStatus4.text = "Rider is picking up your order"
            
            cell.dot1.backgroundColor = UIColor.init(named: "HomeTextLightColor")
            cell.line1.backgroundColor = UIColor.init(named: "HomeTextLightColor")
            cell.dot2.backgroundColor = UIColor.init(named: "HomeTextLightColor")
            cell.line2.backgroundColor = UIColor.init(named: "HomeTextLightColor")
            cell.dot3.backgroundColor = UIColor.init(named: "HomeTextLightColor")
            cell.line3.backgroundColor = UIColor.init(named: "HomeTextLightColor")
            cell.dot4.backgroundColor = UIColor.init(named: "HomeTextLightColor")
            
            let orderCount = self.orderStatus.count
            
            if orderCount == 1 {
                cell.dot1.backgroundColor = UIColor.init(named: "YellowColor")
                cell.labelOrderDate1.text = self.convertDateFormat(dateTime: self.orderStatus[0].logTime ?? "")
            }else if orderCount == 2 {
                cell.dot1.backgroundColor = UIColor.init(named: "YellowColor")
                cell.line1.backgroundColor = UIColor.init(named: "YellowColor")
                cell.dot2.backgroundColor = UIColor.init(named: "YellowColor")
                
                cell.labelOrderDate1.text = self.convertDateFormat(dateTime: self.orderStatus[0].logTime ?? "")
                cell.labelOrderDate2.text = self.convertDateFormat(dateTime: self.orderStatus[1].logTime ?? "")
                
            }else if orderCount == 3 {
                cell.dot1.backgroundColor = UIColor.init(named: "YellowColor")
                cell.line1.backgroundColor = UIColor.init(named: "YellowColor")
                cell.dot2.backgroundColor = UIColor.init(named: "YellowColor")
                cell.line2.backgroundColor = UIColor.init(named: "YellowColor")
                cell.dot3.backgroundColor = UIColor.init(named: "YellowColor")
                
                cell.labelOrderDate1.text = self.convertDateFormat(dateTime: self.orderStatus[0].logTime ?? "")
                cell.labelOrderDate2.text = self.convertDateFormat(dateTime: self.orderStatus[1].logTime ?? "")
                cell.labelOrderDate3.text = self.convertDateFormat(dateTime: self.orderStatus[2].logTime ?? "")
            }else if orderCount == 4 {
                cell.dot1.backgroundColor = UIColor.init(named: "YellowColor")
                cell.line1.backgroundColor = UIColor.init(named: "YellowColor")
                cell.dot2.backgroundColor = UIColor.init(named: "YellowColor")
                cell.line2.backgroundColor = UIColor.init(named: "YellowColor")
                cell.dot3.backgroundColor = UIColor.init(named: "YellowColor")
                cell.line3.backgroundColor = UIColor.init(named: "YellowColor")
                cell.dot4.backgroundColor = UIColor.init(named: "YellowColor")
                
                cell.labelOrderDate1.text = self.convertDateFormat(dateTime: self.orderStatus[0].logTime ?? "")
                cell.labelOrderDate2.text = self.convertDateFormat(dateTime: self.orderStatus[1].logTime ?? "")
                cell.labelOrderDate3.text = self.convertDateFormat(dateTime: self.orderStatus[2].logTime ?? "")
                cell.labelOrderDate4.text = self.convertDateFormat(dateTime: self.orderStatus[3].logTime ?? "")
            }
            
            return cell
        }
        else {
             let cell = tableView.dequeueReusableCell(withIdentifier: "OrderPriceCell") as! OrderPriceCell
                      
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            cell.labelHeader.text = "Price Details"
            cell.labelHeader.setTextFont(fontWeight: .regular, ofSize: 12)
            cell.labelHeader.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelCartTotalTitle.text = "Cart Total"
            cell.labelCartTotalTitle.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelCartTotalTitle.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelCartTotlaValue.text = "₹ \(self.cartTotal)"
            cell.labelCartTotlaValue.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelCartTotlaValue.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelTaxTitle.text = "Tax"
            cell.labelTaxTitle.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelTaxTitle.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelTaxValue.text = "₹ 0.0"
            cell.labelTaxValue.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelTaxValue.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelDeliveryTitle.text = "Delivery"
            cell.labelDeliveryTitle.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelDeliveryTitle.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelDeliveryValue.text = "Free"
            cell.labelDeliveryValue.setTextFont(fontWeight: .regular, ofSize: 14)
            cell.labelDeliveryValue.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.lavelTotalCartTitle.text = "Subtotal"
            cell.lavelTotalCartTitle.setTextFont(fontWeight: .medium, ofSize: 16)
            cell.lavelTotalCartTitle.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelTotalCartValue.text = "₹ \(self.finalAmount)"
            cell.labelTotalCartValue.setTextFont(fontWeight: .medium, ofSize: 16)
            cell.labelTotalCartValue.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.buttonCancelOrder.setNormalTitle(Strings.cancelOrderButton)
            cell.buttonCancelOrder.backgroundColor = UIColor.init(named: "ButtonColor")
            cell.buttonCancelOrder.cornerRadius = 10
            cell.buttonCancelOrder.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
            cell.buttonCancelOrder.setTitleFont(fontWeight: .medium, ofSize: 17.0)
            cell.delegate = self
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("index_row", indexPath.row)
        print("index_section", indexPath.section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = .clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
           return 10
        }
        else {
            return 10
        }
        
    }
}
extension TrackOrderController: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension TrackOrderController: CancelOrderDelegate {
    func cancelOrder(_ sender: UIButton) {
        DispatchQueue.main.async {
            let successAlert = UIAlertController(title: "Alert", message: "Do you want to cancel this order?", preferredStyle: UIAlertController.Style.alert)
            
            successAlert.addAction(UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction!) in
                self.cancelOrder()
            })
            
            successAlert.addAction(UIAlertAction(title: "NO", style: .default) { (action:UIAlertAction!) in
                
            })
            
            self.present(successAlert, animated: true)
        }
    }
}
