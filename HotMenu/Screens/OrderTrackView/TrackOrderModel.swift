//
//  TrackOrderModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 18/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct TrackOrderRequest {
    
    var userID: String
    var orderID: String
            
    init() {
        self.userID = ""
        self.orderID = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        tempDictionary["orderId"] = self.orderID
        
        return tempDictionary
    }
}

/// MARK: - TrackOrderResponseModel
struct TrackOrderResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: TrackOrderResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct TrackOrderResponseData: Codable {
    let paymentInfo: PaymentInfo?
    let orderPrepTime, orderDeliveryTime: String?
    let promoCodeID: JSONNull?
    let restaurantID: String?
    let deliveryAddressID: String?
    let orderStatus: Int?
    let id, orderNo, customerID: String?
    let cartID: CartID?
    let finalAmount: Int?
    let orderLog: [OrderLog]?
    let createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case paymentInfo, orderPrepTime, orderDeliveryTime
        case promoCodeID = "promoCodeId"
        case restaurantID = "restaurantId"
        case deliveryAddressID = "deliveryAddressId"
        case orderStatus
        case id = "_id"
        case orderNo
        case customerID = "customerId"
        case cartID = "cartId"
        case finalAmount, orderLog, createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - CartID
struct CartID: Codable {
    let cartTotal: Int?
    let id, restaurantID: String?
    let menus: [TrackOrderMenu]?

    enum CodingKeys: String, CodingKey {
        case cartTotal
        case id = "_id"
        case restaurantID = "restaurantId"
        case menus
    }
}

// MARK: - Menu
struct TrackOrderMenu: Codable {
    let menuAddedDate, id: String?
    let menuID: TrackOrderMenuID?
    let menuAmount, menuQuantity, menuTotal: Int?

    enum CodingKeys: String, CodingKey {
        case menuAddedDate
        case id = "_id"
        case menuID = "menuId"
        case menuAmount, menuQuantity, menuTotal
    }
}

// MARK: - MenuID
struct TrackOrderMenuID: Codable {
    let id: String?
    let itemID: TrackOrderItemID?
    let restaurantID, createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case itemID = "itemId"
        case restaurantID = "restaurantId"
        case createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - ItemID
struct TrackOrderItemID: Codable {
    let id, itemName, type: String?
    let menuImage: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case itemName, type, menuImage
    }
}

// MARK: - OrderLog
struct OrderLog: Codable {
    let logTime, id, logDetail: String?

    enum CodingKeys: String, CodingKey {
        case logTime
        case id = "_id"
        case logDetail
    }
}

// MARK: - PaymentInfo
struct PaymentInfo: Codable {
    let paymentStatus, paymentID: String?

    enum CodingKeys: String, CodingKey {
        case paymentStatus
        case paymentID = "paymentId"
    }
}

// MARK: - CancelOrderResponseModel
struct CancelOrderResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: CancelResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct CancelResponseData: Codable {
    let paymentInfo: CancelPaymentInfo?
    let promoCodeID: String?
    let orderStatus: Int?
    let id, orderNo, customerID, cartID: String?
    let finalAmount: Double?
    let orderLog: [CancelOrderLog]?
    let createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case paymentInfo
        case promoCodeID = "promoCodeId"
        case orderStatus
        case id = "_id"
        case orderNo
        case customerID = "customerId"
        case cartID = "cartId"
        case finalAmount, orderLog, createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - OrderLog
struct CancelOrderLog: Codable {
    let logTime, id, logDetail: String?

    enum CodingKeys: String, CodingKey {
        case logTime
        case id = "_id"
        case logDetail
    }
}

// MARK: - PaymentInfo
struct CancelPaymentInfo: Codable {
    let paymentStatus, paymentID: String?

    enum CodingKeys: String, CodingKey {
        case paymentStatus
        case paymentID = "paymentId"
    }
}
