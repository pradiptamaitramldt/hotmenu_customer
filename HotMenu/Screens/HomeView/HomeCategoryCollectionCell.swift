//
//  HomeCategoryCollectionCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 20/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class HomeCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imageViewCategory: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
}
