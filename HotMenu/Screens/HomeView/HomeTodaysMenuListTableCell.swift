//
//  HomeTodaysMenuListTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 20/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

protocol TodaysMenuTableDelegate {
    func markAsFavouriteTable(_ sender : UIButton)
    func minusCartItemTable(_ sender : UIButton)
    func plusCartItemTable(_ sender : UIButton)
    func addToCartItemTable(_ sender : UIButton)
//    func updateTable()
}

class HomeTodaysMenuListTableCell: UITableViewCell {

    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var imageViewVeg: UIImageView!
    @IBOutlet weak var imageViewFavourite: UIImageView!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelActualPrice: UILabel!
    @IBOutlet weak var labelDiscountedPrice: UILabel!
    @IBOutlet weak var buttonAddCart: UIButton!
    @IBOutlet weak var viewIncreaseCart: UIView!
    @IBOutlet weak var labelCartCount: UILabel!
    @IBOutlet weak var imageViewLine: UIImageView!
    @IBOutlet weak var buttonFavourite: UIButton!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var viewShadowBG: UIView!
    
    var delegate : TodaysMenuTableDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    override func layoutIfNeeded() {
//        super.layoutIfNeeded()
//        self.delegate?.updateTable()
//    }

    @IBAction func buttonFavouriteAction(_ sender: Any) {
        self.delegate?.markAsFavouriteTable(sender as! UIButton)
    }
    @IBAction func buttonAddToCartItem(_ sender: Any) {
        self.delegate?.addToCartItemTable(sender as! UIButton)
    }
    @IBAction func buttonDecreaseItemCartAction(_ sender: Any) {
        self.delegate?.minusCartItemTable(sender as! UIButton)
    }
    @IBAction func buttonIncreaseItemCartAction(_ sender: Any) {
        self.delegate?.plusCartItemTable(sender as! UIButton)
    }
}
