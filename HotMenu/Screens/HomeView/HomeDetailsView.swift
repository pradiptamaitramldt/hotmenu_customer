//
//  HomeDetailsView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 22/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class HomeDetailsView: UITableViewController {

    @IBOutlet weak var imageViewMenu: UIImageView!
    @IBOutlet weak var imageViewVeg: UIImageView!
    @IBOutlet weak var imageViewFavourite: UIImageView!
    @IBOutlet weak var labelTitle: CustomLabel!
    @IBOutlet weak var labelActualPrice: CustomLabel!
    @IBOutlet weak var labelDiscountedPrice: CustomLabel!
    @IBOutlet weak var imageViewlIne: UIImageView!
    @IBOutlet weak var labelRating: CustomLabel!
    @IBOutlet weak var labelDetailsHeader: CustomLabel!
    @IBOutlet weak var labelDescription: CustomLabel!
    @IBOutlet weak var buttonAddToCart: CustomButton!
    @IBOutlet weak var viewCartBG: UIView!
    @IBOutlet weak var buttonMinus: CustomButton!
    @IBOutlet weak var buttonPlus: CustomButton!
    @IBOutlet weak var labelCartCount: CustomLabel!
    @IBOutlet weak var labelTotalCartCount: CustomLabel!
    @IBOutlet weak var buttonReviews: CustomButton!
    
    private var webService = DashboardAPI()
    private var favouriteRequest = MakeFavouriteRequest()
    private var unFavouriteRequest = MakeFavouriteRequest()
    private var cartWebService = CartAPI()
    private var addCartRequest = AddCartRequest()
    private var updateCartRequest = UpdateCartRequest()
    private var removeCartRequest = RemoveCartRequest()
    
    var menuItem: TopDish?
    var imageURL: String = ""
    var isMenuFavourite: Bool = false
    var totalCartCount: String = ""
    var cartId: String = ""
    var itemCartCount: Int = 0
    var reviewArray: [ViewRatingResponseDatum] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.rowHeight = 40.0
        
        self.labelTotalCartCount.isHidden = true
        self.labelTotalCartCount.setTextFont(fontWeight: .medium, ofSize: 12)
        self.labelTotalCartCount.textColor = UIColor.init(named: "WhiteColor")
        self.labelTotalCartCount.backgroundColor = UIColor.init(named: "ButtonColor")

        self.setup()
    }
    
    // MARK: - General Methods
    func setup() {
        let itemImage = self.menuItem?.itemID?.menuImage ?? ""
        if itemImage != "" {
            self.imageViewMenu.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
        }
        self.imageViewMenu.contentMode = .scaleAspectFill
        self.imageViewMenu.maskToBound = true
        let isVegItem = self.menuItem?.itemID?.type
        if isVegItem == "NON VEG" {
            self.imageViewVeg.image = UIImage(named: "nonveg")
        }else{
            self.imageViewVeg.image = UIImage(named: "veg")
        }
        
        if self.totalCartCount == "" || self.totalCartCount == "0" {
            self.labelTotalCartCount.text = "0"
            self.labelTotalCartCount.isHidden = true
        }else{
            self.labelTotalCartCount.isHidden = false
            self.labelTotalCartCount.text = self.totalCartCount
        }
        
        self.labelTitle.setTextFont(fontWeight: .medium, ofSize: 18)
        self.labelTitle.text = self.menuItem?.itemID?.itemName
        self.labelTitle.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelActualPrice.setTextFont(fontWeight: .light, ofSize: 12)
        self.labelActualPrice.text = "₹\(self.menuItem?.itemID?.price ?? 0)"
        self.labelActualPrice.textColor = UIColor.init(named: "HomeTextLightColor")
        
        let discountType = self.menuItem?.itemID?.discountType
        let price = self.menuItem?.itemID?.price ?? 0
        let discountPrice = self.menuItem?.itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        self.labelDiscountedPrice.setTextFont(fontWeight: .medium, ofSize: 14)
        self.labelDiscountedPrice.text = "₹\(offerPrice )"
        self.labelDiscountedPrice.textColor = UIColor.init(named: "HomeTextLightColor")
        
        self.labelRating.setTextFont(fontWeight: .regular, ofSize: 12)
        self.labelRating.text = "\(self.menuItem?.rating ?? 0)"
        self.labelRating.textColor = UIColor.init(named: "HomeTextColor")
        
        self.buttonReviews.setNormalTitle("\(self.menuItem?.review ?? 0) Reviews")
        
        self.labelDetailsHeader.setTextFont(fontWeight: .medium, ofSize: 14)
        self.labelDetailsHeader.text = "Details"
        self.labelDetailsHeader.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelCartCount.setTextFont(fontWeight: .bold, ofSize: 25)
        self.labelCartCount.text = "1"
        self.labelCartCount.textColor = .white
        
        self.labelDescription.setTextFont(fontWeight: .medium, ofSize: 12)
        self.labelDescription.text = self.menuItem?.itemID?.description
        self.labelDescription.textColor = UIColor.init(named: "HomeTextLightColor")
        self.labelDescription.numberOfLines = 0
        self.labelDescription.sizeToFit()
        
        let isFavItem = self.menuItem?.isFavorite
        if isFavItem == 1 {
            self.isMenuFavourite = true
            self.imageViewFavourite.image = UIImage(named: "heart_red")
        }else{
            self.isMenuFavourite = false
            self.imageViewFavourite.image = UIImage(named: "heart_white")
        }
        
        self.buttonAddToCart.setNormalTitle(Strings.addToCartButton)
        self.buttonAddToCart.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonAddToCart.cornerRadius = 10
        self.buttonAddToCart.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonAddToCart.setTitleFont(fontWeight: .medium, ofSize: 18.0)
        
        self.buttonReviews.setTitleColor(UIColor.init(named: "HomeTextColor"), for: .normal)
        self.buttonReviews.setTitleFont(fontWeight: .regular, ofSize: 14.0)
        
        self.viewCartBG.backgroundColor = UIColor.init(named: "ButtonColor")
        
        self.buttonMinus.backgroundColor = UIColor.init(named: "ButtonCartColor")
        self.buttonMinus.cornerRadius = 10
        
        self.buttonPlus.backgroundColor = UIColor.init(named: "ButtonCartColor")
        self.buttonPlus.cornerRadius = 10
        
        let cartQuantity = self.menuItem?.cartMenuQuantity
        self.itemCartCount = self.menuItem?.cartMenuQuantity ?? 0
        if cartQuantity == 0 {
            self.buttonAddToCart.isHidden = false
            self.viewCartBG.isHidden = true
        }else{
            self.buttonAddToCart.isHidden = true
            self.viewCartBG.isHidden = false
            self.labelCartCount.text = "\(cartQuantity ?? 0)"
        }
    }
    
    func startShimmering() {
        DispatchQueue.main.async {
//            self.imageViewMenu.startShimmeringAnimation()
//            self.labelTitle.startShimmeringAnimation()
//            self.labelActualPrice.startShimmeringAnimation()
//            self.labelDiscountedPrice.startShimmeringAnimation()
            self.buttonAddToCart.startShimmeringAnimation()
            self.viewCartBG.startShimmeringAnimation()
//            self.labelDetailsHeader.startShimmeringAnimation()
//            self.labelDescription.startShimmeringAnimation()
        }
    }
    
    func stopShimmering() {
        DispatchQueue.main.async {
//            self.imageViewMenu.stopShimmeringAnimation()
//            self.labelTitle.stopShimmeringAnimation()
//            self.labelActualPrice.stopShimmeringAnimation()
//            self.labelDiscountedPrice.stopShimmeringAnimation()
            self.buttonAddToCart.stopShimmeringAnimation()
            self.viewCartBG.stopShimmeringAnimation()
//            self.labelDetailsHeader.stopShimmeringAnimation()
//            self.labelDescription.stopShimmeringAnimation()
        }
    }
    
    func makeFavourite(menuID: String) {
        self.favouriteRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId) //"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.favouriteRequest.menuId = menuID
        self.startShimmering()
        self.webService.markFavourite(favouriteRequest: self.favouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.stopShimmering()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                let menuId = data.menuID
                print(menuId ?? "")
                self.isMenuFavourite = true
                DispatchQueue.main.async {
                    self.imageViewFavourite.image = UIImage(named: "heart_red")
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func makeUnfavourite(menuID: String) {
        self.unFavouriteRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.unFavouriteRequest.menuId = menuID
        self.startShimmering()
        self.webService.markUnfavourite(favouriteRequest: self.unFavouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.stopShimmering()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.isMenuFavourite = false
                DispatchQueue.main.async {
                    self.imageViewFavourite.image = UIImage(named: "heart_white")
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func addToCart(menuID: String, menuAmount: String, menuQuantity: String, vendorID: String) {
        self.addCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addCartRequest.menuId = menuID
        self.addCartRequest.menuAmount = menuAmount
        self.addCartRequest.menuQuantity = menuQuantity
        self.addCartRequest.vendorId = vendorID
        self.startShimmering()
        self.cartWebService.addToCart(addCartRequest: self.addCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.stopShimmering()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                let statusCode = successDashboardResponse.statuscode
                self.cartId = data.id ?? ""
                if statusCode == 300 {
                    let message = successDashboardResponse.message
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message ?? "")
                }else if statusCode == 200 {
                    DispatchQueue.main.async {
                        self.labelTotalCartCount.text = "\(data.cartTotal ?? 0)"
                        self.viewCartBG.isHidden = false
                        self.buttonAddToCart.isHidden = true
                        self.labelCartCount.text = "1"
                        var totalcount = Int(self.totalCartCount)
                        totalcount! += 1
                        self.totalCartCount = "\(totalcount ?? 0)"
                        if self.itemCartCount != 0 && totalcount ?? 0 < 1 {
                            self.itemCartCount = totalcount ?? 0
                        }
                        
                        self.labelTotalCartCount.text = self.totalCartCount
                        self.labelTotalCartCount.isHidden = false
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func updateCart(cartID: String, menuID: String, menuAmount: String, menuQuantity: String, isAdd: Bool, vendorId: String) {
        self.updateCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.updateCartRequest.menuId = menuID
        self.updateCartRequest.menuAmount = menuAmount
        self.updateCartRequest.menuQuantity = menuQuantity
        self.updateCartRequest.cartId = cartID
        self.updateCartRequest.vendorId = vendorId
        self.startShimmering()
        self.cartWebService.updateCart(updateCartRequest: self.updateCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.stopShimmering()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                DispatchQueue.main.async {
                    if isAdd == true {
                        self.labelTotalCartCount.isHidden = false
                        self.itemCartCount += 1
                        self.labelCartCount.text = "\(self.itemCartCount)"
                        var totalcount = Int(self.totalCartCount)
                        totalcount! += 1
                        self.totalCartCount = "\(totalcount ?? 0)"
                        self.labelTotalCartCount.text = self.totalCartCount
                    }else{
                        var totalcount = Int(self.totalCartCount)
                        totalcount! -= 1
                        self.totalCartCount = "\(totalcount ?? 0)"
                        self.labelTotalCartCount.text = self.totalCartCount
                        if self.itemCartCount == 1 {
                            self.buttonAddToCart.isHidden = false
                            self.viewCartBG.isHidden = true
                            self.itemCartCount -= 1
                            self.labelCartCount.text = "\(self.itemCartCount)"
                            self.labelTotalCartCount.isHidden = true
                        }else{
                            self.labelTotalCartCount.isHidden = false
                            self.itemCartCount -= 1
                            self.labelCartCount.text = "\(self.itemCartCount)"
                        }
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func removeCart(cartID: String, menuID: String, vendorId: String) {
        self.removeCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.removeCartRequest.menuId = menuID
        self.removeCartRequest.cartId = cartID
        self.removeCartRequest.vendorId = vendorId
        self.startShimmering()
        self.cartWebService.removeCart(removeCartRequest: self.removeCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.stopShimmering()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                DispatchQueue.main.async {
                    var totalcount = Int(self.totalCartCount)
                    totalcount! -= 1
                    self.totalCartCount = "\(totalcount ?? 0)"
                    self.labelTotalCartCount.text = self.totalCartCount
                    self.buttonAddToCart.isHidden = false
                    self.viewCartBG.isHidden = true
                    self.itemCartCount = 0
                    self.labelCartCount.text = "\(self.itemCartCount)"
                    if totalcount == 0 {
                        self.labelTotalCartCount.isHidden = true
                    }
                }
                                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }

    // MARK: - UIButton Actions
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonCartAction(_ sender: Any) {
        if self.labelCartCount.text == "0" || self.labelCartCount.text == "" {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "Opps! Cart is empty\nPlease add something to the cart first.")
        }else{
            let cartView = CartView.instantiateFromAppStoryboard(appStoryboard: .cart)
            self.navigationController?.pushViewController(cartView, animated: true)
        }
    }
    @IBAction func buttonFavouriteAction(_ sender: Any) {
        if self.isMenuFavourite == true {
            self.makeUnfavourite(menuID: self.menuItem?.id ?? "")
        }else{
            self.makeFavourite(menuID: self.menuItem?.id ?? "")
        }
    }
    @IBAction func buttonAddtoCartAction(_ sender: Any) {
        
        if self.menuItem?.itemID?.isActive == true  {
            let menuID = self.menuItem?.id ?? ""
            
            let discountType = self.menuItem?.itemID?.discountType
            let price = self.menuItem?.itemID?.price ?? 0
            let discountPrice = self.menuItem?.itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice)"
            let vendorID = self.menuItem?.restaurantID ?? ""
            
            self.addToCart(menuID: menuID, menuAmount: menuAmount, menuQuantity: "1", vendorID: vendorID)
        }
        else {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: "This item is not available now")
        }
    }
    @IBAction func buttonMinusAction(_ sender: Any) {
        let menuID = self.menuItem?.id ?? ""
        
        let discountType = self.menuItem?.itemID?.discountType
        let price = self.menuItem?.itemID?.price ?? 0
        let discountPrice = self.menuItem?.itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        let menuQuantity: Int = self.itemCartCount
        let vendorID: String = self.menuItem?.restaurantID ?? ""
        
        if menuQuantity == 1 || menuQuantity == 0 {
            self.removeCart(cartID: self.cartId, menuID: menuID, vendorId: vendorID)
        }else{
            if menuQuantity > 1 {
                self.updateCart(cartID: self.cartId, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity-1)", isAdd: false, vendorId: vendorID)
            }
        }
    }
    @IBAction func buttonPlusAction(_ sender: Any) {
        
        if self.menuItem?.itemID?.isActive == true {
            let menuID = self.menuItem?.id ?? ""
            
            let discountType = self.menuItem?.itemID?.discountType
            let price = self.menuItem?.itemID?.price ?? 0
            let discountPrice = self.menuItem?.itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice)"
            let menuQuantity: Int = self.itemCartCount
            if self.itemCartCount == 0 {
                self.itemCartCount += 1
            }
            
            let vendorID: String = self.menuItem?.restaurantID ?? ""
            self.updateCart(cartID: self.cartId, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)", isAdd: true, vendorId: vendorID)
        }
        else {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "This item is not available now")
        }
        
    }
    @IBAction func buttonReviewsAction(_ sender: Any) {
        let ratingView = RatingView.instantiateFromAppStoryboard(appStoryboard: .rating)
        ratingView.menuID = self.menuItem?.id ?? ""
        self.navigationController?.pushViewController(ratingView, animated: true)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 330
        }else if indexPath.row == 1 {
            return 120
        } else if indexPath.row == 2 {
            return UITableView.automaticDimension
        }else{
            return 80
        }
    }
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HomeDetailsView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
