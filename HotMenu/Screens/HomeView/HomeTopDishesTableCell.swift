//
//  HomeTopDishesTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 20/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class HomeTopDishesTableCell: UITableViewCell {

    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var collection_viewTopDishes: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        self.collection_viewTopDishes.reloadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        self.collection_viewTopDishes.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
