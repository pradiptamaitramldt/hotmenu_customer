//
//  HomeTodaysMenuCollectionCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 20/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class HomeTodaysMenuCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewMenuSelectionBG: UIView!
    @IBOutlet weak var labelMenuTypeTitle: UILabel!
}
