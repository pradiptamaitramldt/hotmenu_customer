//
//  HomeModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 20/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct DashboardRequest {
    
    let userID: String
    let UserType: String
    let lattitude: String
    let longitude: String
        
//    init() {
//        self.userID = ""
//        self.UserType = ""
//        self.lattitude = ""
//        self.longitude = ""
//    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        tempDictionary["userType"] = Strings.userTypeCustomer
        tempDictionary["latitude"] = self.lattitude
        tempDictionary["longitude"] = self.longitude
        
        return tempDictionary
    }
}

// MARK: - DashboardResponseModel
struct DashboardResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: DashboardResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct DashboardResponseData: Codable {
    let toDaysMenu: [ToDaysMenu]?
    let topDishes: [TopDish]?
    let categoryData: [CategoryDatum]?
    let categoryImageURL: String?
    let cartCountTotal: Int?
    let cartID, vendorID: String?

    enum CodingKeys: String, CodingKey {
        case toDaysMenu, topDishes
        case categoryData = "category_data"
        case categoryImageURL = "category_imageUrl"
        case cartCountTotal
        case cartID = "cartId"
        case vendorID = "vendorId"
    }
}

// MARK: - CategoryDatum
struct CategoryDatum: Codable {
    let id, name, image: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, image
    }
}

// MARK: - ToDaysMenu
struct ToDaysMenu: Codable {
    let name: String?
    let value: [TopDish]?
}

//// MARK: - TopDish
//struct TopDish: Codable {
//    var isActive: Bool?
//    var id, itemName, categoryID, restaurantID: String?
//    var type: String?
//    var price, rating: Double?
//    var waitingTime: Int?
//    var menuImage: String?
//    var createdAt, updatedAt: String?
//    var v, review: Int?
//    var mealTypeID: MealTypeID?
//    var topDishDescription: String?
//    var offerPrice, isFavorite, cartMenuQuantity: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case isActive
//        case id = "_id"
//        case itemName
//        case categoryID = "categoryId"
//        case restaurantID = "restaurantId"
//        case type, price, waitingTime, menuImage, createdAt, updatedAt, review, rating
//        case v = "__v"
//        case mealTypeID = "mealTypeId"
//        case topDishDescription = "description"
//        case offerPrice, isFavorite, cartMenuQuantity
//    }
//}

// MARK: - Value
struct TopDish: Codable {
    let id: String?
    let itemID: ItemID?
    let restaurantID, createdAt, updatedAt: String?
    let v, isFavorite, review: Int?
    let rating: Int
    let cartMenuQuantity: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case itemID = "itemId"
        case restaurantID = "restaurantId"
        case createdAt, updatedAt
        case v = "__v"
        case isFavorite, review, rating, cartMenuQuantity
    }
}

// MARK: - ItemID
struct ItemID: Codable {
    let discountType: String?
    let discountAmount: Int?
    let validityFrom, validityTo: String?
    let isActive, isApprove: Bool
    let popularity: Int?
    let id, itemName, categoryID, description: String?
    let mealTypeID: MealTypeID?
    let type: String?
    let price, waitingTime: Int?
    let menuImage: String?
    let itemOptions: [ItemOption]
    let createdAt, updatedAt: String?
    let v: Int?
    let ingredients, recipe: String?

    enum CodingKeys: String, CodingKey {
        case discountType, discountAmount, validityFrom, validityTo, isActive, isApprove, popularity
        case id = "_id"
        case itemName, description
        case categoryID = "categoryId"
        case mealTypeID = "mealTypeId"
        case type, price, waitingTime, menuImage, itemOptions, createdAt, updatedAt
        case v = "__v"
        case ingredients, recipe
    }
}

// MARK: - ItemOption
struct ItemOption: Codable {
    let arrOptions: [ArrOption]
    let optionTitle: String?
    let isActive: IsActive
}

enum IsActive: Codable {
    case bool(Bool)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(IsActive.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for IsActive"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

// MARK: - ArrOption
struct ArrOption: Codable {
    let name: String?
    let isActive: Bool
}

// MARK: - MealTypeID
struct MealTypeID: Codable {
    var id, type: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type
    }
}

struct MakeFavouriteRequest {
    
    var customerId: String
    var menuId: String
    
    init() {
        self.customerId = ""
        self.menuId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["menuId"] = self.menuId
        
        return tempDictionary
    }
}

// MARK: - FavouriteResponseModel
struct FavouriteResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: FavouriteResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct FavouriteResponseData: Codable {
    let id, menuID, customerID, createdAt: String?
    let updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case menuID = "menuId"
        case customerID = "customerId"
        case createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - UnfavouriteResponseModel
struct UnfavouriteResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: UnfavouriteResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct UnfavouriteResponseData: Codable {
}
