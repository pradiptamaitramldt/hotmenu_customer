//
//  HomeTopDishesCollectionCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 20/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

protocol TopDishesCollectionDelegate {
    func markAsFavourite(_ sender : UIButton)
    func minusCartItem(_ sender : UIButton)
    func plusCartItem(_ sender : UIButton)
    func addToCartItem(_ sender : UIButton)
}

class HomeTopDishesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var imageViewVeg: UIImageView!
    @IBOutlet weak var imageViewFavourite: UIImageView!
    @IBOutlet weak var labelTitle: CustomLabel!
    @IBOutlet weak var labelDescription: CustomLabel!
    @IBOutlet weak var labelActualPrice: CustomLabel!
    @IBOutlet weak var labelDiscountedPrice: CustomLabel!
    @IBOutlet weak var buttonAddCart: UIButton!
    @IBOutlet weak var viewCartBG: UIView!
    @IBOutlet weak var labelCartCount: UILabel!
    @IBOutlet weak var buttonFavourite: UIButton!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var viewShadowBG: UIView!
    
    var delegate : TopDishesCollectionDelegate?
    
    @IBAction func buttonAddToCartAction(_ sender: Any) {
        self.delegate?.addToCartItem(sender as! UIButton)
    }
    @IBAction func buttonFavouriteAction(_ sender: Any) {
        self.delegate?.markAsFavourite(sender as! UIButton)
    }
    @IBAction func buttonMinesCartAction(_ sender: Any) {
        self.delegate?.minusCartItem(sender as! UIButton)
    }
    @IBAction func buttonPlusCartAction(_ sender: Any) {
        self.delegate?.plusCartItem(sender as! UIButton)
    }
}
