//
//  HomeNoItemTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 22/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class HomeNoItemTableCell: UITableViewCell {

    @IBOutlet weak var labelNoItemFound: CustomLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
