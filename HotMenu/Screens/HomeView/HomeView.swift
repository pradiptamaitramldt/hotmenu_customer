//
//  HomeView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 18/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps

class HomeView: UIViewController {
    
    @IBOutlet weak var labelLocation: CustomLabel!
    @IBOutlet weak var belIcon: UIImageView!
    @IBOutlet weak var cartIcon: UIImageView!
    @IBOutlet weak var buttonFoodSearch: UIButton!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var labelNoRestaurant: CustomLabel!
    @IBOutlet weak var labelCartCount: CustomLabel!
    
    private var webService = DashboardAPI()
    private var cartWebService = CartAPI()
    private var addCartRequest = AddCartRequest()
    private var updateCartRequest = UpdateCartRequest()
    private var removeCartRequest = RemoveCartRequest()
//    private var dashboardRequest = DashboardRequest()
    private var favouriteRequest = MakeFavouriteRequest()
    private var unFavouriteRequest = MakeFavouriteRequest()
    var collectionView_category : UICollectionView!
    var collectionView_topDishes : UICollectionView!
    var collectionView_todaysMenu : UICollectionView!
    var locationManager: CLLocationManager = CLLocationManager()
    var lattitude = Double()
    var longitude = Double()
    var categoryArray: [CategoryDatum] = []
    var topDishesArray: [TopDish] = []
    var todaysAllMenuArray: [ToDaysMenu] = []
    var todaysMenuCategoryArray: [String] = []
    var todaysMenuArray: [TopDish] = []
    var selectedIndexPath = IndexPath(row:0,section:0)
    var categoryImageUrl: String = ""
    var cart_id: String = ""
    var currentPlaceName: String = ""
    var currrentLatitude = CLLocationDegrees()
    var currentLongitude = CLLocationDegrees()
    var currentLocation: CLLocation?
    var geocoder = CLGeocoder()
    var currentAddress = ""
    var shouldFetchDashboardData: Bool = true
    var locationArr = [[String: Any]]()
    var selectedCoordinates: ((_ latitude: CLLocationDegrees?, _ longitude: CLLocationDegrees?, _ address: String, _ placename: String) -> Void)?
    var selectedIndex = Int()
    var isCategorySelected: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelNoRestaurant.isHidden = true
        
        self.belIcon.image = UIImage(named: "bell_menu")
        
        self.labelNoRestaurant.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelNoRestaurant.text = Strings.noDataFOund
        self.labelNoRestaurant.textColor = UIColor.init(named: "HomeTextColor")
        self.labelNoRestaurant.sizeToFit()
        
        self.labelCartCount.isHidden = true
        self.labelCartCount.setTextFont(fontWeight: .medium, ofSize: 12)
        self.labelLocation.setTextFont(fontWeight: .regular, ofSize: 14)
        self.labelCartCount.textColor = UIColor.init(named: "WhiteColor")
        self.labelCartCount.backgroundColor = UIColor.init(named: "ButtonColor")
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        
        DispatchQueue.main.async {
            self.table_view.startShimmeringAnimation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.isCategorySelected = false
        if self.shouldFetchDashboardData == true {
            self.getCurerntLocation()
            DispatchQueue.global(qos: .background).async {
                print("This is run on the background queue")
                
                self.fetchDashboardData()
//                DispatchQueue.main.async {
//                    print("This is run on the main queue, after the previous code in outer block")
//                }
            }
            
        }else{
            self.shouldFetchDashboardData = true
        }
    }
    
    private func getCurerntLocation() {
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            self.locationManager.requestAlwaysAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                locationManager.startUpdatingLocation()
            }
        } else if status == .denied {
            self.createSettingsAlertController(title: Strings.AppName, message: Strings.locationEnableErrorMsg)
        } else {
            self.locationManager.requestAlwaysAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    func createSettingsAlertController(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func didCurrentLocationSelected() {
        
        self.geocoder.reverseGeocodeLocation(self.currentLocation!, completionHandler: { placemarks, error in
            //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
            if error == nil && (placemarks?.count ?? 0) > 0 {
                let placemark = placemarks?[0]
                let currentAddress = Utility.removeNull(from: "\(placemark?.name ?? ""),\(placemark?.locality ?? ""),\(placemark?.subAdministrativeArea ?? "")")
                let getLocation = Location(
                    values: "",
                    locationName: currentAddress,
                    locationSecondaryName: currentAddress,
                    locationLatitude: self.currentLocation!.coordinate.latitude,
                    locationLongitude: self.currentLocation!.coordinate.longitude)
                self.labelLocation.text = getLocation.locationName
                self.dismiss(animated: true)
            } else {
                print("\(error.debugDescription)")
            }
        })
    }
    
    // MARK: getting Address from lat long
    private func getAddressFromLatAndLong(withLat latitude: CLLocationDegrees, withLong longitude: CLLocationDegrees) {
        let geocoder = GMSGeocoder()
        
        let position = CLLocationCoordinate2DMake(latitude, longitude)
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
            }else {
                let result = response?.results()?.first
                let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                
                print(address ?? "")
                //self.location["text"] = address
                
                self.currrentLatitude = latitude
                self.currentLongitude = longitude
                self.currentAddress = address!
                
                self.selectedCoordinates!(self.currrentLatitude,self.currentLongitude,self.currentAddress,self.currentPlaceName)
                
                self.labelLocation.text = address
            }
        }
    }
    
    func fetchDashboardData() {
        
//        self.dashboardRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3" //UserDefaultsManager.shared.getStringValue(forKey: .userId)
//        self.dashboardRequest.UserType = Strings.userTypeCustomer
//        self.dashboardRequest.lattitude = "\(Utility.lattitude)" //"22.6626267"//"\(self.lattitude)"
//        self.dashboardRequest.longitude = "\(Utility.longitude)" //"88.40060609999999"//"\(self.lattitude)"
        
        let dashboardRequest = DashboardRequest(userID: UserDefaultsManager.shared.getStringValue(forKey: .userId),
                                                UserType: Strings.userTypeCustomer, lattitude: "\(Utility.lattitude)", longitude: "\(Utility.longitude)")
        
//        self.dataStartedLoading()
        self.webService.fetchDashboardData(dashboardRequest: dashboardRequest) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
            DispatchQueue.main.async {
                self.table_view.stopShimmeringAnimation()
            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.cart_id = data.cartID ?? ""
                let cart_count = data.cartCountTotal ?? 0
                DispatchQueue.main.async {
                    if cart_count>0{
                        self.labelCartCount.isHidden = false
                        self.labelCartCount.text = "\(cart_count)"
                    }else{
                        self.labelCartCount.isHidden = true
                        self.labelCartCount.text = "\(cart_count)"
                    }
                    Utility.cartCount = cart_count
                }
                self.isCategorySelected = true
                self.categoryImageUrl = data.categoryImageURL ?? ""
                self.categoryArray = data.categoryData ?? []
                self.topDishesArray = data.topDishes ?? []
                let todaysArray = data.toDaysMenu ?? []
                self.todaysAllMenuArray = data.toDaysMenu ?? []
                if todaysArray.count > 0 {
                    self.todaysMenuArray = todaysArray[0].value ?? []
                    self.todaysMenuCategoryArray.removeAll()
                    for item in todaysArray {
                        self.todaysMenuCategoryArray.append(item.name ?? "NA")
                    }
                    
                    DispatchQueue.main.async {
                        self.createMenuArray()
                        self.collectionView_todaysMenu.reloadData()
//                        self.table_view.reloadData()
                    }
                }
                
                if self.categoryArray.count == 0 && self.topDishesArray.count == 0 && todaysArray.count == 0 {
                    DispatchQueue.main.async {
                        self.table_view.isHidden = true
                        self.labelNoRestaurant.isHidden = false
                    }
                }else{
                    DispatchQueue.main.async {
                        self.table_view.isHidden = false
                        self.labelNoRestaurant.isHidden = true
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func makeFavourite(menuID: String) {
        self.favouriteRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.favouriteRequest.menuId = menuID
//        self.dataStartedLoading()
        DispatchQueue.main.async {
            self.table_view.startShimmeringAnimation()
        }
        self.webService.markFavourite(favouriteRequest: self.favouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
            DispatchQueue.main.async {
                self.table_view.stopShimmeringAnimation()
            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                let menuId = data.menuID
                print(menuId ?? "")
                self.fetchDashboardData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func makeUnfavourite(menuID: String) {
        self.unFavouriteRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.unFavouriteRequest.menuId = menuID
//        self.dataStartedLoading()
        DispatchQueue.main.async {
            self.table_view.startShimmeringAnimation()
        }
        self.webService.markUnfavourite(favouriteRequest: self.unFavouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
            DispatchQueue.main.async {
                self.table_view.stopShimmeringAnimation()
            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchDashboardData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func addToCart(menuID: String, menuAmount: String, menuQuantity: String, vendorId: String) {
        
        self.addCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addCartRequest.menuId = menuID
        self.addCartRequest.menuAmount = menuAmount
        self.addCartRequest.menuQuantity = menuQuantity
        self.addCartRequest.vendorId = vendorId
//        self.dataStartedLoading()
//        DispatchQueue.main.async {
//            self.table_view.startShimmeringAnimation()
//        }
        self.cartWebService.addToCart(addCartRequest: self.addCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
//            DispatchQueue.main.async {
//                self.table_view.stopShimmeringAnimation()
//            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                let statusCode = successDashboardResponse.statuscode
                if statusCode == 300 {
                    let message = successDashboardResponse.message
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message ?? "")
                }else if statusCode == 200 {
                    self.fetchDashboardData()
                }
                
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func updateCart(cartID: String, menuID: String, menuAmount: String, menuQuantity: String, vendorId: String) {
        self.updateCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.updateCartRequest.menuId = menuID
        self.updateCartRequest.menuAmount = menuAmount
        self.updateCartRequest.menuQuantity = menuQuantity
        self.updateCartRequest.cartId = cartID
        self.updateCartRequest.vendorId = vendorId
//        self.dataStartedLoading()
//        DispatchQueue.main.async {
//            self.table_view.startShimmeringAnimation()
//        }
        self.cartWebService.updateCart(updateCartRequest: self.updateCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
//            DispatchQueue.main.async {
//                self.table_view.stopShimmeringAnimation()
//            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchDashboardData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func removeCart(cartID: String, menuID: String, vendorId: String) {
        self.removeCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.removeCartRequest.menuId = menuID
        self.removeCartRequest.cartId = cartID
        self.removeCartRequest.vendorId = vendorId
//        self.dataStartedLoading()
//        DispatchQueue.main.async {
//            self.table_view.startShimmeringAnimation()
//        }
        self.cartWebService.removeCart(removeCartRequest: self.removeCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
//            DispatchQueue.main.async {
//                self.table_view.stopShimmeringAnimation()
//            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchDashboardData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func createMenuArray() {
        self.todaysMenuArray = self.todaysAllMenuArray[selectedIndexPath.item].value ?? []
        DispatchQueue.main.async {
            self.table_view.reloadData()
        }
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        LeftMenuViewController.showLeftMenu(onParentViewController: self) { (selectedObj, selectedPosition) in
            
        }
    }
    @IBAction func buttonLocationAction(_ sender: Any) {
        let searchLocationView = SearchLocationView.instantiateFromAppStoryboard(appStoryboard: .search)
        searchLocationView.block = { (addressID, address, landmark, houseNo, latittude, longitude, typeId, type, comingFrom) in
            self.labelLocation.text = address
            Utility.lattitude = Double(latittude)!
            Utility.longitude = Double(longitude)!
            self.shouldFetchDashboardData = false
            self.fetchDashboardData()
        }
        searchLocationView.selectedPlaceDelegate = self
        searchLocationView.locationType = "Pickup"
        searchLocationView.comingFrom = "Home"
        self.navigationController?.pushViewController(searchLocationView, animated: true)
    }
    @IBAction func buttonBellAction(_ sender: Any) {
    }
    @IBAction func buttonCartAction(_ sender: Any) {
        if self.labelCartCount.text == "0" || self.labelCartCount.text == "" {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "Opps! Cart is empty\nPlease add something to the cart first.")
        }else{
            let cartView = CartView.instantiateFromAppStoryboard(appStoryboard: .cart)
            self.navigationController?.pushViewController(cartView, animated: true)
        }
    }
    @IBAction func buttonSearchAction(_ sender: Any) {
        let foodSearchView = FoodSearchView.instantiateFromAppStoryboard(appStoryboard: .search)
        foodSearchView.totalCartCount = self.labelCartCount.text ?? ""
        foodSearchView.lattitude = self.lattitude
        foodSearchView.longitude = self.longitude
        self.navigationController?.pushViewController(foodSearchView, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension HomeView : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.currentLocation = locations.last
        locationManager.stopUpdatingLocation()
        self.lattitude = locValue.latitude
        self.longitude = locValue.longitude
        Utility.lattitude = locValue.latitude
        Utility.longitude = locValue.longitude
        self.didCurrentLocationSelected()
    }
}
extension HomeView : SelecetdPlacesDelegate {
    
    func locationSelected(currentLocation: Location, ofType: String) {
       print("location : ",currentLocation)
        self.labelLocation.text = currentLocation.locationName
//        addLocationView.addressLabel.text = currentLocation.locationName ?? ""
    }
    func selectedLocationType(ofType: String,selectedIndex : Int) {
      
//        self.selectedIndex = selectedIndex
//        addLocationView.collView.reloadData()
        
    }
}
extension HomeView : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }else{
            if self.todaysMenuArray.count>0 {
                return self.todaysMenuArray.count
            }else{
                if self.isCategorySelected == false {
                    return 5
                }else{
                    return 1
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "homeCategoryTableCell", for: indexPath) as! HomeCategoryTableCell
                
                self.collectionView_category = cell.collection_viewCategory
                cell.collection_viewCategory.delegate = self
                cell.collection_viewCategory.dataSource = self
                cell.collection_viewCategory.reloadData()
                
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "homeTopDishesTableCell", for: indexPath) as! HomeTopDishesTableCell
                
                self.collectionView_topDishes = cell.collection_viewTopDishes
                cell.collection_viewTopDishes.delegate = self
                cell.collection_viewTopDishes.dataSource = self
                cell.labelHeader.text = "Top Dishes"
                cell.collection_viewTopDishes.reloadData()
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "homeTodaysMenuTableCell", for: indexPath) as! HomeTodaysMenuTableCell
                
                self.collectionView_todaysMenu = cell.collection_viewTodaysMenu
                cell.collection_viewTodaysMenu.delegate = self
                cell.collection_viewTodaysMenu.dataSource = self
                cell.labelHeader.text = "Today's Menu"
                cell.collection_viewTodaysMenu.reloadData()
                
                return cell
            }
        }else{
            if self.todaysMenuArray.count>0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "homeTodaysMenuListTableCell", for: indexPath) as! HomeTodaysMenuListTableCell
                
                let itemImage = self.todaysMenuArray[indexPath.row].itemID?.menuImage ?? ""
                if itemImage != "" {
                    cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
                }
                cell.imageViewMenuItem.contentMode = .scaleAspectFill
                cell.imageViewMenuItem.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
                cell.delegate = self
                cell.labelTitle.text = self.todaysMenuArray[indexPath.row].itemID?.itemName ?? "NA"
                cell.labelDescription.text = self.todaysMenuArray[indexPath.row].itemID?.description == "" ? "NA" : self.todaysMenuArray[indexPath.row].itemID?.description
//                cell.labelActualPrice.text = "₹ \(self.todaysMenuArray[indexPath.row].itemID?.price ?? 0)"
//                cell.labelActualPrice.sizeToFit()
                cell.imageViewLine.isHidden = true
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "₹ \(self.todaysMenuArray[indexPath.row].itemID?.price ?? 0)")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                
                cell.labelActualPrice.attributedText = attributeString
                
                let discountType = self.todaysMenuArray[indexPath.row].itemID?.discountType
                let price = self.todaysMenuArray[indexPath.row].itemID?.price ?? 0
                let discountPrice = self.todaysMenuArray[indexPath.row].itemID?.discountAmount ?? 0
                var offerPrice : Float = 0.0
                if discountType == "FLAT" {
                    offerPrice = Float(price-discountPrice)
                }else if discountType == "NONE" {
                    offerPrice = Float(price)
                }else{
                    offerPrice = Float(price-((price*discountPrice)/100))
                }
                cell.labelDiscountedPrice.text = "₹ \(offerPrice)"
                
                let isVegItem = self.todaysMenuArray[indexPath.row].itemID?.type
                if isVegItem == "NON VEG" {
                    cell.imageViewVeg.image = UIImage(named: "nonveg")
                }else{
                    cell.imageViewVeg.image = UIImage(named: "veg")
                }
                let isFavItem = self.todaysMenuArray[indexPath.row].isFavorite
                if isFavItem == 1 {
                    cell.imageViewFavourite.image = UIImage(named: "heart_red")
                }else{
                    cell.imageViewFavourite.image = UIImage(named: "heart_white")
                }
                cell.labelRating.text = "\(self.todaysMenuArray[indexPath.row].rating)"
                
                let cartQuantity  = self.todaysMenuArray[indexPath.row].cartMenuQuantity
                
                if cartQuantity == 0 {
                    cell.viewIncreaseCart.isHidden = true
                    cell.buttonAddCart.isHidden = false
                }else{
                    cell.viewIncreaseCart.isHidden = false
                    cell.buttonAddCart.isHidden = true
                    cell.labelCartCount.text = "\(cartQuantity ?? 0)"
                }
                
                let isActive = self.todaysMenuArray[indexPath.row].itemID?.isActive
                if isActive == false {
                    cell.viewShadowBG.isHidden = false
                }else{
                    cell.viewShadowBG.isHidden = true
                }
                
                cell.buttonFavourite.tag = indexPath.row
                cell.buttonMinus.tag = indexPath.row
                cell.buttonPlus.tag = indexPath.row
                cell.buttonAddCart.tag = indexPath.row
                cell.buttonAddCart.backgroundColor = UIColor.init(named: "ButtonColor")
                cell.buttonMinus.setImage(UIImage(named: "minus_blue"), for: .normal)
                cell.buttonPlus.setImage(UIImage(named: "plus_blue"), for: .normal)
                
                return cell
            }else{
                if self.isCategorySelected == true {
                   self.isCategorySelected = false
                    let cell = tableView.dequeueReusableCell(withIdentifier: "homeNoItemTableCell", for: indexPath) as! HomeNoItemTableCell
                    cell.labelNoItemFound.setTextFont(fontWeight: .bold, ofSize: 20)
                    cell.labelNoItemFound.text = Strings.noDataFOund
                    cell.labelNoItemFound.numberOfLines = 0
                    cell.labelNoItemFound.sizeToFit()
                    
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "homeTodaysMenuListTableCell", for: indexPath) as! HomeTodaysMenuListTableCell
                    
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if self.categoryArray.count == 0 {
                    return 0
                }else{
                    return 140
                }
            } else if indexPath.row == 1 {
                if self.topDishesArray.count == 0 {
                    return 0
                }else{
                    return 385
                }
            } else if indexPath.row == 2 {
                if self.todaysMenuCategoryArray.count == 0 {
                    return 0
                }else{
                    return 120
                }
            } else {
                return 320
            }
        } else {
            if self.todaysMenuArray.count>0{
                return 320
            }else{
                return 320
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let isActive = self.todaysMenuArray[indexPath.row].itemID?.isActive
            if isActive == true {
                let homeDetailView = HomeDetailsView.instantiateFromAppStoryboard(appStoryboard: .home)
                homeDetailView.menuItem = self.todaysMenuArray[indexPath.item]
                homeDetailView.imageURL = self.categoryImageUrl
                homeDetailView.cartId = self.cart_id
                homeDetailView.totalCartCount = self.labelCartCount.text ?? ""
                self.navigationController?.pushViewController(homeDetailView, animated: true)
            }
        }
    }
}
extension HomeView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension HomeView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView_category {
            return self.categoryArray.count
        }else if collectionView == self.collectionView_topDishes {
            return self.topDishesArray.count
        }else{
            return todaysMenuCategoryArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.collectionView_category){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCategoryCollectionCell", for: indexPath) as! HomeCategoryCollectionCell
            
            let itemImage = "\(self.categoryImageUrl)\(self.categoryArray[indexPath.item].image ?? "")"
            if itemImage != "" {
                cell.imageViewCategory.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
            }
            cell.imageViewCategory.contentMode = .scaleAspectFill
            cell.labelTitle.text = self.categoryArray[indexPath.item].name
            
            return cell
        }else if(collectionView == self.collectionView_topDishes){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeTopDishesCollectionCell", for: indexPath) as! HomeTopDishesCollectionCell
            
            let itemImage = self.topDishesArray[indexPath.item].itemID?.menuImage ?? ""
            if itemImage != "" {
                cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
            }
//            cell.imageViewMenuItem.contentMode = .scaleAspectFill
            cell.imageViewMenuItem.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
            cell.delegate = self
            cell.labelTitle.text = self.topDishesArray[indexPath.item].itemID?.itemName
            cell.labelDescription.text = self.topDishesArray[indexPath.item].itemID?.description ?? "NA"
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "₹ \(self.topDishesArray[indexPath.item].itemID?.price ?? 0)")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            
            cell.labelActualPrice.attributedText = attributeString
            
            let discountType = self.topDishesArray[indexPath.row].itemID?.discountType
            let price = self.topDishesArray[indexPath.row].itemID?.price ?? 0
            let discountPrice = self.topDishesArray[indexPath.row].itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            cell.labelDiscountedPrice.text = "₹ \(offerPrice)"
            
            cell.labelTitle.setTextFont(fontWeight: .medium, ofSize: 16)
            cell.labelTitle.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelDescription.setTextFont(fontWeight: .light, ofSize: 15)
            cell.labelDescription.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelActualPrice.setTextFont(fontWeight: .light, ofSize: 11)
            cell.labelActualPrice.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelDiscountedPrice.setTextFont(fontWeight: .regular, ofSize: 12)
            cell.labelDiscountedPrice.textColor = UIColor.init(named: "HomeTextColor")
            
            let isVegItem = self.topDishesArray[indexPath.row].itemID?.type
            if isVegItem == "NON VEG" {
                cell.imageViewVeg.image = UIImage(named: "nonveg")
            }else{
                cell.imageViewVeg.image = UIImage(named: "veg")
            }
            let isFavItem = self.topDishesArray[indexPath.row].isFavorite
            if isFavItem == 1 {
                cell.imageViewFavourite.image = UIImage(named: "heart_red")
            }else{
                cell.imageViewFavourite.image = UIImage(named: "heart_white")
            }
            let cartQuantity = self.topDishesArray[indexPath.row].cartMenuQuantity
            if cartQuantity == 0 {
                cell.buttonAddCart.isHidden = false
                cell.viewCartBG.isHidden = true
            }else{
                cell.buttonAddCart.isHidden = true
                cell.viewCartBG.isHidden = false
                cell.labelCartCount.text = "\(cartQuantity ?? 0)"
                Utility.cartCount = cartQuantity ?? 0
            }
            
            let isActive = self.topDishesArray[indexPath.row].itemID?.isActive
            if isActive == false {
                cell.viewShadowBG.isHidden = false
            }else{
                cell.viewShadowBG.isHidden = true
            }
            
            cell.buttonFavourite.tag = indexPath.item
            cell.buttonAddCart.tag = indexPath.item
            cell.buttonMinus.tag = indexPath.item
            cell.buttonPlus.tag = indexPath.item
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeTodaysMenuCollectionCell", for: indexPath) as! HomeTodaysMenuCollectionCell
            
            cell.labelMenuTypeTitle.text = self.todaysMenuCategoryArray[indexPath.item]
            if indexPath == self.selectedIndexPath {
                cell.viewMenuSelectionBG.backgroundColor = UIColor.init(named: "YellowColor")
                cell.labelMenuTypeTitle.textColor = .white
            }else{
                cell.viewMenuSelectionBG.backgroundColor = .clear
                cell.labelMenuTypeTitle.textColor = UIColor.init(named: "HomeTextLightColor")
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionView_todaysMenu {
            let label = UILabel(frame: CGRect.zero)
            label.text = self.todaysMenuCategoryArray[indexPath.item]
            if selectedIndexPath == indexPath {
                label.font = UIFont(name:"CeraPro-Light", size: 18.0)
            } else {
                label.font = UIFont(name:"CeraPro-Light", size: 15.0)
            }
            
            label.sizeToFit()
            
            let view_bg = UIView()
            view_bg.frame = CGRect(x: 5, y: 5, width: label.frame.size.width + 30, height: 65)
            
            return CGSize(width: view_bg.frame.width + 10, height: view_bg.frame.height)
            
        }else if collectionView == self.collectionView_category {
            return CGSize(width: 140, height: 140)
        }else{
            if self.topDishesArray.count == 0 {
                return CGSize(width: UIScreen.main.bounds.size.width, height: 0)
            }else{
                return CGSize(width: UIScreen.main.bounds.size.width, height: 320)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.collectionView_category {
            return UIEdgeInsets(top: 10, left:20, bottom: 10, right: 10)
        }else if collectionView == self.collectionView_topDishes {
            return UIEdgeInsets(top: 10, left:0, bottom: 10, right: 10)
        } else {
            return UIEdgeInsets(top: 10, left:10, bottom: 10, right: 10)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndexPath = IndexPath(row: indexPath.item, section: 0)
        if collectionView == self.collectionView_todaysMenu {
            self.isCategorySelected = true
            self.createMenuArray()
            self.collectionView_todaysMenu.reloadData()
        }else if collectionView == self.collectionView_topDishes {
            let isActive = self.topDishesArray[indexPath.row].itemID?.isActive
            if isActive == true {
                let homeDetailView = HomeDetailsView.instantiateFromAppStoryboard(appStoryboard: .home)
                homeDetailView.menuItem = self.topDishesArray[indexPath.item]
                homeDetailView.imageURL = self.categoryImageUrl
                homeDetailView.cartId = self.cart_id
                homeDetailView.totalCartCount = self.labelCartCount.text ?? ""
                self.navigationController?.pushViewController(homeDetailView, animated: true)
            }
        }else if collectionView == self.collectionView_category {
            let categoryId = self.categoryArray[indexPath.row].id
            let categoryDetailView = CategoryDetailView.instantiateFromAppStoryboard(appStoryboard: .home)
            categoryDetailView.categoryID = categoryId ?? ""
            categoryDetailView.cart_id = self.cart_id
            categoryDetailView.location = self.labelLocation.text ?? ""
            categoryDetailView.totalCartCount = self.labelCartCount.text ?? ""
            self.navigationController?.pushViewController(categoryDetailView, animated: true)
        }
    }
}
extension HomeView : TopDishesCollectionDelegate {
    func markAsFavourite(_ sender: UIButton) {
        
        let isFavourite = self.topDishesArray[sender.tag].isFavorite
        let menuID = self.topDishesArray[sender.tag].id ?? ""
        if isFavourite == 1 {
            if menuID != "" {
                self.makeUnfavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }else{
            if menuID != "" {
                self.makeFavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }
    }
    
    func minusCartItem(_ sender: UIButton) {
        let menuID = self.topDishesArray[sender.tag].id ?? ""
        
        let discountType = self.topDishesArray[sender.tag].itemID?.discountType
        let price = self.topDishesArray[sender.tag].itemID?.price ?? 0
        let discountPrice = self.topDishesArray[sender.tag].itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        let menuQuantity: Int = self.topDishesArray[sender.tag].cartMenuQuantity ?? 0
        let vendorID: String = self.topDishesArray[sender.tag].restaurantID ?? ""
        
        if menuQuantity == 1 {
            self.removeCart(cartID: self.cart_id, menuID: menuID, vendorId: vendorID)
        }else{
            if menuQuantity > 1 {
                self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity-1)", vendorId: vendorID)
            }
        }
        
        var superview = sender.superview
        while let view = superview, !(view is HomeTopDishesCollectionCell) {
            superview = view.superview
        }
        guard let cell = superview as? HomeTopDishesCollectionCell else {
            print("button is not contained in a table view cell")
            return
        }
        
        if menuQuantity == 1 {
            cell.buttonAddCart.isHidden = false
            cell.viewCartBG.isHidden = true
            cell.labelCartCount.text = "\(menuQuantity-1)"
        }else{
            cell.buttonAddCart.isHidden = true
            cell.viewCartBG.isHidden = false
            cell.labelCartCount.text = "\(menuQuantity-1)"
        }
        
    }
    
    func plusCartItem(_ sender: UIButton) {
         if self.topDishesArray[sender.tag].itemID?.isActive == true {
            let menuID = self.topDishesArray[sender.tag].id ?? ""
            
            let discountType = self.topDishesArray[sender.tag].itemID?.discountType
            let price = self.topDishesArray[sender.tag].itemID?.price ?? 0
            let discountPrice = self.topDishesArray[sender.tag].itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice )"
            let menuQuantity: Int = self.topDishesArray[sender.tag].cartMenuQuantity ?? 0
            let vendorID: String = self.topDishesArray[sender.tag].restaurantID ?? ""
            
            self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)", vendorId: vendorID)
            
            var superview = sender.superview
            while let view = superview, !(view is HomeTopDishesCollectionCell) {
                superview = view.superview
            }
            guard let cell = superview as? HomeTopDishesCollectionCell else {
                print("button is not contained in a table view cell")
                return
            }
            
            cell.labelCartCount.text = "\(menuQuantity+1)"
        }
         else {
            self.showErrorAlertWithTitle(title: "Alert", message: "This item is not available now")
        }
    }
    
    func addToCartItem(_ sender: UIButton) {
        
        if self.topDishesArray[sender.tag].itemID?.isActive == true {
            let menuID = self.topDishesArray[sender.tag].id ?? ""
            let discountType = self.topDishesArray[sender.tag].itemID?.discountType
            let price = self.topDishesArray[sender.tag].itemID?.price ?? 0
            let discountPrice = self.topDishesArray[sender.tag].itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice )"
            let vendorID = self.topDishesArray[sender.tag].restaurantID ?? ""
            
            self.addToCart(menuID: menuID, menuAmount: menuAmount, menuQuantity: "1", vendorId: vendorID)
            
            var superview = sender.superview
            while let view = superview, !(view is HomeTopDishesCollectionCell) {
                superview = view.superview
            }
            guard let cell = superview as? HomeTopDishesCollectionCell else {
                print("button is not contained in a table view cell")
                return
            }
            
            cell.labelCartCount.text = "1"
            cell.buttonAddCart.isHidden = true
            cell.viewCartBG.isHidden = false
        }
        else {
            self.showErrorAlertWithTitle(title: "Alert", message: "This item is not available now")
        }
    }
    
    //    func getCurrentCellIndexPath(_ sender: UIButton) -> IndexPath? {
    //        let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
    //        if let indexPath: IndexPath = tableView.indexPathForRow(at: buttonPosition) {
    //            return indexPath
    //        }
    //        return nil
    //    }
}
extension HomeView : TodaysMenuTableDelegate {
    
//    func updateTable() {
//        self.table_view.reloadData()
//    }
    
    func markAsFavouriteTable(_ sender: UIButton) {
        let isFavourite = self.todaysMenuArray[sender.tag].isFavorite
        let menuID = self.todaysMenuArray[sender.tag].id ?? ""
        if isFavourite == 1 {
            if menuID != "" {
                self.makeUnfavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }else{
            if menuID != "" {
                self.makeFavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }
    }
    
    func minusCartItemTable(_ sender: UIButton) {
        let menuID = self.todaysMenuArray[sender.tag].id ?? ""
        
        let discountType = self.todaysMenuArray[sender.tag].itemID?.discountType
        let price = self.todaysMenuArray[sender.tag].itemID?.price ?? 0
        let discountPrice = self.todaysMenuArray[sender.tag].itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = offerPrice
        let menuQuantity: Int = self.todaysMenuArray[sender.tag].cartMenuQuantity ?? 0
        let vendorID: String = self.todaysMenuArray[sender.tag].restaurantID ?? ""
        
        if menuQuantity == 1 {
            self.removeCart(cartID: self.cart_id, menuID: menuID, vendorId: vendorID)
        }else{
            if menuQuantity > 1 {
                self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: "\(menuAmount)", menuQuantity: "\(menuQuantity-1)", vendorId: vendorID)
            }
        }
        
        var superview = sender.superview
        while let view = superview, !(view is HomeTodaysMenuListTableCell) {
            superview = view.superview
        }
        guard let cell = superview as? HomeTodaysMenuListTableCell else {
            print("button is not contained in a table view cell")
            return
        }
        guard let indexPath = self.table_view.indexPath(for: cell) else {
            print("failed to get index path for cell containing button")
            return
        }
        // We've got the index path for the cell that contains the button, now do something with it.
        print("button is in row \(indexPath.row)")
        if menuQuantity == 1 {
            cell.viewIncreaseCart.isHidden = true
            cell.buttonAddCart.isHidden = false
            cell.labelCartCount.text = "\(menuQuantity-1)"
        }else{
            cell.labelCartCount.text = "\(menuQuantity-1)"
        }
        
    }
    
    func plusCartItemTable(_ sender: UIButton) {
        if self.todaysMenuArray[sender.tag].itemID?.isActive == true {
            let menuID = self.todaysMenuArray[sender.tag].id ?? ""
                   
                   let discountType = self.todaysMenuArray[sender.tag].itemID?.discountType
                   let price = self.todaysMenuArray[sender.tag].itemID?.price ?? 0
                   let discountPrice = self.todaysMenuArray[sender.tag].itemID?.discountAmount ?? 0
                   var offerPrice : Float = 0.0
                   if discountType == "FLAT" {
                       offerPrice = Float(price-discountPrice)
                   }else if discountType == "NONE" {
                       offerPrice = Float(price)
                   }else{
                       offerPrice = Float(price-((price*discountPrice)/100))
                   }
                   
                   let menuAmount = "\(offerPrice )"
                   let menuQuantity: Int = self.todaysMenuArray[sender.tag].cartMenuQuantity ?? 0
                   let vendorID: String = self.todaysMenuArray[sender.tag].restaurantID ?? ""
                   
                   self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)", vendorId: vendorID)
                   
                   var superview = sender.superview
                   while let view = superview, !(view is HomeTodaysMenuListTableCell) {
                       superview = view.superview
                   }
                   guard let cell = superview as? HomeTodaysMenuListTableCell else {
                       print("button is not contained in a table view cell")
                       return
                   }
                   guard let indexPath = self.table_view.indexPath(for: cell) else {
                       print("failed to get index path for cell containing button")
                       return
                   }
                   // We've got the index path for the cell that contains the button, now do something with it.
                   print("button is in row \(indexPath.row)")
                   
                   cell.labelCartCount.text = "\(menuQuantity+1)"
        }
        else {
            self.showErrorAlertWithTitle(title: "Alert", message: "This item is not available now")
        }
     }
    
    func addToCartItemTable(_ sender: UIButton) {
        if self.todaysMenuArray[sender.tag].itemID?.isActive == true {
            let menuID = self.todaysMenuArray[sender.tag].id ?? ""
            
            let discountType = self.todaysMenuArray[sender.tag].itemID?.discountType
            let price = self.todaysMenuArray[sender.tag].itemID?.price ?? 0
            let discountPrice = self.todaysMenuArray[sender.tag].itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice ?? 0.0)"
            let vendorID = self.todaysMenuArray[sender.tag].restaurantID ?? ""
            
            self.addToCart(menuID: menuID, menuAmount: menuAmount, menuQuantity: "1", vendorId: vendorID)
            
            var superview = sender.superview
            while let view = superview, !(view is HomeTodaysMenuListTableCell) {
                superview = view.superview
            }
            guard let cell = superview as? HomeTodaysMenuListTableCell else {
                print("button is not contained in a table view cell")
                return
            }
            guard let indexPath = self.table_view.indexPath(for: cell) else {
                print("failed to get index path for cell containing button")
                return
            }
            // We've got the index path for the cell that contains the button, now do something with it.
            print("button is in row \(indexPath.row)")
            
            cell.labelCartCount.text = "1"
            cell.viewIncreaseCart.isHidden = false
            cell.buttonAddCart.isHidden = true
        }
        else {
            self.showErrorAlertWithTitle(title: "Alert", message: "This item is not available now")
        }
        
        
    }
}
// Handle the user's selection.
extension HomeView: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place ID: \(place.placeID ?? "")")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

