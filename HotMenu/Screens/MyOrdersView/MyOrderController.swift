//
//  MyOrderController.swift
//  LocationTacking
//
//  Created by BrainiumSSD on 08/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit
import AMShimmer

class MyOrderController: UIViewController {

    @IBOutlet var orderTableView: UITableView!
    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var labelNoOrderFound: CustomLabel!
    
    private var webService = MyOrderAPI()
    private var myOrderRequest = MyOrderRequest()
    
    var orderList: [MyOrderResponseDatum] = []
    var previousClass: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.myOrderHeader
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelNoOrderFound.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelNoOrderFound.text = Strings.noOrderFound
        self.labelNoOrderFound.textColor = UIColor.init(named: "HomeTextColor")
        self.labelNoOrderFound.isHidden = true
        
        orderTableView.register(UINib(nibName: "MyOrderView", bundle: nil), forCellReuseIdentifier: "MyOrderCell")
        orderTableView.dataSource = self
        orderTableView.delegate = self
        DispatchQueue.main.async {
            AMShimmer.start(for: self.orderTableView)
        }
        self.myOrderList()
    }
    
    func myOrderList() {
//        self.dataStartedLoading()
        self.myOrderRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.webService.myOrderList(myOrderRequest: self.myOrderRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.orderTableView)
            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.orderList = data
                
                DispatchQueue.main.async {
                    if self.orderList.count>0 {
                        self.labelNoOrderFound.isHidden = true
                        self.orderTableView.isHidden = false
                        self.orderTableView.reloadData()
                    }else{
                        self.labelNoOrderFound.isHidden = false
                        self.orderTableView.isHidden = true
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if self.previousClass == "CartView" {
            DispatchQueue.main.async {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: HomeView.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}
extension MyOrderController : UITableViewDataSource,UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.orderList.count > 0 {
            return self.orderList.count
        }else{
            return 5
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: MyOrderCell = tableView.dequeueReusableCell(withIdentifier: "MyOrderCell") as! MyOrderCell
        cell.selectionStyle = .none
        
        if self.orderList.count > 0 {
            cell.labelOrderId.text = "Order Id:\(self.orderList[indexPath.row].orderID ?? "")"
            cell.labelPrice.text = "₹ \(self.orderList[indexPath.row].finalAmount ?? 0.0)"
            let orderDateStr = self.orderList[indexPath.row].orderTime ?? ""
            print(orderDateStr)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let orderDate = dateFormatter.date(from: orderDateStr)!
            print(orderDate)
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
            let finalOderDateStr = dateFormatter.string(from: orderDate)
            cell.labelDate.text = finalOderDateStr
            
            cell.labelOrderId.setTextFont(fontWeight: .medium, ofSize: 15)
            cell.labelPrice.setTextFont(fontWeight: .regular, ofSize: 12)
            cell.labelDate.setTextFont(fontWeight: .regular, ofSize: 12)
            
            cell.labelOrderId.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelPrice.textColor = UIColor.init(named: "HomeTextColor")
            cell.labelDate.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.delegate = self
            
            let orderStatus = self.orderList[indexPath.row].orderStatus
            if orderStatus == "Order initiate" {
                cell.buttonCancel.setNormalTitle("PENDING")
                cell.buttonCancel.backgroundColor = UIColor.init(named: "Pending")
                cell.buttonCancel.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
                cell.buttonCancel.setTitleFont(fontWeight: .medium, ofSize: 10.0)
                cell.buttonTrack.isHidden = false
            }else if orderStatus == "Delivered" {
                cell.buttonCancel.setNormalTitle("DELIVERED")
                cell.buttonCancel.backgroundColor = UIColor.init(named: "Delivered")
                cell.buttonCancel.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
                cell.buttonCancel.setTitleFont(fontWeight: .medium, ofSize: 10.0)
                cell.buttonTrack.isHidden = true
            }else if orderStatus == "Cancelled" {
                cell.buttonCancel.setNormalTitle("CANCELLED")
                cell.buttonCancel.backgroundColor = UIColor.init(named: "ButtonColor")
                cell.buttonCancel.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
                cell.buttonCancel.setTitleFont(fontWeight: .medium, ofSize: 10.0)
                cell.buttonTrack.isHidden = true
            }else if orderStatus == "Order Picked Up" {
                cell.buttonCancel.setNormalTitle("PICKED UP")
                cell.buttonCancel.backgroundColor = UIColor.init(named: "Pickedup")
                cell.buttonCancel.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
                cell.buttonCancel.setTitleFont(fontWeight: .medium, ofSize: 10.0)
                cell.buttonTrack.isHidden = false
            }else if orderStatus == "Accept" {
                cell.buttonCancel.setNormalTitle("ACCEPTED")
                cell.buttonCancel.backgroundColor = UIColor.init(named: "Pending")
                cell.buttonCancel.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
                cell.buttonCancel.setTitleFont(fontWeight: .medium, ofSize: 10.0)
                cell.buttonTrack.isHidden = false
            }else if orderStatus == "Assigned" {
                cell.buttonCancel.setNormalTitle("ASSIGNED")
                cell.buttonCancel.backgroundColor = UIColor.init(named: "Delivered")
                cell.buttonCancel.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
                cell.buttonCancel.setTitleFont(fontWeight: .medium, ofSize: 10.0)
                cell.buttonTrack.isHidden = false
            }
            
            cell.buttonTrack.setNormalTitle("TRACK ORDER")
            cell.buttonTrack.backgroundColor = .clear
            cell.buttonTrack.setTitleColor(UIColor.init(named: "ButtonColor"), for: .normal)
            cell.buttonTrack.setTitleFont(fontWeight: .medium, ofSize: 11.0)
            cell.buttonTrack.tag = indexPath.row
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        
        headerView.backgroundColor = .clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        else {
            return 20
        }
        
    }
}
extension MyOrderController: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension MyOrderController: OrderListDelegate {
    func trackOrder(_ sender: UIButton) {
        let orderId = self.orderList[sender.tag].orderID ?? ""
        let trackOrderView = TrackOrderController.instantiateFromAppStoryboard(appStoryboard: .myOrder)
        trackOrderView.orderId = orderId
        self.navigationController?.pushViewController(trackOrderView, animated: true)
    }
}
