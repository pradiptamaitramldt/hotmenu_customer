//
//  MyOrderCell.swift
//  LocationTacking
//
//  Created by BrainiumSSD on 08/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit

protocol OrderListDelegate {
    func trackOrder(_ sender : UIButton)
}

class MyOrderCell: UITableViewCell {
    
    @IBOutlet weak var labelOrderId: CustomLabel!
    @IBOutlet weak var labelPrice: CustomLabel!
    @IBOutlet weak var labelDate: CustomLabel!
    @IBOutlet weak var buttonCancel: CustomButton!
    @IBOutlet weak var buttonTrack: CustomButton!
    
    var delegate : OrderListDelegate?
    
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
        self.backgroundColor = .clear
        
    }
       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }

    @IBAction func buttonTackAction(_ sender: Any) {
        self.delegate?.trackOrder(sender as! UIButton)
    }
    
}
