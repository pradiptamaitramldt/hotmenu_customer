//
//  MyOrderModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 18/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct MyOrderRequest {
    
    var userID: String
            
    init() {
        self.userID = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        
        return tempDictionary
    }
}

// MARK: - MyOrderResponseModel
struct MyOrderResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [MyOrderResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct MyOrderResponseDatum: Codable {
    let orderID, orderNo: String?
    let finalAmount: Double?
    let orderStatus, orderTime: String?

    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case orderNo, finalAmount, orderStatus, orderTime
    }
}
