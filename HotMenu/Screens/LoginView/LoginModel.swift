//
//  LoginModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 14/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct LoginRequest {
    
    var phone: String
    var password: String
    var country_code: String
    var login_type: String
    var device_token: String
    var app_type: String
    var push_mode: String
    
    var delegate: LoginRequestDelegate?
    
    init() {
        self.phone = ""
        self.password = ""
        self.country_code = ""
        self.login_type = ""
        self.device_token = ""
        self.app_type = ""
        self.push_mode = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.login_type == Strings.loginTypeGeneral {
            if self.country_code != "" {
                if self.phone.isValidPhoneNumber() {
                    if self.password.isValidPassword() {
                        delegate.loginValidationPassed()
                    }else{
                        delegate.invalidLoginData(message: ErrorMessages.invalidPassword)
                    }
                }else{
                    delegate.invalidLoginData(message: ErrorMessages.invalidPhoneNumber)
                }
            }else{
                delegate.invalidLoginData(message: ErrorMessages.invalidDialCode)
            }
        }else{
            if self.phone != "" {
                delegate.loginValidationPassed()
            }else{
                delegate.invalidLoginData(message: ErrorMessages.invalidSocialID)
            }
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        if UserDefaultsManager.shared.getStringValue(forKey: .deviceToken) != "" {
            tempDictionary["deviceToken"] = UserDefaultsManager.shared.getStringValue(forKey: .deviceToken)
        } else {
            tempDictionary["deviceToken"] = "12345"
        }
        tempDictionary["appType"] = Strings.appType
        tempDictionary["loginType"] = self.login_type
        tempDictionary["pushMode"] = self.push_mode
        tempDictionary["user"] = self.phone
        tempDictionary["countryCode"] = self.country_code
        tempDictionary["password"] = self.password
        
        return tempDictionary
    }
}

protocol LoginRequestDelegate {
    
    func invalidLoginData(message: String)
    
    func loginValidationPassed()
}

struct SocialLoginDataModelFromSocialPlatforms {
    
    var socialID: String?
    var socialEmail: String?
    var socialFirstName: String?
    var socialLastName: String?
    var socialImageUrl: String?
    var type: String?
    
    init() {
        self.socialID = ""
        self.socialEmail = ""
        self.socialFirstName = ""
        self.socialLastName = ""
        self.socialImageUrl = ""
    }
    
    init(socialID: String, socialEmail: String, socialFirstName: String, socialLastName: String, socialImageUrl: String) {
        self.socialID = socialID
        self.socialEmail = socialEmail
        self.socialFirstName = socialFirstName
        self.socialLastName = socialLastName
        self.socialImageUrl = socialImageUrl
    }
}

// MARK: - LoginResponseModel
struct LoginResponseModel: Codable {
    let success: Bool
    let statuscode: Int?
    let message: String?
    let responseData: LoginResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct LoginResponseData: Codable {
    let userDetails: LoginUserDetails?
    let authToken: String?
}

// MARK: - UserDetails
struct LoginUserDetails: Codable {
    let fullName, email, countryCode, phone, otp: String?
    let socialID, customerID: String?
    let profileImage: String?
    let userType, loginType, sid, loginId: String?

    enum CodingKeys: String, CodingKey {
        case fullName, email, countryCode, phone, otp
        case socialID = "socialId"
        case customerID = "customerId"
        case profileImage, userType, loginType, sid, loginId
    }
}
