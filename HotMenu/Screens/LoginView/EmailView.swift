//
//  EmailView.swift
//  Driver
//
//  Created by Pradipta Maitra on 30/06/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

protocol EmailActionDelegate {
    func emailAction(email : String)
}

class EmailView: UIViewController {

    @IBOutlet weak var view_bg: CustomView!
    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var textfield_email: CustomTextField!
    @IBOutlet weak var buttonSubmit: CustomButton!
    
    var delegate: EmailActionDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 20)
        self.labelHeader.text = "Enter Email"
        
        self.textfield_email.keyboardType = .emailAddress
        self.textfield_email.tintColor = .white
        
        self.buttonSubmit.setNormalTitle(Strings.proceed)
        self.buttonSubmit.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonSubmit.setTitleFont(fontWeight: .bold, ofSize: 20.0)
        self.buttonSubmit.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonSubmit.cornerRadius = self.buttonSubmit.bounds.height/2
        self.buttonSubmit.borderColor = UIColor.white
        
        self.view.addhideKeyboardFeatureOnTouch()
    }
    
    @IBAction func buttonCrossAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonSubmitAction(_ sender: Any) {
        if self.textfield_email.text?.isValidEmail() ?? false {
            if self.delegate != nil {
                self.delegate?.emailAction(email: self.textfield_email.text ?? "")
                dismiss(animated: true, completion: nil)
            }
        }else{
            self.showErrorAlertWithTitle(title: Strings.blankString, message: ErrorMessages.invalidEmail)
        }
    }
}
extension EmailView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
