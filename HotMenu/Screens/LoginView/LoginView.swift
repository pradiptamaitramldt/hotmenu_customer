//
//  LoginView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 05/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import CoreLocation

class LoginView: UITableViewController {
    
    @IBOutlet weak var textField_countryCode: CustomTextField!
    @IBOutlet weak var textField_phone: CustomTextField!
    @IBOutlet weak var textField_password: CustomTextField!
    @IBOutlet weak var buttonForgotPassword: CustomButton!
    @IBOutlet weak var buttonLogin: CustomButton!
    @IBOutlet weak var buttonCreateAccount: CustomButton!
    @IBOutlet weak var labelOr: CustomLabel!
    
    var locationManager: CLLocationManager = CLLocationManager()
    private var selectedCountry : Country?
    private var webService = LoginAPI()
    private var loginRequest = LoginRequest()
    private var socialLoginAPIRequest: RegistrationRequest?
    var facebook_user = SocialLoginDataModelFromSocialPlatforms()
    var google_user = SocialLoginDataModelFromSocialPlatforms()
    var currentTextField = UITextField()
    var phone: String = ""
    var otp: String = ""
    var login_type: String = ""
    var lattitude = Double()
    var longitude = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView.init(image: UIImage(named: "joseph-gonzalez-176749-unsplash"))
        tableView.backgroundView?.contentMode = .scaleAspectFill
        tableView.contentInset = UIEdgeInsets(top: -44, left: 0, bottom: 0, right: 0)
        
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        
        //Google SignIn Setup
        GIDSignIn.sharedInstance().clientID = Strings.googleSignInClientId
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        self.loginRequest.delegate = self
        self.fetchCurrentCountry()
        
        //        self.setTableViewBackgroundGradient(sender: self, UIColor.init(named: "TopGradientColor")!, UIColor.init(named: "MiddleGradientColor")!, UIColor.init(named: "BottomGradientColor")!)
        self.setup()
    }
    
    func setup() {
        self.textField_phone.attributedPlaceholder = NSAttributedString(string: Strings.placeholderMobileNumber, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.textField_phone.fontSize =  17
        self.textField_phone.textColor = .white
        self.textField_phone.delegate = self
        self.textField_phone.backgroundColor = .clear
        self.textField_phone.tintColor = .white
        
        self.textField_countryCode.delegate = self
        self.textField_countryCode.backgroundColor = .clear
        self.textField_countryCode.textColor = .white
        
        self.textField_password.withImage(direction: .Left, image: #imageLiteral(resourceName: "lock") ,placeholderText : Strings.placeholderPassword)
        self.textField_password.cornerRadius = 10
        self.textField_password.fontSize =  17
        self.textField_password.textColor = .white
        self.textField_password.delegate = self
        self.textField_password.backgroundColor = UIColor.init(named: "TextFieldBGColor")
        self.textField_password.tintColor = .white
        
        self.buttonForgotPassword.setNormalTitle(Strings.forgotPassword)
        self.buttonForgotPassword.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonForgotPassword.setTitleFont(fontWeight: .thin, ofSize: 14.0)
        
        self.buttonLogin.setNormalTitle(Strings.loginButton)
        self.buttonLogin.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonLogin.cornerRadius = 10
        self.buttonLogin.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonLogin.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
        self.buttonCreateAccount.setNormalTitle(Strings.createAccount)
        self.buttonCreateAccount.setTitleColor(UIColor.init(named: "YellowColor"), for: .normal)
        self.buttonCreateAccount.setTitleFont(fontWeight: .thin, ofSize: 15.0)
        
        self.labelOr.setTextFont(fontWeight: .regular , ofSize: 14)
        self.labelOr.text = Strings.orConnectWith
        self.labelOr.textColor = UIColor.init(named: "WhiteColor")
        
        self.view.addhideKeyboardFeatureOnTouch()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let userId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        let loginId = UserDefaultsManager.shared.getStringValue(forKey: .loginId)
        
        if userId != "" && loginId != "" {
            let homeView = HomeView.instantiateFromAppStoryboard(appStoryboard: .home)
            self.navigationController?.pushViewController(homeView, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "facebookSegue" {
            let secondVC: EmailView = segue.destination as! EmailView
            secondVC.delegate = self
        }
    }
    
    private func openCountryPicker() {
        let countryPickerView = CountryPickerViewController.instantiateFromAppStoryboard(appStoryboard: .countryPicker)
        countryPickerView.delegate = self
        countryPickerView.modalPresentationStyle = .fullScreen
        self.present(countryPickerView, animated: true, completion: nil)
    }
    
    public func fetchCurrentCountry() {
        var countries: [Country] = []
        if let url = Bundle.main.url(forResource: "countryCodes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                countries = try decoder.decode([Country].self, from: data)
            } catch {
                Utility.log("error:\(error)")
            }
        }
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            for country in countries {
                if countryCode.lowercased() == country.code?.lowercased() {
                    self.selectedCountry = country
                    self.textField_countryCode.text = "\(country.dialCode ?? "")"
                    self.textField_countryCode.textColor = .white
                    self.textField_countryCode.withImageForMobile(direction: .Left, image: UIImage(named: country.code!.lowercased()) ?? UIImage(named: "call")!, placeholderText: "")
                    self.textField_countryCode.textAlignment = .left
                    self.loginRequest.country_code = country.dialCode ?? ""
                    break
                }
            }
        }
    }
    
    func setTableViewBackgroundGradient(sender: UITableViewController, _ topColor:UIColor, _ middleColor:UIColor, _ bottomColor:UIColor) {
        
        let gradientBackgroundColors = [topColor.cgColor, middleColor.cgColor, bottomColor.cgColor]
        let gradientLocations = [0.0,0.7,1.0]
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientBackgroundColors
        gradientLayer.locations = gradientLocations as [NSNumber]
        
        gradientLayer.frame = sender.tableView.bounds
        let backgroundView = UIView(frame: sender.tableView.bounds)
        let image_view = UIImageView.init(image: UIImage(named: "joseph-gonzalez-176749-unsplash"))
        image_view.bounds = sender.tableView.bounds
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        backgroundView.addSubview(image_view)
        sender.tableView.backgroundView = backgroundView
        tableView.backgroundView?.contentMode = .scaleAspectFill
        tableView.contentInset = UIEdgeInsets(top: -44, left: 0, bottom: 0, right: 0)
    }
    
    //MARK: - Web Service
    private func loginUser() {
        self.dataStartedLoading()
        self.webService.login(loginRequest: self.loginRequest, imageData: nil) { (success, message, loginResponse, error) in
            self.dataFinishedLoading()
            if success {
                let statusCode = loginResponse?.statuscode
                if statusCode == 422 {
                    let message = loginResponse?.message
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message ?? "")
                }else if statusCode == 410 {
                    guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                        return
                    }
                    UserDefaultsManager.shared.setStringValue(data.authToken ?? "", forKey: .acccessToken)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.email ?? "", forKey: .email)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.fullName ?? "", forKey: .fullName)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.customerID ?? "", forKey: .userId)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.profileImage ?? "", forKey: .profileImage)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.phone ?? "", forKey: .phone)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCode ?? "", forKey: .isdCode)
                    self.phone = data.userDetails?.phone ?? ""
                    self.otp = data.userDetails?.otp ?? ""
                    if data.userDetails?.loginId != nil {
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.loginId ?? "", forKey: .loginId)
                    }
                    self.customerOTPVerifyPending()
                }else if statusCode == 200 {
                    guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                        return
                    }
                    UserDefaultsManager.shared.setStringValue(data.authToken ?? "", forKey: .acccessToken)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.email ?? "", forKey: .email)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.fullName ?? "", forKey: .fullName)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.customerID ?? "", forKey: .userId)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.profileImage ?? "", forKey: .profileImage)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.phone ?? "", forKey: .phone)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCode ?? "", forKey: .isdCode)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.sid ?? "", forKey: .sid)
                    if data.userDetails?.loginId != nil {
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.loginId ?? "", forKey: .loginId)
                        self.customerLoginSuccess(loginData: successLoginResponse)
                    }else{
                        self.otp = data.userDetails?.otp ?? ""
                        self.customerOTPVerifyPending()
                    }
                    
                    Utility.log("Finally it's done")
                }
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    private func socialLoginUser() {
        self.dataStartedLoading()
        self.webService.login(loginRequest: self.loginRequest, imageData: nil) { (success, message, loginResponse, error) in
            self.dataFinishedLoading()
            if success {
                let statusCode = loginResponse?.statuscode
                if statusCode == 422 {
                    let message = loginResponse?.message
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message ?? "")
                }else if statusCode == 410 {
                    guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                        return
                    }
                    UserDefaultsManager.shared.setStringValue(data.authToken ?? "", forKey: .acccessToken)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.email ?? "", forKey: .email)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.fullName ?? "", forKey: .fullName)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.customerID ?? "", forKey: .userId)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.profileImage ?? "", forKey: .profileImage)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.phone ?? "", forKey: .phone)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCode ?? "", forKey: .isdCode)
                    self.phone = data.userDetails?.phone ?? ""
                    self.otp = data.userDetails?.otp ?? ""
                    if data.userDetails?.loginId != nil {
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.loginId ?? "", forKey: .loginId)
                    }
                    self.customerOTPVerifyPending()
                }else if statusCode == 200 {
                    guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                        return
                    }
                    UserDefaultsManager.shared.setStringValue(data.authToken ?? "", forKey: .acccessToken)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.email ?? "", forKey: .email)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.fullName ?? "", forKey: .fullName)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.customerID ?? "", forKey: .userId)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.profileImage ?? "", forKey: .profileImage)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.phone ?? "", forKey: .phone)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCode ?? "", forKey: .isdCode)
                    UserDefaultsManager.shared.setStringValue(data.userDetails?.sid ?? "", forKey: .sid)
                    if data.userDetails?.loginId != nil {
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.loginId ?? "", forKey: .loginId)
                        self.customerLoginSuccess(loginData: successLoginResponse)
                    }else{
                        self.otp = data.userDetails?.otp ?? ""
                        self.customerOTPVerifyPending()
                    }
                    
                    Utility.log("Finally it's done")
                }else if statusCode == 201 {
                    if self.login_type == Strings.loginTypeFB {
                        if self.facebook_user.socialEmail != nil {
                            DispatchQueue.main.async {
                                self.loginWithSocialID(socialDetails: self.facebook_user, loginType: Strings.loginTypeFB)
                            }
                        }else{
                            DispatchQueue.main.async {
                                let emailView = EmailView.instantiateFromAppStoryboard(appStoryboard: .login)
                                emailView.modalPresentationStyle = .overCurrentContext
                                emailView.modalTransitionStyle = .crossDissolve
                                emailView.delegate = self
                                self.present(emailView, animated: true, completion: nil)
                            }
                        }
                    }else if self.login_type == Strings.loginTypeGoogle {
                        DispatchQueue.main.async {
                            self.loginWithSocialID(socialDetails: self.google_user, loginType: Strings.loginTypeGoogle)
                        }
                    }
                }
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    //MARK: - Navigation
    func customerOTPVerifyPending() {
        //navigate to phone number page
        DispatchQueue.main.async {
            let otpView = OTPVerificationView.instantiateFromAppStoryboard(appStoryboard: .verify)
            otpView.previousView = Strings.login
            otpView.phone = self.phone
            otpView.otp = self.otp
            self.navigationController?.pushViewController(otpView, animated: true)
        }
    }
    
    func customerLoginSuccess(loginData: LoginResponseModel) {
        //navigate to phone number page
        DispatchQueue.main.async {
//            let homeView = HomeView.instantiateFromAppStoryboard(appStoryboard: .home)
//            homeView.lattitude = self.lattitude
//            homeView.longitude = self.longitude
//            self.navigationController?.pushViewController(homeView, animated: true)
            let welcomeView = WelcomeView.instantiateFromAppStoryboard(appStoryboard: .verify)
            self.navigationController?.pushViewController(welcomeView, animated: true)
        }
    }
    
    //MARK: - Social Login
    private func loginWithSocialID(socialDetails: SocialLoginDataModelFromSocialPlatforms, loginType: String) {
//        self.socialLoginAPIRequest = RegistrationRequest(socialLoginData: socialDetails)
//        self.socialLoginAPIRequest?.delegate = self
//        self.socialLoginAPIRequest?.validateData()
        
        let registrationView = RegistrationView.instantiateFromAppStoryboard(appStoryboard: .registration)
        registrationView.loginType = loginType
        registrationView.registrationRequest = RegistrationRequest(socialLoginData: socialDetails)
        self.navigationController?.pushViewController(registrationView, animated: true)
    }
    
    @IBAction func buttonForgotPasswordAction(_ sender: Any) {
        let forgotPasswordView = ForgotPasswordView.instantiateFromAppStoryboard(appStoryboard: .login)
        self.navigationController?.pushViewController(forgotPasswordView, animated: true)
    }
    @IBAction func buttonLoginAction(_ sender: Any) {
        self.loginRequest.country_code = self.textField_countryCode.text ?? ""
        self.loginRequest.phone = self.textField_phone.text ?? ""
        self.loginRequest.password = self.textField_password.text ?? ""
        self.loginRequest.login_type = Strings.loginTypeGeneral
        self.loginRequest.app_type = Strings.appType
        self.loginRequest.device_token = "1234"
        self.loginRequest.push_mode = "S"
        self.login_type = Strings.loginTypeGeneral
        self.loginRequest.validateData()
    }
    @IBAction func buttonCreateAccountAction(_ sender: Any) {
        let registrationView = RegistrationView.instantiateFromAppStoryboard(appStoryboard: .registration)
        self.navigationController?.pushViewController(registrationView, animated: true)
    }
    @IBAction func buttonFBAction(_ sender: Any) {
        let facebookLoginManager: LoginManager = LoginManager()
        facebookLoginManager.logIn(permissions: ["email", "public_profile"], from: self) { [unowned self] (loginManagerLoginResult, error) in
            if error != nil {
                self.showAlertWithTitleOkAction(Strings.blankString, message: error!.localizedDescription, okTitle: Strings.ok, okAction: nil)
            } else if loginManagerLoginResult!.isCancelled {
                //self.showAlertWithTitleOkAction(Strings.blankString, message: "Login cancelled by the user", okTitle: Strings.ok, okAction: nil)
            } else {
                if(loginManagerLoginResult!.grantedPermissions.contains("email")) {
                    self.getUserDataFromFacebook()
                }
            }
            facebookLoginManager.logOut()
        }
    }
    
    func getUserDataFromFacebook() {
        if AccessToken.current != nil {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    if let resultDictionary = result as? [String : AnyObject?] {
                        self.login_type = Strings.loginTypeFB
                        Utility.log(resultDictionary)
                        var facebookUser = SocialLoginDataModelFromSocialPlatforms()
                        facebookUser.type = Strings.loginTypeFB
                        if let socialIdFB = resultDictionary["id"] as? String {
                            facebookUser.socialID = socialIdFB
                        }
                        if let firstNameFB = resultDictionary["first_name"] as? String {
                            facebookUser.socialFirstName = firstNameFB
                        }
                        if let lastNameFB = resultDictionary["last_name"] as? String {
                            facebookUser.socialLastName = lastNameFB
                        }
                        if let pictureUrlFB = resultDictionary["picture"] as? [String:Any] {
                            if let photoData = pictureUrlFB["data"] as? [String:Any] {
                                if let photoUrl = photoData["url"] as? String {
                                    facebookUser.socialImageUrl = photoUrl
                                }
                            }
                        }
                        if let emailFB = resultDictionary["email"] as? String {
                            facebookUser.socialEmail = emailFB
                            //self.loginWithSocialID(socialDetails: facebookUser, loginType: Strings.loginTypeFB)
                        }else{
//                            self.facebook_user = facebookUser
//                            let otpVerificationView = EmailView.instantiateFromAppStoryboard(appStoryboard: .login)
//                            otpVerificationView.modalPresentationStyle = .overCurrentContext
//                            otpVerificationView.modalTransitionStyle = .crossDissolve
//                            otpVerificationView.delegate = self
//                            self.present(otpVerificationView, animated: true, completion: nil)
                        }
                        self.facebook_user = facebookUser
                        self.loginRequest.country_code = ""
                        self.loginRequest.phone = facebookUser.socialID ?? ""
                        self.loginRequest.password = ""
                        self.loginRequest.login_type = Strings.loginTypeFB
                        self.loginRequest.app_type = Strings.appType
                        self.loginRequest.device_token = "1234"
                        self.loginRequest.push_mode = "S"
                        
                        self.loginRequest.validateData()
                    }
                }
            })
        }
    }
    @IBAction func buttonGoogleAction(_ sender: Any) {
        self.login_type = Strings.loginTypeGoogle
        GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK: - Table view data source
    
    /*
     override func numberOfSections(in tableView: UITableView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }
     
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     // #warning Incomplete implementation, return the number of rows
     return 0
     }
     
     
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension LoginView: EmailActionDelegate {
    func emailAction(email: String) {
        self.facebook_user.socialEmail = email
        
        self.loginWithSocialID(socialDetails: self.facebook_user, loginType: Strings.loginTypeFB)
    }
}
extension LoginView : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.textField_countryCode {
            self.currentTextField = textField
            textField.resignFirstResponder()
            self.openCountryPicker()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension LoginView : CountryPickerViewControllerDelegate {
    
    func didSelectCountry(country: Country) {
        self.selectedCountry = country
        if self.currentTextField == self.textField_countryCode {
            self.textField_countryCode.text = "\(country.dialCode ?? "")"
            self.textField_countryCode.textAlignment = .left
            self.textField_countryCode.withImageForMobile(direction: .Left, image: UIImage(named: country.code!.lowercased()) ?? UIImage(named: "call")!, placeholderText: "")
            self.textField_countryCode.textColor = .white
            self.loginRequest.country_code = country.dialCode ?? ""
        }
    }
}
extension LoginView: LoginRequestDelegate {
    func invalidLoginData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func loginValidationPassed() {
        if self.login_type == Strings.loginTypeGeneral {
            self.loginUser()
        }else{
            self.socialLoginUser()
        }
    }
    
}
extension LoginView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension LoginView: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            Utility.log("\(error.localizedDescription)")
        } else {
            var googleUser = SocialLoginDataModelFromSocialPlatforms()
            //check all fields with if let
            if user != nil {
                //let userId = user.userID // For client-side use only! (but this is unique with user)
                googleUser.type = "google"
                googleUser.socialID = user.userID//user.authentication.idToken // Safe to send to the server (but idToken is generating randomly, not unique to user)
                googleUser.socialFirstName = user.profile.givenName
                googleUser.socialLastName = user.profile.familyName
                googleUser.socialEmail = user.profile.email
                if user.profile.hasImage {
                    googleUser.socialImageUrl = user.profile.imageURL(withDimension: 100).absoluteString
                }
            }
            self.google_user = googleUser
            GIDSignIn.sharedInstance().signOut()
            
            self.loginRequest.country_code = ""
            self.loginRequest.phone = googleUser.socialID ?? ""
            self.loginRequest.password = ""
            self.loginRequest.login_type = Strings.loginTypeGoogle
            self.loginRequest.app_type = Strings.appType
            self.loginRequest.device_token = "1234"
            self.loginRequest.push_mode = "S"
            
            self.loginRequest.validateData()
            
//            self.loginWithSocialID(socialDetails: googleUser, loginType: Strings.loginTypeGoogle)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
extension LoginView: RegistrationRequestDelegate {
    func invalidRegistrationData(message: String) {
        self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
    }
    
    func registrationValidationPassed() {
        if self.socialLoginAPIRequest != nil {
            self.showLoader()
            self.webService.loginWithSocial(socialLoginRequest: self.socialLoginAPIRequest!) { (code, success, message, socialLoginResponse, error) in
                DispatchQueue.main.async {
                    self.hideLoader()
                }
                if success {
                    if code == 401 {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: message, preferredStyle: .actionSheet)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                                let regView = RegistrationView.instantiateFromAppStoryboard(appStoryboard: .registration)
                                regView.loginType = "Social"
                                regView.registrationRequest = self.socialLoginAPIRequest!
                                self.navigationController?.pushViewController(regView, animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }else{
                        guard let successLoginResponse = socialLoginResponse, let successLoginResponseData = successLoginResponse.responseData, let userProfile = successLoginResponseData.userDetails else {
                            return
                        }
                        Utility.log("Finally it's done")
                        //Save values to UserDefaults
                        UserDefaultsManager.shared.setStringValue(successLoginResponseData.authToken ?? "", forKey: .acccessToken)
                        if successLoginResponse.statuscode == 201 {
                            //phone not verified
                            UserDefaultsManager.shared.setStringValue(userProfile.email ?? "", forKey: .email)
                            UserDefaultsManager.shared.setStringValue(userProfile.fullName ?? "", forKey: .fullName)
                            UserDefaultsManager.shared.setStringValue(userProfile.countryCode ?? "", forKey: .isdCode)
                            UserDefaultsManager.shared.setStringValue(userProfile.phone ?? "", forKey: .phone)
                            UserDefaultsManager.shared.setStringValue(userProfile.profileImage ?? "", forKey: .profileImage)
                            UserDefaultsManager.shared.setStringValue(userProfile.customerID ?? "", forKey: .userId)
                            //                            self.phoneNotVerified(loginData: successLoginResponseData)
                        } else if successLoginResponse.statuscode == 200 {
                            UserDefaultsManager.shared.setStringValue(userProfile.email ?? "", forKey: .email)
                            UserDefaultsManager.shared.setStringValue(userProfile.fullName ?? "", forKey: .fullName)
                            UserDefaultsManager.shared.setStringValue(userProfile.countryCode ?? "", forKey: .isdCode)
                            UserDefaultsManager.shared.setStringValue(userProfile.phone ?? "", forKey: .phone)
                            UserDefaultsManager.shared.setStringValue(userProfile.profileImage ?? "", forKey: .profileImage)
                            UserDefaultsManager.shared.setStringValue(userProfile.customerID ?? "", forKey: .userId)
                            //                            self.loginSuccessful()
                        }
                    }
                } else {
                    if error == .forbidden {
                        self.userForbidden()
                    } else {
                        DispatchQueue.main.async {
                            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
                        }
                    }
                }
                
            }
        }
    }
}
extension LoginView : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
//        Utility.lattitude = locValue.latitude
//        Utility.longitude = locValue.longitude
    }
}
