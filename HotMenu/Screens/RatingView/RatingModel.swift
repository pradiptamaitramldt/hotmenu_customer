//
//  RatingModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct ViewRatingRequest {
    
    var customerId: String
    var menuId: String
    
    init() {
        self.customerId = ""
        self.menuId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["menuId"] = self.menuId
        
        return tempDictionary
    }
}

// MARK: - ViewRatingResponseModel
struct ViewRatingResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [ViewRatingResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct ViewRatingResponseDatum: Codable {
    let review, id, menuID: String?
    let customerID: CustomerID?
    let rating, createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case review
        case id = "_id"
        case menuID = "menuId"
        case customerID = "customerId"
        case rating, createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - CustomerID
struct CustomerID: Codable {
    let email, password, socialID, countryCode: String?
    let countryCodeID: String?
    let phone: Int?
    let gender: String?
    let profileImage: String?
    let verifyOtp: String?
    let lastLogin: String?
    let appType, deviceToken, id, fullName: String?
    let userType, loginType, status, createdAt: String?
    let updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case email, password
        case socialID = "socialId"
        case countryCode
        case countryCodeID = "countryCodeId"
        case phone, gender, profileImage, verifyOtp, lastLogin, appType, deviceToken
        case id = "_id"
        case fullName, userType, loginType, status, createdAt, updatedAt
        case v = "__v"
    }
}

struct AddRatingRequest {
    
    var customerId: String
    var menuId: String
    var rating: String
    var review: String
    
    var delegate: AddRatingRequestDelegate?
    
    init() {
        self.customerId = ""
        self.menuId = ""
        self.rating = ""
        self.review = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.rating != "0.0" {
            if self.review != Strings.placeholderTextView {
                if self.review != "" {
                    delegate.ratingValidationPassed()
                }else{
                    delegate.invalidRatingData(message: ErrorMessages.invalidReview)
                }
            }else{
                delegate.invalidRatingData(message: ErrorMessages.invalidReview)
            }
        }else{
            delegate.invalidRatingData(message: ErrorMessages.invalidRate)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["menuId"] = self.menuId
        tempDictionary["rating"] = self.rating
        tempDictionary["review"] = self.review
        
        return tempDictionary
    }
}

protocol AddRatingRequestDelegate {
    
    func invalidRatingData(message: String)
    
    func ratingValidationPassed()
    
}

// MARK: - AddRatingResponseModel
struct AddRatingResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: AddRatingResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct AddRatingResponseData: Codable {
    let review, id, menuID, customerID: String?
    let rating, createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case review
        case id = "_id"
        case menuID = "menuId"
        case customerID = "customerId"
        case rating, createdAt, updatedAt
        case v = "__v"
    }
}
