//
//  AddReviewView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import Cosmos

class AddReviewView: UIViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var labelInfo: CustomLabel!
    @IBOutlet weak var textViewReview: UITextView!
    @IBOutlet weak var buttonAddReview: CustomButton!
    
    var menuID: String = ""
    
    private var webService = RatingAPI()
    private var addRatingRequest = AddRatingRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addRatingRequest.delegate = self
        self.textViewReview.delegate = self
        self.viewRating.rating = 0
        self.setup()
        self.view.addhideKeyboardFeatureOnTouch()
    }
    
    func setup() {
        
        self.buttonAddReview.setNormalTitle(Strings.submitButton)
        self.buttonAddReview.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonAddReview.cornerRadius = 10
        self.buttonAddReview.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonAddReview.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.ratingHeader
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelInfo.setTextFont(fontWeight: .regular , ofSize: 17)
        self.labelInfo.text = "Rate your experience"
        self.labelInfo.textColor = UIColor.init(named: "HomeTextLightColor")
        
        self.viewRating.settings.starSize = 40
        self.viewRating.settings.starMargin = 10
        
        self.textViewReview.text = Strings.placeholderTextView
        self.textViewReview.font = UIFont(name: "CeraPro-Light", size: 14.0)
        
        self.viewRating.didFinishTouchingCosmos = { rating in
            print(rating)
        }
    }
    
    func addReview() {
        self.dataStartedLoading()
        self.webService.addRating(addRatingRequest: self.addRatingRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse else {
                    return
                }
                
                self.showSuccessAlertWithTitle(title: Strings.blankString, message: successDashboardResponse.message ?? "Rating successful.")
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonAddReviewAction(_ sender: Any) {
        
        self.addRatingRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addRatingRequest.menuId = self.menuID
        self.addRatingRequest.rating = "\(self.viewRating.rating)"
        self.addRatingRequest.review = self.textViewReview.text
        self.addRatingRequest.validateData()
    }
    
}
extension AddReviewView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension AddReviewView: AddRatingRequestDelegate {
    func invalidRatingData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func ratingValidationPassed() {
        self.addReview()
    }
}
extension AddReviewView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == Strings.placeholderTextView)
        {
            textView.text = ""
        }
        textView.becomeFirstResponder() //Optional
    }

    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = Strings.placeholderTextView
        }
        textView.resignFirstResponder()
    }
}
