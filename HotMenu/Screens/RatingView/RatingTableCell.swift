//
//  RatingTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class RatingTableCell: UITableViewCell {

    @IBOutlet weak var imageViewRater: UIImageView!
    @IBOutlet weak var labelRaterName: CustomLabel!
    @IBOutlet weak var labelRate: CustomLabel!
    @IBOutlet weak var viewRatingBG: UIView!
    @IBOutlet weak var labelReview: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
