//
//  RatingView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 26/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class RatingView: UIViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var buttonAddReview: CustomButton!
    @IBOutlet weak var labelNoData: CustomLabel!
    
    private var webService = RatingAPI()
    private var viewRatingRequest = ViewRatingRequest()
        
    var reviewArray: [ViewRatingResponseDatum] = []
    var menuID: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view.rowHeight = UITableView.automaticDimension
        self.table_view.estimatedRowHeight = 44.0
        
        self.fetchReviewList()
        self.setup()
    }
    
    func setup() {
        self.buttonAddReview.setNormalTitle(Strings.addReviewButton)
        self.buttonAddReview.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonAddReview.cornerRadius = 10
        self.buttonAddReview.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonAddReview.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.ratingHeader
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelNoData.isHidden = true
        self.labelNoData.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelNoData.text = Strings.noReviewFound
        self.labelNoData.textColor = UIColor.init(named: "HomeTextColor")
    }
    
    func fetchReviewList() {
        self.viewRatingRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.viewRatingRequest.menuId = self.menuID
        self.dataStartedLoading()
        self.webService.viewRatingList(viewRatingRequest: self.viewRatingRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.reviewArray = data
                                
                DispatchQueue.main.async {
                    if self.reviewArray.count>0 {
                        self.table_view.isHidden = false
                        self.labelNoData.isHidden = true
                        self.table_view.reloadData()
                    }else{
                        self.table_view.isHidden = true
                        self.labelNoData.isHidden = false
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonAddReviewAction(_ sender: Any) {
        let reviewView = AddReviewView.instantiateFromAppStoryboard(appStoryboard: .rating)
        reviewView.menuID = self.menuID
        self.navigationController?.pushViewController(reviewView, animated: true)
    }
}
extension RatingView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension RatingView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviewArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ratingTableCell", for: indexPath) as! RatingTableCell
        
        cell.selectionStyle = .none
        let itemImage = self.reviewArray[indexPath.row].customerID?.profileImage ?? ""
        cell.imageViewRater.sd_setImage(with: URL(string: itemImage), placeholderImage: UIImage(named:"user_placeholder"), options: .progressiveLoad, completed: nil)
        
        cell.imageViewRater.contentMode = .scaleAspectFill
        
        cell.labelRaterName.text = self.reviewArray[indexPath.row].customerID?.fullName
        cell.labelReview.text = self.reviewArray[indexPath.row].review
        cell.labelRate.text = self.reviewArray[indexPath.row].rating
        
        cell.labelRaterName.setTextFont(fontWeight: .medium, ofSize: 14)
        cell.labelRaterName.textColor = UIColor.init(named: "HomeTextColor")
        
        cell.labelRate.setTextFont(fontWeight: .regular, ofSize: 12)
        cell.labelRate.textColor = UIColor.init(named: "YellowColor")
        
        cell.labelReview.setTextFont(fontWeight: .light, ofSize: 12)
        cell.labelReview.textColor = UIColor.init(named: "HomeTextLightColor")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

