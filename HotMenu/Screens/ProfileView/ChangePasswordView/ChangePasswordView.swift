//
//  ChangePasswordView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 19/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class ChangePasswordView: UITableViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var textFieldCurrentPassword: CustomTextField!
    @IBOutlet weak var textFieldNewPassword: CustomTextField!
    @IBOutlet weak var textFieldConfirmPassword: CustomTextField!
    @IBOutlet weak var buttonUpdate: CustomButton!
    
    private var webService = ChangePasswordAPI()
    private var changePasswordRequest = ChangePasswordRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.changePasswordRequest.delegate = self
        self.setupView()
    }

    private func setupView() {
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .white
        
        self.labelHeader.setTextFont(fontWeight: .medium, ofSize: 18)
        self.labelHeader.text = "Change Password"
        self.labelHeader.textColor = UIColor(named: "HomeTextColor")
        
        self.textFieldCurrentPassword.withImage(direction: .Left, image: UIImage(named: "lock-dark"), placeholder: "Current Password")
        self.textFieldCurrentPassword.cornerRadius = 10
        self.textFieldCurrentPassword.fontSize = 17
        self.textFieldCurrentPassword.setFont(fontWeight: .regular, size: 16)
        self.textFieldCurrentPassword.textColor = UIColor(named: "HomeTextColor")
        self.textFieldCurrentPassword.delegate = self
        self.textFieldCurrentPassword.backgroundColor = UIColor(named: "TextFieldBGColorDark")
        self.textFieldCurrentPassword.tintColor = .darkGray
        
        self.textFieldNewPassword.withImage(direction: .Left, image: UIImage(named: "lock-dark"), placeholder: "New Password")
        self.textFieldNewPassword.fontSize = 17
        self.textFieldNewPassword.setFont(fontWeight: .regular, size: 16)
        self.textFieldNewPassword.textColor = UIColor(named: "HomeTextColor")
        self.textFieldNewPassword.delegate = self
        self.textFieldNewPassword.backgroundColor = UIColor(named: "TextFieldBGColorDark")
        self.textFieldNewPassword.tintColor = .darkGray
        
        self.textFieldConfirmPassword.withImage(direction: .Left, image: UIImage(named: "lock-dark"), placeholder: "Confirm Password")
        self.textFieldConfirmPassword.cornerRadius = 10
        self.textFieldConfirmPassword.fontSize = 17
        self.textFieldConfirmPassword.setFont(fontWeight: .regular, size: 16)
        self.textFieldConfirmPassword.textColor = UIColor(named: "HomeTextColor")
        self.textFieldConfirmPassword.delegate = self
        self.textFieldConfirmPassword.backgroundColor = UIColor(named: "TextFieldBGColorDark")
        self.textFieldConfirmPassword.tintColor = .darkGray
        
        self.buttonUpdate.setNormalTitle("UPDATE")
        self.buttonUpdate.backgroundColor = UIColor(named: "ButtonColor")
        self.buttonUpdate.cornerRadius = 10
        self.buttonUpdate.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonUpdate.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
    }
    
    func changePassword() {
        
        self.dataStartedLoading()
        self.webService.changePassword(changePasswordRequest: self.changePasswordRequest, imageData: nil) { (success, message, loginResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successLoginResponse = loginResponse else {
                    return
                }
                
                let message = successLoginResponse.message
                DispatchQueue.main.async {
                    let successAlert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
                    
                    successAlert.addAction(UIAlertAction(title: Strings.ok, style: .default) { (action:UIAlertAction!) in
                        
                    })
                    
                    self.present(successAlert, animated: true)
                }
                                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    @IBAction func buttonBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonUpdatePassword(_ sender: Any) {
        
        self.changePasswordRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.changePasswordRequest.loginId = UserDefaultsManager.shared.getStringValue(forKey: .loginId)
        self.changePasswordRequest.oldPassword = self.textFieldCurrentPassword.text ?? ""
        self.changePasswordRequest.newPassword = self.textFieldNewPassword.text ?? ""
        self.changePasswordRequest.confirmPassword = self.textFieldConfirmPassword.text ?? ""
        
        self.changePasswordRequest.validateData()
    }
}

extension ChangePasswordView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension ChangePasswordView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension ChangePasswordView: ChangePasswordRequestDelegate {
    func invalidPasswordData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func passwordValidationPassed() {
        self.changePassword()
    }
}
