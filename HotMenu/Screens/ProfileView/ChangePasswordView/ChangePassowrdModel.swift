//
//  ChangePassowrdModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 21/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct ChangePasswordRequest {
    
    var customerId: String
    var loginId: String
    var oldPassword: String
    var newPassword : String
    var confirmPassword: String
    
    var delegate: ChangePasswordRequestDelegate?
    
    init() {
        self.customerId = ""
        self.loginId = ""
        self.oldPassword = ""
        self.newPassword = ""
        self.confirmPassword = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.oldPassword.isValidPassword() {
            if self.newPassword.isValidPassword() {
                if self.confirmPassword.isValidPassword() {
                    if self.newPassword == self.confirmPassword {
                        delegate.passwordValidationPassed()
                    }else{
                        delegate.invalidPasswordData(message: ErrorMessages.passwordAndConfirmPasswordDidNotMatch)
                    }
                }else{
                    delegate.invalidPasswordData(message: ErrorMessages.invalidPassword)
                }
            }else{
                delegate.invalidPasswordData(message: ErrorMessages.invalidPassword)
            }
        }else{
            delegate.invalidPasswordData(message: ErrorMessages.invalidPassword)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["loginId"] = self.loginId
        tempDictionary["oldPassword"] = self.oldPassword
        tempDictionary["newPassword"] = self.newPassword
        tempDictionary["confirmPassword"] = self.confirmPassword
        
        return tempDictionary
    }
}

protocol ChangePasswordRequestDelegate {
    
    func invalidPasswordData(message: String)
    
    func passwordValidationPassed()
    
}

// MARK: - ChangePasswordResponseModel
struct ChangePasswordResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ChangePasswordResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct ChangePasswordResponseData: Codable {
}
