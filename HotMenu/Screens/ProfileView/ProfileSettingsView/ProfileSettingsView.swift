//
//  ProfileSettingsView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 19/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class ProfileSettingsView: UITableViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var labelAccount: CustomLabel!
    @IBOutlet weak var labelEditProfile: CustomLabel!
    @IBOutlet weak var labelChangePassword: CustomLabel!
    @IBOutlet weak var labelOther: CustomLabel!
    @IBOutlet weak var labelPrivacyPolicy: CustomLabel!
    @IBOutlet weak var labelTermsAndConditions: CustomLabel!
    @IBOutlet weak var labelLogout: CustomLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    private func setupView() {
        
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .white
        
        self.labelHeader.setTextFont(fontWeight: .medium, ofSize: 18)
        self.labelHeader.text = "Profile Settings"
        self.labelHeader.textColor = UIColor(named: "HomeTextColor")
        
        self.labelAccount.setTextFont(fontWeight: .regular, ofSize: 16)
        self.labelAccount.text = "Account"
        self.labelAccount.textColor = UIColor(named: "HomeTextLightColor")
        
        self.labelEditProfile.setTextFont(fontWeight: .medium, ofSize: 16)
        self.labelEditProfile.text = "Edit Profile"
        self.labelEditProfile.textColor = UIColor(named: "HomeTextColor")
        
        self.labelChangePassword.setTextFont(fontWeight: .medium, ofSize: 16)
        self.labelChangePassword.text = "Change Password"
        self.labelChangePassword.textColor = UIColor(named: "HomeTextColor")
        
        self.labelOther.setTextFont(fontWeight: .regular, ofSize: 16)
        self.labelOther.text = "Other"
        self.labelOther.textColor = UIColor(named: "HomeTextLightColor")
        
        self.labelPrivacyPolicy.setTextFont(fontWeight: .medium, ofSize: 16)
        self.labelPrivacyPolicy.text = "Privacy Policy"
        self.labelPrivacyPolicy.textColor = UIColor(named: "HomeTextColor")
        
        self.labelTermsAndConditions.setTextFont(fontWeight: .medium, ofSize: 16)
        self.labelTermsAndConditions.text = "Terms & Conditions"
        self.labelTermsAndConditions.textColor = UIColor(named: "HomeTextColor")
        
        self.labelLogout.setTextFont(fontWeight: .medium, ofSize: 16)
        self.labelLogout.text = "Logout"
        self.labelLogout.textColor = UIColor(named: "ButtonColor")
    }

    @IBAction func buttonBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonLogoutAction(_ sender: Any) {
        DispatchQueue.main.async {
            let successAlert = UIAlertController(title: "Alert", message: "Do you want to logout?", preferredStyle: UIAlertController.Style.alert)
            
            successAlert.addAction(UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction!) in
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: LoginView.self) {
                        UserDefaultsManager.shared.clearAllValuesExceptDeviceToken()
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            })
            
            successAlert.addAction(UIAlertAction(title: "NO", style: .default) { (action:UIAlertAction!) in
                
            })
            
            self.present(successAlert, animated: true)
        }
    }
}

extension ProfileSettingsView {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 2:
            //Edit Profile
            let editProfileVC = EditProfileView.instantiateFromAppStoryboard(appStoryboard: .profile)
            self.navigationController?.pushViewController(editProfileVC, animated: true)
        case 3:
            //Change Password
            let changePasswordVC = ChangePasswordView.instantiateFromAppStoryboard(appStoryboard: .profile)
            self.navigationController?.pushViewController(changePasswordVC, animated: true)
            break
        default:
            break
        }
    }
    
}
