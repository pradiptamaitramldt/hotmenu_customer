//
//  EditProfileModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 21/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct ViewProfileRequest {
    
    var customerId: String
    var loginId: String
    
    init() {
        self.customerId = ""
        self.loginId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["loginId"] = self.loginId
        
        return tempDictionary
    }
}

// MARK: - ViewProfileResponseModel
struct ViewProfileResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ViewProfileResponseData?
    
    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct ViewProfileResponseData: Codable {
    let userDetails: ViewProfileUserDetails?
    let authToken: String?
}

// MARK: - UserDetails
struct ViewProfileUserDetails: Codable {
    let fullName, email, countryCode, countryCodeID: String?
    let phone, socialID, customerID, loginID: String?
    let userType, loginType, profileImage: String?
    
    enum CodingKeys: String, CodingKey {
        case fullName, email, countryCode
        case countryCodeID = "countryCodeId"
        case phone
        case socialID = "socialId"
        case customerID = "customerId"
        case loginID = "loginId"
        case userType, loginType, profileImage
    }
}

struct EditProfileRequest {
    
    var customerId: String
    var loginId: String
    var fullName: String
    var countryCode : String
    var phone: String
    var email: String
    
    var delegate: EditProfileRequestDelegate?
    
    init() {
        self.customerId = ""
        self.loginId = ""
        self.fullName = ""
        self.countryCode = ""
        self.phone = ""
        self.email = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.fullName != "" {
            if self.phone.isValidPhoneNumber() {
                if self.email.isValidEmail() {
                    delegate.profileValidationPassed()
                }else{
                    delegate.invalidProfileData(message: ErrorMessages.invalidEmail)
                }
            }else{
                delegate.invalidProfileData(message: ErrorMessages.invalidPhoneNumber)
            }
        }else{
            delegate.invalidProfileData(message: ErrorMessages.invalidName)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["loginId"] = self.loginId
        tempDictionary["fullName"] = self.fullName
        tempDictionary["countryCode"] = self.countryCode
        tempDictionary["phone"] = self.phone
        tempDictionary["email"] = self.email
        return tempDictionary
    }
}

protocol EditProfileRequestDelegate {
    
    func invalidProfileData(message: String)
    
    func profileValidationPassed()
    
}

// MARK: - EditProfileResponseModel
struct EditProfileResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: EditProfileResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct EditProfileResponseData: Codable {
}

struct ProfileImageUploadRequest {
    
    var customerId: String
    var loginId: String
    var isPicture: Bool
    
    var delegate: ProfileImageUploadRequestDelegate?
    
    init() {
        self.customerId = ""
        self.loginId = ""
        self.isPicture = false
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.isPicture == true{
            delegate.profileImageValidationPassed()
        }else{
            delegate.invalidProfileImageData(message: ErrorMessages.invalidPicture)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["loginId"] = self.loginId
        return tempDictionary
    }
}
protocol ProfileImageUploadRequestDelegate {
    
    func invalidProfileImageData(message: String)
    
    func profileImageValidationPassed()
    
}
