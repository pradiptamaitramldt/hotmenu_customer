//
//  EditProfileView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 19/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class EditProfileView: UITableViewController {
    
    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var imageViewProfileImage: UIImageView!
    @IBOutlet weak var buttonUploadProfileImage: UIButton!
    @IBOutlet weak var textFieldUserFullName: CustomTextField!
    @IBOutlet weak var textFieldUserCountryCode: CustomTextField!
    @IBOutlet weak var textFieldUserMobileNumber: CustomTextField!
    @IBOutlet weak var textFieldUserEmail: CustomTextField!
    @IBOutlet weak var buttonUpdate: CustomButton!
    
    private var selectedCountry : Country?
    private var webService = ProfileAPI()
    private var viewProfileRequest = ViewProfileRequest()
    private var editProfileRequest = EditProfileRequest()
    private var profileImageUploadRequest = ProfileImageUploadRequest()
    var currentTextField = UITextField()
    private var imagePicker = UIImagePickerController()
    var imageData = Data()
    var isImagePicked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.editProfileRequest.delegate = self
        self.profileImageUploadRequest.delegate = self
        self.fetchCurrentCountry()
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewProfile()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.imageViewProfileImage.makeCircular()
    }
    
    private func setupView() {
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .white
        
        self.labelHeader.setTextFont(fontWeight: .medium, ofSize: 18)
        self.labelHeader.text = "Edit Profile"
        self.labelHeader.textColor = UIColor(named: "HomeTextColor")
        
        self.textFieldUserFullName.withImage(direction: .Left, image: UIImage(named: "user-dark"), placeholder: Strings.placeholderName)
        self.textFieldUserFullName.cornerRadius = 10
        self.textFieldUserFullName.fontSize =  17
        self.textFieldUserFullName.setFont(fontWeight: .regular, size: 16)
        self.textFieldUserFullName.textColor = UIColor(named: "HomeTextColor")
        self.textFieldUserFullName.delegate = self
        self.textFieldUserFullName.backgroundColor = UIColor(named: "TextFieldBGColorDark")
        self.textFieldUserFullName.tintColor = .darkGray
        
        self.textFieldUserCountryCode.withImage(direction: .Left, image: UIImage(named: "smartphone-dark"), placeholder: "Dial Code")
        self.textFieldUserCountryCode.cornerRadius = 10
        self.textFieldUserCountryCode.fontSize =  17
        self.textFieldUserCountryCode.setFont(fontWeight: .regular, size: 16)
        self.textFieldUserCountryCode.textColor = UIColor(named: "HomeTextColor")
        self.textFieldUserCountryCode.delegate = self
        self.textFieldUserCountryCode.backgroundColor = UIColor(named: "TextFieldBGColorDark")
        
        self.textFieldUserMobileNumber.withImage(direction: .Left, image: UIImage(named: "smartphone-dark"), placeholder: Strings.placeholderMobileNumber)
        self.textFieldUserMobileNumber.cornerRadius = 10
        self.textFieldUserMobileNumber.fontSize =  17
        self.textFieldUserMobileNumber.setFont(fontWeight: .regular, size: 16)
        self.textFieldUserMobileNumber.textColor = UIColor(named: "HomeTextColor")
        self.textFieldUserMobileNumber.delegate = self
        self.textFieldUserMobileNumber.backgroundColor = UIColor(named: "TextFieldBGColorDark")
        self.textFieldUserMobileNumber.tintColor = .darkGray
        
        self.textFieldUserEmail.withImage(direction: .Left, image: UIImage(named: "email-dark"), placeholder: Strings.placeholderEmail)
        self.textFieldUserEmail.cornerRadius = 10
        self.textFieldUserEmail.fontSize =  17
        self.textFieldUserEmail.setFont(fontWeight: .regular, size: 16)
        self.textFieldUserEmail.textColor = UIColor(named: "HomeTextColor")
        self.textFieldUserEmail.delegate = self
        self.textFieldUserEmail.backgroundColor = UIColor(named: "TextFieldBGColorDark")
        self.textFieldUserEmail.tintColor = .darkGray
        
        self.buttonUpdate.setNormalTitle("UPDATE")
        self.buttonUpdate.backgroundColor = UIColor(named: "ButtonColor")
        self.buttonUpdate.cornerRadius = 10
        self.buttonUpdate.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonUpdate.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
        self.view.addhideKeyboardFeatureOnTouch()
    }
    
    func viewProfile() {
        self.viewProfileRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.viewProfileRequest.loginId = UserDefaultsManager.shared.getStringValue(forKey: .loginId)
        self.dataStartedLoading()
        self.webService.viewProfile(viewProfileRequest: self.viewProfileRequest, imageData: nil) { (success, message, loginResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData, let userDetails = data.userDetails else {
                    return
                }
                
                self.setProfileView(profileData: userDetails)
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func setProfileView(profileData: ViewProfileUserDetails) {
        DispatchQueue.main.async {
            self.textFieldUserFullName.text = profileData.fullName
            self.textFieldUserEmail.text = profileData.email
            self.textFieldUserMobileNumber.text = profileData.phone
            let itemImage = profileData.profileImage ?? ""
            if itemImage != "" {
                UserDefaultsManager.shared.setStringValue(itemImage, forKey: .profileImage)
                self.imageViewProfileImage.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
                self.imageViewProfileImage.contentMode = .scaleAspectFill
            }
        }
    }
    
    func editProfile() {
        
        self.dataStartedLoading()
        self.webService.editProfile(editProfileRequest: self.editProfileRequest, imageData: nil) { (success, message, loginResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                    return
                }
                
                self.viewProfile()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func uploadProfileImage() {
        self.dataStartedLoading()
        self.webService.uploadProfileImage(registrationRequest: self.profileImageUploadRequest, imageData: self.imageData) { (success, message, loginResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                    return
                }
                
                let delayTime = DispatchTime.now() + 1.0
                print("one")
                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                    self.viewProfile()
                })
                                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    public func fetchCurrentCountry() {
        var countries: [Country] = []
        if let url = Bundle.main.url(forResource: "countryCodes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                countries = try decoder.decode([Country].self, from: data)
            } catch {
                Utility.log("error:\(error)")
            }
        }
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            for country in countries {
                if countryCode.lowercased() == country.code?.lowercased() {
                    self.selectedCountry = country
                    self.textFieldUserCountryCode.text = "\(country.dialCode ?? "")"
                    self.textFieldUserCountryCode.textAlignment = .left
                    self.textFieldUserCountryCode.textColor = .white
                    self.textFieldUserCountryCode.withImageForMobile(direction: .Left, image: UIImage(named: country.code!.lowercased()) ?? UIImage(named: "call")!, placeholderText: "")
                    self.editProfileRequest.countryCode = country.dialCode ?? ""
                    break
                }
            }
        }
    }
    private func openCountryPicker() {
        let countryPickerView = CountryPickerViewController.instantiateFromAppStoryboard(appStoryboard: .countryPicker)
        countryPickerView.delegate = self
        countryPickerView.modalPresentationStyle = .fullScreen
        self.present(countryPickerView, animated: true, completion: nil)
    }
    
    @IBAction func buttonBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonUploadProfileImageAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        self.imagePicker.delegate = self
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        self.imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func buttonUpdateProfileAction(_ sender: Any) {
        
        self.editProfileRequest.fullName = self.textFieldUserFullName.text ?? ""
        self.editProfileRequest.phone = self.textFieldUserMobileNumber.text ?? ""
        self.editProfileRequest.email = self.textFieldUserEmail.text ?? ""
        self.editProfileRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.editProfileRequest.loginId = UserDefaultsManager.shared.getStringValue(forKey: .loginId)
        
        self.editProfileRequest.validateData()
    }
}

extension EditProfileView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.textFieldUserCountryCode {
            self.currentTextField = textField
            textField.resignFirstResponder()
            self.openCountryPicker()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension EditProfileView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension EditProfileView : CountryPickerViewControllerDelegate {
    
    func didSelectCountry(country: Country) {
        self.selectedCountry = country
        if self.currentTextField == self.textFieldUserCountryCode {
            self.textFieldUserCountryCode.text = "\(country.dialCode ?? "")"
            self.textFieldUserCountryCode.textAlignment = .left
            self.textFieldUserCountryCode.withImageForMobile(direction: .Left, image: UIImage(named: country.code!.lowercased()) ?? UIImage(named: "call")!, placeholderText: "")
            self.textFieldUserCountryCode.textColor = .white
            self.editProfileRequest.countryCode = country.dialCode ?? ""
        }
    }
}
extension EditProfileView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info[.originalImage] as? UIImage {
            self.imageViewProfileImage.contentMode = .scaleAspectFill
            self.imageViewProfileImage.image = pickedImage
            self.imageData = pickedImage.jpeg(.low)!
            self.isImagePicked = true
            self.profileImageUploadRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
            self.profileImageUploadRequest.isPicture = true
            self.profileImageUploadRequest.loginId = UserDefaultsManager.shared.getStringValue(forKey: .loginId)
            self.profileImageUploadRequest.validateData()
        }
    }
}
extension EditProfileView: EditProfileRequestDelegate {
    
    func invalidProfileData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func profileValidationPassed() {
        self.editProfile()
    }
}
extension EditProfileView: ProfileImageUploadRequestDelegate {
    func invalidProfileImageData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func profileImageValidationPassed() {
        self.uploadProfileImage()
    }
}
