//
//  FoodSearchModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 04/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct FoodSearchRequest {
    
    var customerId: String
    var searchValue: String
    var lattitude: String
    var longitude: String
    
    var delegate: FoodSearchRequestDelegate?
    
    init() {
        self.customerId = ""
        self.searchValue = ""
        self.lattitude = ""
        self.longitude = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.searchValue != "" {
            delegate.foodSearchValidationPassed()
        }else{
            delegate.invalidFoodSearchData(message: ErrorMessages.invalidRate)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["searchValue"] = self.searchValue
        tempDictionary["latitude"] = self.lattitude
        tempDictionary["longitude"] = self.longitude
        
        return tempDictionary
    }
}

protocol FoodSearchRequestDelegate {
    
    func invalidFoodSearchData(message: String)
    
    func foodSearchValidationPassed()
    
}

// MARK: - FoodSearchResponseModel
struct FoodSearchResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: FoodSearchResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct FoodSearchResponseData: Codable {
    let cartCountTotal: Int?
    let list: [FoodSearchList]?
}

// MARK: - List
struct FoodSearchList: Codable {
    let id: String?
    let itemID: FavouriteItemID?
    let restaurantID, createdAt, updatedAt: String?
    let v, isFavorite, review: Int?
    let rating: Double?
    let cartMenuQuantity: Int?
    let cartID, vendorID: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case itemID = "itemId"
        case restaurantID = "restaurantId"
        case createdAt, updatedAt
        case v = "__v"
        case isFavorite, review, rating, cartMenuQuantity
        case cartID = "cartId"
        case vendorID = "vendorId"
    }
}

// MARK: - ItemID
struct FavouriteItemID: Codable {
    let itemIDDescription, discountType: String
    let discountAmount: Int
    let validityFrom, validityTo: String?
    let isActive, isApprove: Bool
    let popularity: Int
    let id, itemName, categoryID, mealTypeID: String
    let type: String
    let price, waitingTime: Int
    let menuImage: String
    let itemOptions: [ItemOption]
    let createdAt, updatedAt: String
    let v: Int
    let ingredients, recipe: JSONNull?

    enum CodingKeys: String, CodingKey {
        case itemIDDescription = "description"
        case discountType, discountAmount, validityFrom, validityTo, isActive, isApprove, popularity
        case id = "_id"
        case itemName
        case categoryID = "categoryId"
        case mealTypeID = "mealTypeId"
        case type, price, waitingTime, menuImage, itemOptions, createdAt, updatedAt
        case v = "__v"
        case ingredients, recipe
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
