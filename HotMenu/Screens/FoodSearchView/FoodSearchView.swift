//
//  FoodSearchView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 31/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class FoodSearchView: UIViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var textFieldSearch: CustomTextField!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var buttonSearch: UIButton!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var imageTick: UIImageView!
    @IBOutlet weak var imageClose: UIImageView!
    @IBOutlet weak var labelNoData: CustomLabel!
    @IBOutlet weak var labelTotalCartCount: CustomLabel!
    
    private var webService = FoodSearchAPI()
    private var foodSearchRequest = FoodSearchRequest()
    
    var totalCartCount: String = ""
    var searchResult: [FoodSearchList] = []
    var lattitude = Double()
    var longitude = Double()
    
    private var cartWebService = CartAPI()
    private var addCartRequest = AddCartRequest()
    private var updateCartRequest = UpdateCartRequest()
    private var removeCartRequest = RemoveCartRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textFieldSearch.delegate = self
        self.foodSearchRequest.delegate = self
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.textFieldSearch.text != "" {
            self.foodSearchRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
            self.foodSearchRequest.searchValue = self.textFieldSearch.text ?? ""
            self.foodSearchRequest.lattitude = "\(self.lattitude)"
            self.foodSearchRequest.longitude = "\(self.longitude)"
            self.foodSearchRequest.validateData()
        }
    }
    
    func setup() {
        self.hideCrossTick()
        
        self.labelTotalCartCount.isHidden = true
        self.labelTotalCartCount.setTextFont(fontWeight: .medium, ofSize: 12)
        self.labelTotalCartCount.textColor = UIColor.init(named: "WhiteColor")
        self.labelTotalCartCount.backgroundColor = UIColor.init(named: "ButtonColor")
        
        if self.totalCartCount == "" || self.totalCartCount == "0" {
            self.labelTotalCartCount.text = "0"
            self.labelTotalCartCount.isHidden = true
        }else{
            self.labelTotalCartCount.isHidden = false
            self.labelTotalCartCount.text = self.totalCartCount
        }
        
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.foodSearchHeader
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
        
        self.textFieldSearch.setFont(fontWeight: .regular, size: 16)
        self.textFieldSearch.placeholder = Strings.placeholder_FoodSearch
        self.textFieldSearch.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelNoData.setTextFont(fontWeight: .bold, ofSize: 25)
        self.labelNoData.text = Strings.noFoodItemFound
        self.labelNoData.textColor = UIColor.init(named: "HomeTextColor")
    }
    
    func searchFood() {
        self.dataStartedLoading()
        self.webService.searchFood(foodSearchRequest: self.foodSearchRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.searchResult = data.list ?? []
                
                DispatchQueue.main.async {
                    if data.cartCountTotal == 0 {
                        self.labelTotalCartCount.isHidden = true
                    }else{
                        self.labelTotalCartCount.isHidden = false
                        self.labelTotalCartCount.text = "\(data.cartCountTotal ?? 0)"
                    }
                    if self.searchResult.count>0 {
                        self.table_view.isHidden = false
                        self.labelNoData.isHidden = true
                        self.table_view.reloadData()
                    }else{
                        self.table_view.isHidden = true
                        self.labelNoData.isHidden = false
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func hideCrossTick() {
        self.buttonSearch.isHidden = true
        self.buttonClose.isHidden = true
        self.imageTick.isHidden = true
        self.imageClose.isHidden = true
    }
    
    func showCrossTick() {
        self.buttonSearch.isHidden = false
        self.buttonClose.isHidden = false
        self.imageTick.isHidden = false
        self.imageClose.isHidden = false
    }
    
    func addToCart(menuID: String, menuAmount: String, menuQuantity: String) {
        self.addCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addCartRequest.menuId = menuID
        self.addCartRequest.menuAmount = menuAmount
        self.addCartRequest.menuQuantity = menuQuantity
        self.dataStartedLoading()
        self.cartWebService.addToCart(addCartRequest: self.addCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.searchFood()
                                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func updateCart(cartID: String, menuID: String, menuAmount: String, menuQuantity: String) {
        self.updateCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.updateCartRequest.menuId = menuID
        self.updateCartRequest.menuAmount = menuAmount
        self.updateCartRequest.menuQuantity = menuQuantity
        self.updateCartRequest.cartId = cartID
        self.dataStartedLoading()
        self.cartWebService.updateCart(updateCartRequest: self.updateCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                self.searchFood()
                                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func removeCart(cartID: String, menuID: String, vendorID: String) {
        self.removeCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.removeCartRequest.menuId = menuID
        self.removeCartRequest.cartId = cartID
        self.dataStartedLoading()
        self.cartWebService.removeCart(removeCartRequest: self.removeCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.searchFood()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSearchAction(_ sender: Any) {
        self.view.endEditing(true)
        self.foodSearchRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.foodSearchRequest.searchValue = self.textFieldSearch.text ?? ""
        self.foodSearchRequest.lattitude = "\(self.lattitude)"
        self.foodSearchRequest.longitude = "\(self.longitude)"
        
        self.foodSearchRequest.validateData()
    }
    @IBAction func buttonCloseAction(_ sender: Any) {
        self.textFieldSearch.text = ""
        self.hideCrossTick()
        self.table_view.isHidden = true
        self.labelNoData.isHidden = false
        self.view.endEditing(true)
    }
    @IBAction func buttonCartAction(_ sender: Any) {
        let cartView = CartView.instantiateFromAppStoryboard(appStoryboard: .cart)
        self.navigationController?.pushViewController(cartView, animated: true)
    }
}
extension FoodSearchView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension FoodSearchView: FoodSearchRequestDelegate {
    func invalidFoodSearchData(message: String) {
        self.view.endEditing(true)
    }
    
    func foodSearchValidationPassed() {
        self.searchFood()
    }
}
extension FoodSearchView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if !text.isEmpty{
            self.showCrossTick()
        } else {
            self.hideCrossTick()
        }
        return true
    }
}
extension FoodSearchView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodSearchTableCell", for: indexPath) as! FoodSearchTableCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let itemImage = self.searchResult[indexPath.row].itemID?.menuImage ?? ""
        if itemImage != "" {
            cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
        }
        cell.imageViewMenuItem.contentMode = .scaleAspectFill
        cell.delegate = self
        cell.labelMenuTitle.setTextFont(fontWeight: .medium, ofSize: 16.0)
        cell.labelMenuTitle.text = self.searchResult[indexPath.row].itemID?.itemName
        cell.labelMenuTitle.textColor = UIColor.init(named: "HomeTextColor")
        
        cell.labelActualPrice.setTextFont(fontWeight: .light, ofSize: 11.0)
        cell.labelActualPrice.text = "₹\(self.searchResult[indexPath.row].itemID?.price ?? 0)"
        cell.labelActualPrice.textColor = UIColor.init(named: "HomeTextLightColor")
        cell.labelActualPrice.sizeToFit()
        
        let discountType = self.searchResult[indexPath.row].itemID?.discountType
        let price = self.searchResult[indexPath.row].itemID?.price ?? 0
        let discountPrice = self.searchResult[indexPath.row].itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        cell.labelDiscountedPrice.setTextFont(fontWeight: .regular, ofSize: 12.0)
        cell.labelDiscountedPrice.text = "₹\(offerPrice)"
        cell.labelDiscountedPrice.textColor = UIColor.init(named: "HomeTextColor")
        
        let cartQuantity = self.searchResult[indexPath.row].cartMenuQuantity
        if cartQuantity == 0 {
            cell.buttonAddCart.isHidden = false
            cell.viewCartBG.isHidden = true
        }else{
            cell.buttonAddCart.isHidden = true
            cell.viewCartBG.isHidden = false
            cell.labelCartCount.text = "\(cartQuantity ?? 0)"
        }
        
        cell.buttonAddCart.setNormalTitle(Strings.addToCartButtonSmall)
        cell.buttonAddCart.backgroundColor = UIColor.init(named: "ButtonColor")
        cell.buttonAddCart.cornerRadius = 10
        cell.buttonAddCart.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        cell.buttonAddCart.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
        cell.labelCartCount.setTextFont(fontWeight: .bold, ofSize: 16.0)
        cell.labelCartCount.textColor = UIColor.init(named: "ButtonColor")
        
        let isVegItem = self.searchResult[indexPath.row].itemID?.type
        if isVegItem == "NON VEG" {
            cell.imageViewVeg.image = UIImage(named: "nonveg")
        }else{
            cell.imageViewVeg.image = UIImage(named: "veg")
        }
        
        cell.buttonPlus.tag = indexPath.row
        cell.buttonMinus.tag = indexPath.row
        cell.buttonAddCart.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchObj = self.searchResult[indexPath.row]
        
        let mealType = MealTypeID(id: "", type: searchObj.itemID?.mealTypeID)
                
        let topItemID = ItemID(discountType: searchObj.itemID?.discountType, discountAmount: searchObj.itemID?.discountAmount, validityFrom: searchObj.itemID?.validityFrom, validityTo: searchObj.itemID?.validityTo, isActive: searchObj.itemID?.isActive ?? true, isApprove: searchObj.itemID?.isApprove ?? true, popularity: searchObj.itemID?.popularity, id: searchObj.itemID?.id, itemName: searchObj.itemID?.itemName, categoryID: searchObj.itemID?.categoryID, description: searchObj.itemID?.itemIDDescription, mealTypeID: mealType, type: searchObj.itemID?.type, price: searchObj.itemID?.price, waitingTime: searchObj.itemID?.waitingTime, menuImage: searchObj.itemID?.menuImage, itemOptions: [], createdAt: searchObj.itemID?.createdAt, updatedAt: searchObj.itemID?.updatedAt, v: searchObj.itemID?.v, ingredients: "", recipe: "")
        
        let topDishesObj = TopDish(id: searchObj.id, itemID: topItemID, restaurantID: searchObj.restaurantID, createdAt: searchObj.createdAt, updatedAt: searchObj.updatedAt, v: searchObj.v, isFavorite: 1, review: searchObj.review, rating: Int(searchObj.rating ?? 0.0), cartMenuQuantity: searchObj.cartMenuQuantity)
        
        
        
//        let topDishesObj = TopDish(isActive: searchObj.isActive, id: searchObj.id, itemName: searchObj.itemName, categoryID: searchObj.categoryID, restaurantID: searchObj.restaurantID, type: searchObj.type, price: searchObj.price, rating: Double(searchObj.rating ?? 0), waitingTime: searchObj.waitingTime, menuImage: searchObj.menuImage, createdAt: searchObj.createdAt, updatedAt: searchObj.updatedAt, v: searchObj.v, review: searchObj.review, mealTypeID: nil, topDishDescription: searchObj.listDescription, offerPrice: searchObj.offerPrice, isFavorite: 0, cartMenuQuantity: searchObj.cartMenuQuantity)
        
        let homeDetailView = HomeDetailsView.instantiateFromAppStoryboard(appStoryboard: .home)
        homeDetailView.menuItem = topDishesObj
        homeDetailView.cartId = self.searchResult[indexPath.row].cartID ?? ""
        homeDetailView.totalCartCount = self.labelTotalCartCount.text ?? ""
        self.navigationController?.pushViewController(homeDetailView, animated: true)
    }
}
extension FoodSearchView: TodaysMenuTableDelegate {
//    func updateTable() {
//        self.table_view.reloadData()
//    }
    
    func markAsFavouriteTable(_ sender: UIButton) {
        
    }
    
    func minusCartItemTable(_ sender: UIButton) {
        let menuID = self.searchResult[sender.tag].id ?? ""
        let vendorID = self.searchResult[sender.tag].vendorID ?? ""
        
        let discountType = self.searchResult[sender.tag].itemID?.discountType
        let price = self.searchResult[sender.tag].itemID?.price ?? 0
        let discountPrice = self.searchResult[sender.tag].itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        let menuQuantity: Int = self.searchResult[sender.tag].cartMenuQuantity ?? 0
        
        if menuQuantity == 1 {
            self.removeCart(cartID: self.searchResult[sender.tag].cartID ?? "", menuID: menuID, vendorID: vendorID)
        }else{
            if menuQuantity > 1 {
                self.updateCart(cartID: self.searchResult[sender.tag].cartID ?? "", menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity-1)")
            }
        }
    }
    
    func plusCartItemTable(_ sender: UIButton) {
        let menuID = self.searchResult[sender.tag].id ?? ""
        
        let discountType = self.searchResult[sender.tag].itemID?.discountType
        let price = self.searchResult[sender.tag].itemID?.price ?? 0
        let discountPrice = self.searchResult[sender.tag].itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        let menuQuantity: Int = self.searchResult[sender.tag].cartMenuQuantity ?? 0
        self.updateCart(cartID: self.searchResult[sender.tag].cartID ?? "", menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)")
    }
    
    func addToCartItemTable(_ sender: UIButton) {
        let menuID = self.searchResult[sender.tag].id ?? ""
        
        let discountType = self.searchResult[sender.tag].itemID?.discountType
        let price = self.searchResult[sender.tag].itemID?.price ?? 0
        let discountPrice = self.searchResult[sender.tag].itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        
        self.addToCart(menuID: menuID, menuAmount: menuAmount, menuQuantity: "1")
    }
}
