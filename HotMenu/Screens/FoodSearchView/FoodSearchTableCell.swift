//
//  FoodSearchTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 31/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class FoodSearchTableCell: UITableViewCell {

    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var labelMenuTitle: CustomLabel!
    @IBOutlet weak var labelActualPrice: CustomLabel!
    @IBOutlet weak var labelDiscountedPrice: CustomLabel!
    @IBOutlet weak var imageViewLine: UIImageView!
    @IBOutlet weak var buttonAddCart: CustomButton!
    @IBOutlet weak var viewCartBG: UIView!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var labelCartCount: CustomLabel!
    @IBOutlet weak var imageViewVeg: UIImageView!
    
    var delegate : TodaysMenuTableDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    @IBAction func buttonAddCartAction(_ sender: Any) {
        self.delegate?.addToCartItemTable(sender as! UIButton)
    }
    @IBAction func buttonMinusCartAction(_ sender: Any) {
        self.delegate?.minusCartItemTable(sender as! UIButton)
    }
    @IBAction func buttonPlusCartAction(_ sender: Any) {
        self.delegate?.plusCartItemTable(sender as! UIButton)
    }
}
