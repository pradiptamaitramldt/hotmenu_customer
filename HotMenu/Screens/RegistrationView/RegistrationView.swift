//
//  RegistrationView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 07/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class RegistrationView: UITableViewController {
    
    @IBOutlet weak var view_profileBG: UIView!
    @IBOutlet weak var user_image: UIImageView!
    @IBOutlet weak var textField_name: CustomTextField!
    @IBOutlet weak var textField_email: CustomTextField!
    @IBOutlet weak var textField_mobile: CustomTextField!
    @IBOutlet weak var textField_countryCode: CustomTextField!
    @IBOutlet weak var textField_password: CustomTextField!
    @IBOutlet weak var textField_confirmPassword: CustomTextField!
    @IBOutlet weak var buttonAlreadyHaveAccount: CustomButton!
    
    private var selectedCountry : Country?
    private var webService = RegistrationAPI()
    var currentTextField = UITextField()
    var loginType: String = ""
    var phone: String = ""
    var imageData = Data()
    var isImagePicked: Bool = false
    var registrationRequest = RegistrationRequest()
    private var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView.init(image: UIImage(named: "register_bg"))
        tableView.backgroundView?.contentMode = .scaleAspectFill
        tableView.contentInset = UIEdgeInsets(top: -44, left: 0, bottom: 0, right: 0)
        
        self.registrationRequest.delegate = self
        self.view_profileBG.cornerRadius = self.view_profileBG.bounds.height/2
        
        self.fetchCurrentCountry()
        
        if !UIAccessibility.isReduceTransparencyEnabled {
            view.backgroundColor = .clear
            
            let blurEffect = UIBlurEffect(style: .dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.view_profileBG.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view_profileBG.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        } else {
            view.backgroundColor = .black
        }
        
        self.setup()
    }
    
    func setup() {
        self.textField_name.withImage(direction: .Left, image: #imageLiteral(resourceName: "user_small") ,placeholderText : Strings.placeholderName)
        self.textField_name.cornerRadius = 10
        self.textField_name.fontSize =  17
        self.textField_name.textColor = .white
        self.textField_name.delegate = self
        self.textField_name.backgroundColor = UIColor.init(named: "TextFieldBGColor")
        self.textField_name.tintColor = .white
        
        self.textField_email.withImage(direction: .Left, image: #imageLiteral(resourceName: "mail") ,placeholderText : Strings.placeholderEmail)
        self.textField_email.cornerRadius = 10
        self.textField_email.fontSize =  17
        self.textField_email.textColor = .white
        self.textField_email.delegate = self
        self.textField_email.backgroundColor = UIColor.init(named: "TextFieldBGColor")
        self.textField_email.tintColor = .white
        
        self.textField_countryCode.delegate = self
        self.textField_countryCode.backgroundColor = .clear
        self.textField_countryCode.textColor = .white
        
        self.textField_mobile.attributedPlaceholder = NSAttributedString(string: Strings.placeholderMobileNumber, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.textField_mobile.fontSize =  17
        self.textField_mobile.textColor = .white
        self.textField_mobile.delegate = self
        self.textField_mobile.backgroundColor = .clear
        textField_mobile.tintColor = .white
        
        self.textField_password.withImage(direction: .Left, image: #imageLiteral(resourceName: "lock") ,placeholderText : Strings.placeholderPassword)
        self.textField_password.cornerRadius = 10
        self.textField_password.fontSize =  17
        self.textField_password.textColor = .white
        self.textField_password.delegate = self
        self.textField_password.backgroundColor = UIColor.init(named: "TextFieldBGColor")
        self.textField_password.tintColor = .white
        
        self.textField_confirmPassword.withImage(direction: .Left, image: #imageLiteral(resourceName: "lock"), placeholderText : Strings.placeholderConfirmPassword)
        self.textField_confirmPassword.cornerRadius = 10
        self.textField_confirmPassword.fontSize =  17
        self.textField_confirmPassword.textColor = .white
        self.textField_confirmPassword.delegate = self
        self.textField_confirmPassword.backgroundColor = UIColor.init(named: "TextFieldBGColor")
        self.textField_confirmPassword.tintColor = .white
        
        if self.loginType != Strings.loginTypeGeneral {
            self.textField_name.text = self.registrationRequest.full_name
            self.textField_email.text = self.registrationRequest.email_id
            self.textField_mobile.text = self.registrationRequest.phone
        }
        
        self.buttonAlreadyHaveAccount.customizeButtonFont(fullText: Strings.alreadyAccount, mainText: "Already have an account?" , creditsText: "Login")
        
        self.view.addhideKeyboardFeatureOnTouch()
    }
    
    private func openCountryPicker() {
        let countryPickerView = CountryPickerViewController.instantiateFromAppStoryboard(appStoryboard: .countryPicker)
        countryPickerView.delegate = self
        countryPickerView.modalPresentationStyle = .fullScreen
        self.present(countryPickerView, animated: true, completion: nil)
    }
    
    public func fetchCurrentCountry() {
        var countries: [Country] = []
        if let url = Bundle.main.url(forResource: "countryCodes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                countries = try decoder.decode([Country].self, from: data)
            } catch {
                Utility.log("error:\(error)")
            }
        }
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            for country in countries {
                if countryCode.lowercased() == country.code?.lowercased() {
                    self.selectedCountry = country
                    self.textField_countryCode.text = "\(country.dialCode ?? "")"
                    self.textField_countryCode.textAlignment = .left
                    self.textField_countryCode.textColor = .white
                    self.textField_countryCode.withImageForMobile(direction: .Left, image: UIImage(named: country.code!.lowercased()) ?? UIImage(named: "call")!, placeholderText: "")
                    self.registrationRequest.country_code = country.dialCode ?? ""
//                    self.registrationRequest.countryCodeID = country.code ?? ""
                    break
                }
            }
        }
    }
    
    //MARK: - Web Service
    private func registerUser() {
        self.dataStartedLoading()
        var tempImageData: Data?
        if self.isImagePicked == true {
            tempImageData = self.imageData
            self.webService.register(registrationRequest: self.registrationRequest, imageData: tempImageData) { (success, message, registrationResponse, error) in
                self.dataFinishedLoading()
                if success {
                    guard let successRegistrationResponse = registrationResponse, let data = successRegistrationResponse.responseData else {
                        return
                    }
                    Utility.log("Finally it's done")
                    if successRegistrationResponse.statuscode == 422 {
                        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
                    }else if successRegistrationResponse.statuscode == 200 {
                        UserDefaultsManager.shared.setStringValue(data.authToken ?? "" , forKey: .acccessToken)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.email ?? "", forKey: .email)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.fullName ?? "", forKey: .fullName)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.customerID ?? "", forKey: .userId)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.profileImage ?? "", forKey: .profileImage)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.phone ?? "", forKey: .phone)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCodeID ?? "", forKey: .isdCodeId)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCode ?? "", forKey: .isdCode)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.sid ?? "", forKey: .sid)
                        self.phone = data.userDetails?.phone ?? ""
                        self.customerRegisteredSuccessfully(registrationData: data)
                    }
                } else {
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
                }
            }
        }else{
            tempImageData = nil
            self.webService.registerWithoutImage(registrationRequest: self.registrationRequest, imageData: nil) { (success, message, registrationResponse, error) in
                self.dataFinishedLoading()
                if success {
                    guard let successRegistrationResponse = registrationResponse, let data = successRegistrationResponse.responseData else {
                        return
                    }
                    
                    if successRegistrationResponse.statuscode == 422 {
                        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
                    }else if successRegistrationResponse.statuscode == 200 {
                        UserDefaultsManager.shared.setStringValue(data.authToken ?? "" , forKey: .acccessToken)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.email ?? "", forKey: .email)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.fullName ?? "", forKey: .fullName)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.customerID ?? "", forKey: .userId)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.profileImage ?? "", forKey: .profileImage)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.phone ?? "", forKey: .phone)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCodeID ?? "", forKey: .isdCodeId)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.countryCode ?? "", forKey: .isdCode)
                        UserDefaultsManager.shared.setStringValue(data.userDetails?.sid ?? "", forKey: .sid)
                        self.phone = data.userDetails?.phone ?? ""
                        self.customerRegisteredSuccessfully(registrationData: data)
                    }
                                    
                    Utility.log("Finally it's done")
                } else {
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
                }
            }
        }
    }
    
    //MARK: - Navigation
    func customerRegisteredSuccessfully(registrationData: RegistrationResponseData) {
        //navigate to phone number page
        DispatchQueue.main.async {
            let otpView = OTPVerificationView.instantiateFromAppStoryboard(appStoryboard: .verify)
            otpView.registrationResponseData = registrationData
            otpView.previousView = Strings.registration
            otpView.phone = self.phone
            otpView.otp = registrationData.userDetails?.otp ?? ""
            self.navigationController?.pushViewController(otpView, animated: true)
        }
    }
    
    @IBAction func buttonUploadImageAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        self.imagePicker.delegate = self
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        self.imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func buttonRegisterAction(_ sender: Any) {
        //        let verifyView = OTPVerificationView.instantiateFromAppStoryboard(appStoryboard: .verify)
        //        self.navigationController?.pushViewController(verifyView, animated: true)
        
        self.registrationRequest.full_name = self.textField_name.text ?? ""
        self.registrationRequest.email_id = self.textField_email.text ?? ""
        self.registrationRequest.country_code = self.textField_countryCode.text ?? ""
        self.registrationRequest.phone = self.textField_mobile.text ?? ""
        self.registrationRequest.password = self.textField_password.text ?? ""
        self.registrationRequest.password_confirmation = self.textField_confirmPassword.text ?? ""
        self.registrationRequest.device_token = "1234"
        self.registrationRequest.app_type = Strings.appType
        self.registrationRequest.push_mode = "S"
        self.registrationRequest.isPicture = self.isImagePicked
        if self.loginType == Strings.loginTypeGoogle {
            self.registrationRequest.isPicture = true
            self.registrationRequest.login_type = Strings.loginTypeGoogle
        }else if self.loginType == Strings.loginTypeFB {
            self.registrationRequest.login_type = Strings.loginTypeFB
        } else{
            self.registrationRequest.social_unique_id = ""
            self.registrationRequest.login_type = Strings.loginTypeGeneral
        }
        self.registrationRequest.validateData()
    }
    @IBAction func buttonAlreadyHaveAccountAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4 || indexPath.row == 5 {
            if self.loginType == Strings.loginTypeFB || self.loginType == Strings.loginTypeGoogle {
                return 0
            }else{
                return 80
            }
        }else if indexPath.row == 0 {
            return 328
        }else if indexPath.row == 6 || indexPath.row == 8 {
            return 100
        }else{
            return 80
        }
    }
    //    override func numberOfSections(in tableView: UITableView) -> Int {
    //        // #warning Incomplete implementation, return the number of sections
    //        return 0
    //    }
    //
    //    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        // #warning Incomplete implementation, return the number of rows
    //        return 0
    //    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension RegistrationView : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.textField_countryCode {
            self.currentTextField = textField
            textField.resignFirstResponder()
            self.openCountryPicker()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension RegistrationView : CountryPickerViewControllerDelegate {
    
    func didSelectCountry(country: Country) {
        self.selectedCountry = country
        if self.currentTextField == self.textField_countryCode {
            self.textField_countryCode.text = "\(country.dialCode ?? "")"
            self.textField_countryCode.textAlignment = .left
            self.textField_countryCode.withImageForMobile(direction: .Left, image: UIImage(named: country.code!.lowercased()) ?? UIImage(named: "call")!, placeholderText: "")
            self.textField_countryCode.textColor = .white
            self.registrationRequest.country_code = country.dialCode ?? ""
//            self.registrationRequest.countryCodeID = country.code ?? ""
        }
    }
    
}
extension RegistrationView: ViewControllerProtocol {
    
    func userRegisteredSuccessfully(registrationData: RegistrationRequest) {
        //navigate to phone number page
        DispatchQueue.main.async {
            /*
             guard let presenter = self.presenter else { return }
             let registrationStep2View = RegistrationStep2View.instantiateFromAppStoryboard(appStoryboard: .registration)
             registrationStep2View.presenter = presenter
             presenter.setViewTwo(viewTwo: registrationStep2View)
             presenter.setAccessToken(accessToken: registrationData.accessToken ?? "", tokenType: registrationData.tokenType ?? "")
             self.navigationController?.pushViewController(registrationStep2View, animated: true)
             */
        }
    }
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension RegistrationView: RegistrationRequestDelegate {
    
    func invalidRegistrationData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func registrationValidationPassed() {
        self.registerUser()
    }
    
}
extension RegistrationView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info[.originalImage] as? UIImage {
            self.user_image.contentMode = .scaleAspectFill
            self.user_image.image = pickedImage
            self.imageData = pickedImage.jpeg(.low)!
            self.isImagePicked = true
        }
    }
}
