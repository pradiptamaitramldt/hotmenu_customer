//
//  RegistrationModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 12/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct RegistrationRequest {
    
    var full_name: String
    var email_id: String
    var phone: String
    var password: String
    var password_confirmation : String
    var country_code: String
    var login_type: String
    var social_unique_id: String
    var device_token: String
    var app_type: String
    var push_mode: String
    var picture: String
    var isPicture: Bool
//    var countryCodeID: String
    
    var delegate: RegistrationRequestDelegate?
    
    init() {
        self.full_name = ""
        self.email_id = ""
        self.phone = ""
        self.password = ""
        self.password_confirmation = ""
        self.country_code = ""
        self.login_type = ""
        self.social_unique_id = ""
        self.device_token = ""
        self.app_type = ""
        self.push_mode = ""
        self.picture = ""
//        self.countryCodeID = ""
        self.isPicture = false
    }
    
    init(socialLoginData: SocialLoginDataModelFromSocialPlatforms) {
        self.full_name = socialLoginData.socialFirstName ?? ""
        self.email_id = socialLoginData.socialEmail ?? ""
        self.login_type = socialLoginData.type ?? ""
        self.social_unique_id = socialLoginData.socialID ?? ""
        self.picture = socialLoginData.socialImageUrl ?? ""
        self.phone = ""
        self.country_code = ""
        self.password = ""
        self.password_confirmation = ""
        self.device_token = ""
        self.app_type = ""
        self.push_mode = ""
//        self.countryCodeID = ""
        self.isPicture = false
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.isPicture == true{
            if self.full_name != "" {
                if self.email_id.isValidEmail() {
                    if self.country_code != "" {
                        if self.phone.isValidPhoneNumber() {
                            if self.login_type == Strings.loginTypeGeneral {
                                if self.password.isValidPassword() {
                                    if password_confirmation.isValidPassword() {
                                        if password == password_confirmation {
                                            delegate.registrationValidationPassed()
                                        } else {
                                            delegate.invalidRegistrationData(message: ErrorMessages.passwordAndConfirmPasswordDidNotMatch)
                                        }
                                    }else{
                                        delegate.invalidRegistrationData(message: ErrorMessages.invalidPassword)
                                    }
                                }else{
                                    delegate.invalidRegistrationData(message: ErrorMessages.invalidPassword)
                                }
                            }else{
                                if self.social_unique_id != "" {
                                    delegate.registrationValidationPassed()
                                } else {
                                    delegate.invalidRegistrationData(message: ErrorMessages.invalidSocialId)
                                }
                            }
                        }else{
                            delegate.invalidRegistrationData(message: ErrorMessages.invalidPhoneNumber)
                        }
                    }else{
                        delegate.invalidRegistrationData(message: ErrorMessages.invalidDialCode)
                    }
                } else {
                    delegate.invalidRegistrationData(message: ErrorMessages.invalidEmail)
                }
            } else {
                delegate.invalidRegistrationData(message: ErrorMessages.invalidName)
            }
        }else{
            delegate.invalidRegistrationData(message: ErrorMessages.invalidPicture)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        if UserDefaultsManager.shared.getStringValue(forKey: .deviceToken) != "" {
            tempDictionary["deviceToken"] = UserDefaultsManager.shared.getStringValue(forKey: .deviceToken)
        } else {
            tempDictionary["deviceToken"] = "12345"
        }
        tempDictionary["appType"] = Strings.appType
        tempDictionary["fullName"] = self.full_name
        tempDictionary["email"] = self.email_id
        tempDictionary["loginType"] = self.login_type
        tempDictionary["pushMode"] = self.push_mode
        tempDictionary["phone"] = self.phone
        tempDictionary["countryCode"] = self.country_code
//        tempDictionary["countryCodeId"] = self.countryCodeID
        tempDictionary["socialId"] = self.social_unique_id
        tempDictionary["password"] = self.password
        tempDictionary["confirmPassword"] = self.password_confirmation
        if self.login_type != Strings.loginTypeGeneral {
            tempDictionary["image"] = self.picture
        }
        
        return tempDictionary
    }
}

protocol RegistrationRequestDelegate {
    
    func invalidRegistrationData(message: String)
    
    func registrationValidationPassed()
    
}

// MARK: - RegistrationResponseModel
struct RegistrationResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: RegistrationResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct RegistrationResponseData: Codable {
    let userDetails: UserDetails?
    let authToken: String?
}

// MARK: - UserDetails
struct UserDetails: Codable {
    let fullName, email, countryCode, countryCodeID: String?
    let phone, socialID, customerID: String?
    let profileImage: String?
    let userType, loginType, sid, otp: String?

    enum CodingKeys: String, CodingKey {
        case fullName, email, countryCode
        case countryCodeID = "countryCodeId"
        case phone
        case socialID = "socialId"
        case customerID = "customerId"
        case profileImage, userType, loginType, sid, otp
    }
}
