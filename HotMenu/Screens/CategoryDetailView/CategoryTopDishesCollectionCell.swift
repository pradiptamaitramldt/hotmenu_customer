//
//  CategoryTopDishesCollectionCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class CategoryTopDishesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var imageViewVeg: UIImageView!
    @IBOutlet weak var imageViewFavourite: UIImageView!
    @IBOutlet weak var labelTitle: CustomLabel!
    @IBOutlet weak var labelDescription: CustomLabel!
    @IBOutlet weak var labelActualPrice: CustomLabel!
    @IBOutlet weak var labelDiscountedPrice: CustomLabel!
    @IBOutlet weak var buttonAddCart: UIButton!
    @IBOutlet weak var viewCartBG: UIView!
    @IBOutlet weak var buttonFavourite: UIButton!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var labelCartCount: CustomLabel!
    @IBOutlet weak var labelRating: CustomLabel!
    
    var delegate : TopDishesCollectionDelegate?
    
    @IBAction func buttonAddCartAction(_ sender: Any) {
        self.delegate?.addToCartItem(sender as! UIButton)
    }
    @IBAction func buttonFavouriteAction(_ sender: Any) {
        self.delegate?.markAsFavourite(sender as! UIButton)
    }
    @IBAction func buttonMinusAction(_ sender: Any) {
        self.delegate?.minusCartItem(sender as! UIButton)
    }
    @IBAction func buttonPlusAction(_ sender: Any) {
        self.delegate?.plusCartItem(sender as! UIButton)
    }
}
