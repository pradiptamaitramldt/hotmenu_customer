//
//  CategoryDetailView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import CoreLocation

class CategoryDetailView: UIViewController {
    
    @IBOutlet weak var labelLocation: CustomLabel!
    @IBOutlet weak var belIcon: UIImageView!
    @IBOutlet weak var cartIcon: UIImageView!
    @IBOutlet weak var buttonFoodSearch: UIButton!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var labelNoRestaurant: CustomLabel!
    @IBOutlet weak var labelCartCount: CustomLabel!
    
    private var cartWebService = CartAPI()
    private var addCartRequest = AddCartRequest()
    private var updateCartRequest = UpdateCartRequest()
    private var removeCartRequest = RemoveCartRequest()
    private var webService = CategoryWiseAPI()
    private var deshboardWebService = DashboardAPI()
    private var categoryRequest = CategoryWiseRequest()
    private var favouriteRequest = MakeFavouriteRequest()
    private var unFavouriteRequest = MakeFavouriteRequest()
    var collectionView_category : UICollectionView!
    var collectionView_topDishes : UICollectionView!
    var collectionView_todaysMenu : UICollectionView!
    var locationManager: CLLocationManager = CLLocationManager()
    var lattitude = Double()
    var longitude = Double()
    var categoryArray: [CategoryDatum] = []
    var topDishesArray: [TopDish] = []
    var todaysAllMenuArray: [ToDaysMenu] = []
    var todaysMenuCategoryArray: [String] = []
    var todaysMenuArray: [TopDish] = []
    var selectedIndexPath = IndexPath(row:0,section:0)
    var categoryImageUrl: String = ""
    var categoryID: String = ""
    var cart_id: String = ""
    var totalCartCount: String = ""
    var itemCartCount: Int = 0
    var location: String = ""
    var shouldFetchCategoryWiseData: Bool = true
    var isCategorySelected: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        
        self.labelNoRestaurant.isHidden = true
        self.labelNoRestaurant.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelNoRestaurant.text = Strings.noDataFOund
        self.labelNoRestaurant.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelCartCount.isHidden = true
        self.labelCartCount.setTextFont(fontWeight: .medium, ofSize: 12)
        self.labelLocation.setTextFont(fontWeight: .regular, ofSize: 14)
        self.labelCartCount.textColor = UIColor.init(named: "WhiteColor")
        self.labelCartCount.backgroundColor = UIColor.init(named: "ButtonColor")
        if self.totalCartCount == "" || self.totalCartCount == "0" {
            self.labelCartCount.text = "0"
            self.labelCartCount.isHidden = true
        }else{
            self.labelCartCount.isHidden = false
            self.labelCartCount.text = self.totalCartCount
        }
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        
        self.labelLocation.text = self.location
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.isCategorySelected = false
        if self.shouldFetchCategoryWiseData == true {
            DispatchQueue.main.async {
                self.table_view.startShimmeringAnimation()
            }
            self.fetchCategoryWiseData()
        }else{
            self.shouldFetchCategoryWiseData = true
        }
    }
    
    func fetchCategoryWiseData() {
        
        self.categoryRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3" //UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.categoryRequest.UserType = Strings.userTypeCustomer
        self.categoryRequest.lattitude = "\(Utility.lattitude)"//"22.6626267"//"\(Utility.lattitude)"
        self.categoryRequest.longitude = "\(Utility.longitude)"//"88.40060609999999"//"\(Utility.longitude)"
        self.categoryRequest.categoryId = self.categoryID
        
        //        self.dataStartedLoading()
        self.webService.fetchCategoryWiseMenu(categoryWiseRequest: self.categoryRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            //            self.dataFinishedLoading()
            DispatchQueue.main.async {
                self.table_view.stopShimmeringAnimation()
            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.categoryImageUrl = data.categoryImageURL ?? ""
                self.categoryArray = data.categoryData ?? []
                self.topDishesArray = data.topDishes ?? []
                let todaysArray = data.toDaysMenu ?? []
                self.todaysAllMenuArray = data.toDaysMenu ?? []
                let total_cartCount = data.cartCountTotal
                self.totalCartCount = "\(total_cartCount ?? 0)"
                self.isCategorySelected = true
//                if todaysArray.count > 0 {
//                    self.todaysMenuArray = todaysArray[0].value ?? []
//                    self.todaysMenuCategoryArray.removeAll()
//                    for item in todaysArray {
//                        self.todaysMenuCategoryArray.append(item.name ?? "NA")
//                    }
//
//                    DispatchQueue.main.async {
//                        self.createMenuArray()
//                        self.collectionView_todaysMenu.reloadData()
//                        //                        self.table_view.reloadData()
//                    }
//                }
                
                DispatchQueue.main.async {
                    if total_cartCount == 0 {
                        self.labelCartCount.text = "0"
                        self.labelCartCount.isHidden = true
                    }else{
                        self.labelCartCount.isHidden = false
                        self.labelCartCount.text = self.totalCartCount
                    }
                }
                
                if todaysArray.count > 0 {
                    self.todaysMenuArray = todaysArray[0].value ?? []
                    self.todaysMenuCategoryArray.removeAll()
                    for item in todaysArray {
                        self.todaysMenuCategoryArray.append(item.name ?? "NA")
                    }
                    
                    DispatchQueue.main.async {
                        self.createMenuArray()
                        self.collectionView_todaysMenu.reloadData()
                    }
                }
                
                if self.categoryArray.count == 0 && self.topDishesArray.count == 0 && todaysArray.count == 0 {
                    DispatchQueue.main.async {
                        self.table_view.isHidden = true
                        self.labelNoRestaurant.isHidden = false
                    }
                }else{
                    
                    DispatchQueue.main.async {
                        self.table_view.reloadData()
                        self.table_view.isHidden = false
                        self.labelNoRestaurant.isHidden = true
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func makeFavourite(menuID: String) {
        self.favouriteRequest.customerId = "5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.favouriteRequest.menuId = menuID
        self.dataStartedLoading()
        self.deshboardWebService.markFavourite(favouriteRequest: self.favouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                let menuId = data.menuID
                print(menuId ?? "")
                self.fetchCategoryWiseData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func makeUnfavourite(menuID: String) {
        self.unFavouriteRequest.customerId = "5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.unFavouriteRequest.menuId = menuID
        self.dataStartedLoading()
        self.deshboardWebService.markUnfavourite(favouriteRequest: self.unFavouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchCategoryWiseData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func addToCart(menuID: String, menuAmount: String, menuQuantity: String, vendorId: String) {
        self.addCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addCartRequest.menuId = menuID
        self.addCartRequest.menuAmount = menuAmount
        self.addCartRequest.menuQuantity = menuQuantity
        self.addCartRequest.vendorId = vendorId
        self.dataStartedLoading()
        self.cartWebService.addToCart(addCartRequest: self.addCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchCategoryWiseData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func updateCart(cartID: String, menuID: String, menuAmount: String, menuQuantity: String, vendorId: String) {
        self.updateCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.updateCartRequest.menuId = menuID
        self.updateCartRequest.menuAmount = menuAmount
        self.updateCartRequest.menuQuantity = menuQuantity
        self.updateCartRequest.cartId = cartID
        self.updateCartRequest.vendorId = vendorId
        self.dataStartedLoading()
        self.cartWebService.updateCart(updateCartRequest: self.updateCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchCategoryWiseData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func removeCart(cartID: String, menuID: String, vendorId: String) {
        self.removeCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.removeCartRequest.menuId = menuID
        self.removeCartRequest.cartId = cartID
        self.removeCartRequest.vendorId = vendorId
        self.dataStartedLoading()
        self.cartWebService.removeCart(removeCartRequest: self.removeCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchCategoryWiseData()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func createMenuArray() {
        self.todaysMenuArray = self.todaysAllMenuArray[selectedIndexPath.item].value ?? []
        DispatchQueue.main.async {
            self.table_view.reloadData()
        }
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonLocationAction(_ sender: Any) {
        let searchLocationView = SearchLocationView.instantiateFromAppStoryboard(appStoryboard: .search)
        searchLocationView.block = { (addressID, address, landmark, houseNo, latittude, longitude, typeId, type, comingFrom) in
            self.labelLocation.text = address
            Utility.lattitude = Double(latittude)!
            Utility.longitude = Double(longitude)!
            self.shouldFetchCategoryWiseData = false
            self.fetchCategoryWiseData()
        }
        searchLocationView.selectedPlaceDelegate = self
        searchLocationView.locationType = "Pickup"
        searchLocationView.comingFrom = "Category"
        self.navigationController?.pushViewController(searchLocationView, animated: true)
    }
    @IBAction func buttonBellAction(_ sender: Any) {
    }
    @IBAction func buttonCartAction(_ sender: Any) {
        if self.labelCartCount.text == "0" || self.labelCartCount.text == "" {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "Opps! Cart is empty\nPlease add something to the cart first.")
        }else{
            let cartView = CartView.instantiateFromAppStoryboard(appStoryboard: .cart)
            self.navigationController?.pushViewController(cartView, animated: true)
        }
    }
    @IBAction func buttonSearchAction(_ sender: Any) {
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension CategoryDetailView : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.lattitude = locValue.latitude
        self.longitude = locValue.longitude
        Utility.lattitude = locValue.latitude
        Utility.longitude = locValue.longitude
    }
}
extension CategoryDetailView : SelecetdPlacesDelegate {
    
    func locationSelected(currentLocation: Location, ofType: String) {
        print("location : ",currentLocation)
        self.labelLocation.text = currentLocation.locationName
        //        addLocationView.addressLabel.text = currentLocation.locationName ?? ""
    }
    func selectedLocationType(ofType: String,selectedIndex : Int) {
        
        //        self.selectedIndex = selectedIndex
        //        addLocationView.collView.reloadData()
        
    }
}
extension CategoryDetailView : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else{
            if self.todaysMenuArray.count>0 {
                return self.todaysMenuArray.count
            }else{
                if self.isCategorySelected == false {
                    return 5
                }else{
                    return 1
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTopDishesTableCell", for: indexPath) as! CategoryTopDishesTableCell
                
                self.collectionView_topDishes = cell.collection_viewTopDishes
                cell.collection_viewTopDishes.delegate = self
                cell.collection_viewTopDishes.dataSource = self
                cell.labelHeader.text = "Top Dishes"
                cell.labelHeader.setTextFont(fontWeight: .medium , ofSize: 20)
                cell.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
                cell.collection_viewTopDishes.reloadData()
                
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTodaysMenuTableCell", for: indexPath) as! CategoryTodaysMenuTableCell
                
                self.collectionView_todaysMenu = cell.collection_viewTodaysMenu
                cell.collection_viewTodaysMenu.delegate = self
                cell.collection_viewTodaysMenu.dataSource = self
                cell.labelHeader.text = "Today's Menu"
                cell.collection_viewTodaysMenu.reloadData()
                
                return cell
            }
        }else{
            if self.todaysMenuArray.count>0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTodaysMenuListTableCell", for: indexPath) as! CategoryTodaysMenuListTableCell
                
                let itemImage = self.todaysMenuArray[indexPath.row].itemID?.menuImage ?? ""
                if itemImage != "" {
                    cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
                }
                cell.imageViewMenuItem.contentMode = .scaleAspectFill
                cell.imageViewMenuItem.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
                cell.delegate = self
                cell.labelTitle.text = self.todaysMenuArray[indexPath.row].itemID?.itemName ?? "NA"
                cell.labelDescription.text = self.todaysMenuArray[indexPath.row].itemID?.description == "" ? "NA" : self.todaysMenuArray[indexPath.row].itemID?.description
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "₹ \(self.todaysMenuArray[indexPath.row].itemID?.price ?? 0)")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                
                cell.labelActualPrice.attributedText = attributeString
                
                //                cell.labelActualPrice.text = "₹ \(self.todaysMenuArray[indexPath.row].itemID?.price ?? 0)"
                
                let discountType = self.todaysMenuArray[indexPath.row].itemID?.discountType
                let price = self.todaysMenuArray[indexPath.row].itemID?.price ?? 0
                let discountPrice = self.todaysMenuArray[indexPath.row].itemID?.discountAmount ?? 0
                var offerPrice : Float = 0.0
                if discountType == "FLAT" {
                    offerPrice = Float(price-discountPrice)
                }else if discountType == "NONE" {
                    offerPrice = Float(price)
                }else{
                    offerPrice = Float(price-((price*discountPrice)/100))
                }
                
                cell.labelDiscountedPrice.text = "₹ \(offerPrice)"
                
                cell.labelTitle.setTextFont(fontWeight: .medium, ofSize: 16)
                cell.labelTitle.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelDescription.setTextFont(fontWeight: .light, ofSize: 15)
                cell.labelDescription.textColor = UIColor.init(named: "HomeTextLightColor")
                
                cell.labelActualPrice.setTextFont(fontWeight: .light, ofSize: 11)
                cell.labelActualPrice.textColor = UIColor.init(named: "HomeTextLightColor")
                
                cell.labelDiscountedPrice.setTextFont(fontWeight: .regular, ofSize: 12)
                cell.labelDiscountedPrice.textColor = UIColor.init(named: "HomeTextColor")
                
                let isVegItem = self.todaysMenuArray[indexPath.row].itemID?.type
                if isVegItem == "NON VEG" {
                    cell.imageViewVeg.image = UIImage(named: "nonveg")
                }else{
                    cell.imageViewVeg.image = UIImage(named: "veg")
                }
                let isFavItem = self.todaysMenuArray[indexPath.row].isFavorite
                if isFavItem == 1 {
                    cell.imageViewFavourite.image = UIImage(named: "heart_red")
                }else{
                    cell.imageViewFavourite.image = UIImage(named: "heart_white")
                }
                
                let cartQuantity  = self.todaysMenuArray[indexPath.row].cartMenuQuantity
                
                if cartQuantity == 0 {
                    cell.viewIncreaseCart.isHidden = true
                    cell.buttonAddCart.isHidden = false
                }else{
                    cell.viewIncreaseCart.isHidden = false
                    cell.buttonAddCart.isHidden = true
                    cell.labelCartCount.text = "\(cartQuantity ?? 0)"
                    cell.labelCartCount.textColor = UIColor.init(named: "ButtonColor")
                }
                
                cell.labelRating.text = "3"
                cell.labelRating.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelRating.textColor = UIColor.init(named: "HomeTextColor")
                cell.buttonFavourite.tag = indexPath.row
                cell.buttonMinus.tag = indexPath.row
                cell.buttonPlus.tag = indexPath.row
                cell.buttonAddCart.tag = indexPath.row
                
                return cell
            }else{
                if self.isCategorySelected == true {
                    self.isCategorySelected = false
                    let cell = tableView.dequeueReusableCell(withIdentifier: "categoryNoItemTableCell", for: indexPath) as! CategoryNoItemTableCell
                    cell.labelNoItemFound.setTextFont(fontWeight: .bold, ofSize: 20)
                    cell.labelNoItemFound.text = Strings.noDataFOund
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTodaysMenuListTableCell", for: indexPath) as! CategoryTodaysMenuListTableCell
                    return cell
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if self.topDishesArray.count > 0 {
                    return 320
                }else{
                    return 0
                }
            } else {
                if todaysMenuCategoryArray.count > 0 {
                    return 120
                }else{
                    return 0
                }
            }
        } else {
            if self.todaysMenuArray.count>0{
                return 320
            }else{
                return 320
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let homeDetailView = HomeDetailsView.instantiateFromAppStoryboard(appStoryboard: .home)
            if self.todaysMenuArray.count > 0 {
                homeDetailView.menuItem = self.todaysMenuArray[indexPath.item]
                homeDetailView.imageURL = self.categoryImageUrl
                homeDetailView.cartId = self.cart_id
                homeDetailView.totalCartCount = self.labelCartCount.text ?? ""
                self.navigationController?.pushViewController(homeDetailView, animated: true)
            }
        }
    }
}
extension CategoryDetailView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension CategoryDetailView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView_category {
            return self.categoryArray.count
        }else if collectionView == self.collectionView_topDishes {
            return self.topDishesArray.count
        }else{
            return todaysMenuCategoryArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.collectionView_topDishes){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryTopDishesCollectionCell", for: indexPath) as! CategoryTopDishesCollectionCell
            
            let itemImage = self.topDishesArray[indexPath.item].itemID?.menuImage ?? ""
            if itemImage != "" {
                cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
            }
            cell.imageViewMenuItem.contentMode = .scaleAspectFill
            cell.imageViewMenuItem.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
            cell.delegate = self
            cell.labelTitle.text = self.topDishesArray[indexPath.item].itemID?.itemName
            cell.labelDescription.text = self.topDishesArray[indexPath.row].itemID?.description == "" ? "NA" : self.topDishesArray[indexPath.row].itemID?.description
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "₹ \(self.topDishesArray[indexPath.item].itemID?.price ?? 0)")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            
            cell.labelActualPrice.attributedText = attributeString
            
            //            cell.labelActualPrice.text = "₹ \(self.topDishesArray[indexPath.item].itemID?.price ?? 0)"
            
            let discountType = self.topDishesArray[indexPath.row].itemID?.discountType
            let price = self.topDishesArray[indexPath.row].itemID?.price ?? 0
            let discountPrice = self.topDishesArray[indexPath.row].itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            cell.labelDiscountedPrice.text = "₹ \(offerPrice)"
            cell.labelTitle.setTextFont(fontWeight: .medium, ofSize: 16)
            cell.labelTitle.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelDescription.setTextFont(fontWeight: .light, ofSize: 15)
            cell.labelDescription.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelActualPrice.setTextFont(fontWeight: .light, ofSize: 11)
            cell.labelActualPrice.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelDiscountedPrice.setTextFont(fontWeight: .regular, ofSize: 12)
            cell.labelDiscountedPrice.textColor = UIColor.init(named: "HomeTextColor")
            
            let isVegItem = self.topDishesArray[indexPath.row].itemID?.type
            if isVegItem == "NON VEG" {
                cell.imageViewVeg.image = UIImage(named: "nonveg")
            }else{
                cell.imageViewVeg.image = UIImage(named: "veg")
            }
            let isFavItem = self.topDishesArray[indexPath.row].isFavorite
            if isFavItem == 1 {
                cell.imageViewFavourite.image = UIImage(named: "heart_red")
            }else{
                cell.imageViewFavourite.image = UIImage(named: "heart_white")
            }
            
            let cartQuantity = self.topDishesArray[indexPath.row].cartMenuQuantity
            if cartQuantity == 0 {
                cell.buttonAddCart.isHidden = false
                cell.viewCartBG.isHidden = true
            }else{
                cell.buttonAddCart.isHidden = true
                cell.viewCartBG.isHidden = false
                cell.labelCartCount.text = "\(cartQuantity ?? 0)"
                cell.labelCartCount.textColor = UIColor.init(named: "ButtonColor")
            }
            cell.labelRating.text = "3.5"
            cell.labelRating.setTextFont(fontWeight: .regular , ofSize: 14)
            cell.labelRating.textColor = UIColor.init(named: "HomeTextColor")
            cell.buttonFavourite.tag = indexPath.item
            cell.buttonAddCart.tag = indexPath.item
            cell.buttonMinus.tag = indexPath.item
            cell.buttonPlus.tag = indexPath.item
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryTodaysMenuCollectionCell", for: indexPath) as! CategoryTodaysMenuCollectionCell
            
            cell.labelMenuTypeTitle.text = self.todaysMenuCategoryArray[indexPath.item]
            if indexPath == self.selectedIndexPath {
                cell.viewMenuSelectionBG.backgroundColor = UIColor.init(named: "YellowColor")
                cell.labelMenuTypeTitle.textColor = .white
            }else{
                cell.viewMenuSelectionBG.backgroundColor = .clear
                cell.labelMenuTypeTitle.textColor = UIColor.init(named: "HomeTextLightColor")
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionView_todaysMenu {
            let label = UILabel(frame: CGRect.zero)
            label.text = self.todaysMenuCategoryArray[indexPath.item]
            if selectedIndexPath == indexPath {
                label.font = UIFont(name:"CeraPro-Light", size: 18.0)
            } else {
                label.font = UIFont(name:"CeraPro-Light", size: 15.0)
            }
            
            label.sizeToFit()
            
            let view_bg = UIView()
            view_bg.frame = CGRect(x: 5, y: 5, width: label.frame.size.width + 30, height: 65)
            
            return CGSize(width: view_bg.frame.width + 10, height: view_bg.frame.height)
            
        }else if collectionView == self.collectionView_category {
            return CGSize(width: 140, height: 140)
        }else{
            return CGSize(width: UIScreen.main.bounds.size.width, height: 275)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left:10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndexPath = IndexPath(row: indexPath.item, section: 0)
        if collectionView == self.collectionView_todaysMenu {
            self.isCategorySelected = true
            self.createMenuArray()
            self.collectionView_todaysMenu.reloadData()
        }else if collectionView == self.collectionView_topDishes {
            let homeDetailView = HomeDetailsView.instantiateFromAppStoryboard(appStoryboard: .home)
            homeDetailView.menuItem = self.topDishesArray[indexPath.item]
            homeDetailView.imageURL = self.categoryImageUrl
            homeDetailView.cartId = self.cart_id
            homeDetailView.totalCartCount = self.labelCartCount.text ?? ""
            self.navigationController?.pushViewController(homeDetailView, animated: true)
        }
    }
}
extension CategoryDetailView : TopDishesCollectionDelegate {
    func markAsFavourite(_ sender: UIButton) {
        
        let isFavourite = self.topDishesArray[sender.tag].isFavorite
        let menuID = self.topDishesArray[sender.tag].id ?? ""
        if isFavourite == 1 {
            if menuID != "" {
                self.makeUnfavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }else{
            if menuID != "" {
                self.makeFavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }
    }
    
    func minusCartItem(_ sender: UIButton) {
        let menuID = self.topDishesArray[sender.tag].id ?? ""
        
        let discountType = self.topDishesArray[sender.tag].itemID?.discountType
        let price = self.topDishesArray[sender.tag].itemID?.price ?? 0
        let discountPrice = self.topDishesArray[sender.tag].itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        let menuQuantity: Int = self.topDishesArray[sender.tag].cartMenuQuantity ?? 0
        let vendorID: String = self.topDishesArray[sender.tag].restaurantID ?? ""
        
        if menuQuantity == 1 {
            self.removeCart(cartID: self.cart_id, menuID: menuID, vendorId: vendorID)
        }else{
            if menuQuantity > 1 {
                self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity-1)", vendorId: vendorID)
            }
        }
    }
    
    func plusCartItem(_ sender: UIButton) {
        if self.topDishesArray[sender.tag].itemID?.isActive == true {
            let menuID = self.topDishesArray[sender.tag].id ?? ""
                   
                   let discountType = self.topDishesArray[sender.tag].itemID?.discountType
                   let price = self.topDishesArray[sender.tag].itemID?.price ?? 0
                   let discountPrice = self.topDishesArray[sender.tag].itemID?.discountAmount ?? 0
                   var offerPrice : Float = 0.0
                   if discountType == "FLAT" {
                       offerPrice = Float(price-discountPrice)
                   }else if discountType == "NONE" {
                       offerPrice = Float(price)
                   }else{
                       offerPrice = Float(price-((price*discountPrice)/100))
                   }
                   
                   let menuAmount = "\(offerPrice)"
                   let menuQuantity: Int = self.topDishesArray[sender.tag].cartMenuQuantity ?? 0
                   let vendorID: String = self.topDishesArray[sender.tag].restaurantID ?? ""
                   
                   self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)", vendorId: vendorID)
        }
        else {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "This item is not available now")
        }
    }
    
    func addToCartItem(_ sender: UIButton) {
        
         if self.topDishesArray[sender.tag].itemID?.isActive == true {
            let menuID = self.topDishesArray[sender.tag].id ?? ""
                   
                   let discountType = self.topDishesArray[sender.tag].itemID?.discountType
                   let price = self.topDishesArray[sender.tag].itemID?.price ?? 0
                   let discountPrice = self.topDishesArray[sender.tag].itemID?.discountAmount ?? 0
                   var offerPrice : Float = 0.0
                   if discountType == "FLAT" {
                       offerPrice = Float(price-discountPrice)
                   }else if discountType == "NONE" {
                       offerPrice = Float(price)
                   }else{
                       offerPrice = Float(price-((price*discountPrice)/100))
                   }
                   
                   let menuAmount = "\(offerPrice)"
                   
                   let vendorID: String = self.topDishesArray[sender.tag].restaurantID ?? ""
                   
                   self.addToCart(menuID: menuID, menuAmount: menuAmount, menuQuantity: "1", vendorId: vendorID)
        }
         else {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "This item is not available now")
        }
       
    }
    
    //    func getCurrentCellIndexPath(_ sender: UIButton) -> IndexPath? {
    //        let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
    //        if let indexPath: IndexPath = tableView.indexPathForRow(at: buttonPosition) {
    //            return indexPath
    //        }
    //        return nil
    //    }
}
extension CategoryDetailView : TodaysMenuTableDelegate {
//    func updateTable() {
//        
//    }
    
    func markAsFavouriteTable(_ sender: UIButton) {
        let isFavourite = self.todaysMenuArray[sender.tag].isFavorite
        let menuID = self.todaysMenuArray[sender.tag].id ?? ""
        if isFavourite == 1 {
            if menuID != "" {
                self.makeUnfavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }else{
            if menuID != "" {
                self.makeFavourite(menuID: menuID)
            }else{
                self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
            }
        }
    }
    
    func minusCartItemTable(_ sender: UIButton) {
        
            let menuID = self.todaysMenuArray[sender.tag].id ?? ""
            
            let discountType = self.todaysMenuArray[sender.tag].itemID?.discountType
            let price = self.todaysMenuArray[sender.tag].itemID?.price ?? 0
            let discountPrice = self.todaysMenuArray[sender.tag].itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = offerPrice
            let menuQuantity: Int = self.todaysMenuArray[sender.tag].cartMenuQuantity ?? 0
            let vendorID: String = self.todaysMenuArray[sender.tag].restaurantID ?? ""
            
            if menuQuantity == 1 {
                self.removeCart(cartID: self.cart_id, menuID: menuID, vendorId: vendorID)
            }else{
                if menuQuantity > 1 {
                    self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: "\(menuAmount )", menuQuantity: "\(menuQuantity-1)", vendorId: vendorID)
                }
            }
        }
        
    func plusCartItemTable(_ sender: UIButton) {
        if self.todaysMenuArray[sender.tag].itemID?.isActive == true {
            let menuID = self.todaysMenuArray[sender.tag].id ?? ""
                   
                   let discountType = self.todaysMenuArray[sender.tag].itemID?.discountType
                   let price = self.todaysMenuArray[sender.tag].itemID?.price ?? 0
                   let discountPrice = self.todaysMenuArray[sender.tag].itemID?.discountAmount ?? 0
                   var offerPrice : Float = 0.0
                   if discountType == "FLAT" {
                       offerPrice = Float(price-discountPrice)
                   }else if discountType == "NONE" {
                       offerPrice = Float(price)
                   }else{
                       offerPrice = Float(price-((price*discountPrice)/100))
                   }
                   let vendorID: String = self.todaysMenuArray[sender.tag].restaurantID ?? ""
                   
                   let menuAmount = "\(offerPrice)"
                   let menuQuantity: Int = self.todaysMenuArray[sender.tag].cartMenuQuantity ?? 0
                   self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)", vendorId: vendorID)
        }
        else {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "This item is not available now")
        }
       
    }
    
    func addToCartItemTable(_ sender: UIButton) {
        if self.todaysMenuArray[sender.tag].itemID?.isActive == true {
            let menuID = self.todaysMenuArray[sender.tag].id ?? ""
            
            let discountType = self.todaysMenuArray[sender.tag].itemID?.discountType
            let price = self.todaysMenuArray[sender.tag].itemID?.price ?? 0
            let discountPrice = self.todaysMenuArray[sender.tag].itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice)"
            let vendorID: String = self.todaysMenuArray[sender.tag].restaurantID ?? ""
            self.addToCart(menuID: menuID, menuAmount: menuAmount, menuQuantity: "1", vendorId: vendorID)
        }
        else {
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "This item is not available now")
        }
    }
}
