//
//  CategoryDetailModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct CategoryWiseRequest {
    
    var userID: String
    var UserType: String
    var lattitude: String
    var longitude: String
    var categoryId: String
        
    init() {
        self.userID = ""
        self.UserType = ""
        self.lattitude = ""
        self.longitude = ""
        self.categoryId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        tempDictionary["userType"] = Strings.userTypeCustomer
        tempDictionary["latitude"] = self.lattitude
        tempDictionary["longitude"] = self.longitude
        tempDictionary["categoryId"] = self.categoryId
        
        return tempDictionary
    }
}

// MARK: - CategoryWiseResponseModel
struct CategoryWiseResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: CategoryWiseResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct CategoryWiseResponseData: Codable {
    let toDaysMenu: [ToDaysMenu]?
    let topDishes: [TopDish]?
    let categoryData: [CategoryDatum]?
    let categoryImageURL: String?
    let cartCountTotal: Int?
    let cartID, vendorID: String

    enum CodingKeys: String, CodingKey {
        case toDaysMenu, topDishes
        case categoryData = "category_data"
        case categoryImageURL = "category_imageUrl"
        case cartCountTotal
        case cartID = "cartId"
        case vendorID = "vendorId"
    }
}
