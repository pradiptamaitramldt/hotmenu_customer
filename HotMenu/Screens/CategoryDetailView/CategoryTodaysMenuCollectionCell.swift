//
//  CategoryTodaysMenuCollectionCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class CategoryTodaysMenuCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewMenuSelectionBG: UIView!
    @IBOutlet weak var labelMenuTypeTitle: CustomLabel!
}
