//
//  CategoryNoItemTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class CategoryNoItemTableCell: UITableViewCell {

    @IBOutlet weak var labelNoItemFound: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
