//
//  ResetPasswordView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 17/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class ResetPasswordView: UITableViewController {

    @IBOutlet weak var textField_newPassword: CustomTextField!
    @IBOutlet weak var textField_confirmPassword: CustomTextField!
    
    private var webService = ForgotPasswordAPI()
    private var resetPasswordRequest = ResetPasswordRequest()
    var email: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.backgroundView = UIImageView.init(image: UIImage(named: "joseph-gonzalez-176749-unsplash"))
        tableView.backgroundView?.contentMode = .scaleAspectFill
        tableView.contentInset = UIEdgeInsets(top: -44, left: 0, bottom: 0, right: 0)
        
        self.resetPasswordRequest.delegate = self
        
        self.setup()
    }
    
    func setup() {
        
        self.textField_newPassword.withImage(direction: .Left, image: #imageLiteral(resourceName: "mail") ,placeholderText : Strings.placeholderPasswrod)
        self.textField_newPassword.cornerRadius = 10
        self.textField_newPassword.fontSize =  17
        self.textField_newPassword.delegate = self
        self.textField_newPassword.textColor = .white
        self.textField_newPassword.tintColor = .white
        self.textField_newPassword.backgroundColor = UIColor.init(named: "TextFieldBGColor")
        
        self.textField_confirmPassword.withImage(direction: .Left, image: #imageLiteral(resourceName: "mail") ,placeholderText : Strings.placeholderConfirmPassword)
        self.textField_confirmPassword.cornerRadius = 10
        self.textField_confirmPassword.fontSize =  17
        self.textField_confirmPassword.textColor = .white
        self.textField_confirmPassword.tintColor = .white
        self.textField_confirmPassword.delegate = self
        self.textField_confirmPassword.backgroundColor = UIColor.init(named: "TextFieldBGColor")
        
        self.view.addhideKeyboardFeatureOnTouch()
    }
    
    func resetPassword() {
        self.dataStartedLoading()
        self.webService.resetPassword(resetPasswordRequest: self.resetPasswordRequest) { (success, message, loginResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successLoginResponse = loginResponse, let data = successLoginResponse.responseData else {
                    return
                }
                
                let message = successLoginResponse.message
                DispatchQueue.main.async {
                    let successAlert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
                    
                    successAlert.addAction(UIAlertAction(title: Strings.ok, style: .default) { (action:UIAlertAction!) in
                        
                    })
                    
                    self.present(successAlert, animated: true)
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }

    @IBAction func buttonSendAction(_ sender: Any) {
        self.resetPasswordRequest.email = self.email
        self.resetPasswordRequest.password = self.textField_newPassword.text ?? ""
        self.resetPasswordRequest.confirmPassword = self.textField_confirmPassword.text ?? ""
        self.resetPasswordRequest.userType = Strings.userTypeCustomer
        
        self.resetPasswordRequest.validateData()
    }
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ResetPasswordView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension ResetPasswordView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension ResetPasswordView: ResetPasswordRequestDelegate {
    func invalidPasswordData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func passwordValidationPassed() {
        self.resetPassword()
    }
}
