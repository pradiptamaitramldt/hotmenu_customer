//
//  ResetPasswordModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 19/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct ResetPasswordRequest {
    
    var email: String
    var password: String
    var confirmPassword: String
    var userType: String
    
    var delegate: ResetPasswordRequestDelegate?
    
    init() {
        self.email = ""
        self.password = ""
        self.confirmPassword = ""
        self.userType = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.password.isValidPassword() {
            if self.confirmPassword.isValidPassword() {
                if self.password == self.confirmPassword {
                    delegate.passwordValidationPassed()
                }else{
                    delegate.invalidPasswordData(message: ErrorMessages.passwordAndConfirmPasswordDidNotMatch)
                }
            }else{
                delegate.invalidPasswordData(message: ErrorMessages.invalidPassword)
            }
        }else{
            delegate.invalidPasswordData(message: ErrorMessages.invalidPassword)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["email"] = self.email
        tempDictionary["password"] = self.password
        tempDictionary["confirmPassword"] = self.confirmPassword
        tempDictionary["userType"] = self.userType
        
        return tempDictionary
    }
}

protocol ResetPasswordRequestDelegate {
    
    func invalidPasswordData(message: String)
    
    func passwordValidationPassed()
}

// MARK: - ResetPasswordResponseModel
struct ResetPasswordResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ResetPasswordResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct ResetPasswordResponseData: Codable {
}
