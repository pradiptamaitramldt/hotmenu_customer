//
//  Protocols.swift
//  Driver
//
//  Created by Pallab on 04/06/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

protocol ViewControllerProtocol: class {
    //common protocol for all views. these methods must be implemented
    func dataStartedLoading()
    func dataFinishedLoading()
    func showErrorAlertWithTitle(title: String, message: String)
    func showSuccessAlertWithTitle(title: String, message: String)
    func userUnauthorized()
    func userForbidden()
}
