//
//  CartSubmitTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

protocol CartSubmitDelegate {
    func submitCart(_ sender : UIButton)
}

class CartSubmitTableCell: UITableViewCell {

    @IBOutlet weak var buttonSubmit: UIButton!
    
    var delegate : CartSubmitDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonSubmitAction(_ sender: Any) {
        self.delegate?.submitCart(sender as! UIButton)
    }
    
}
