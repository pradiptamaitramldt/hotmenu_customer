//
//  PaymentDoneView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 23/10/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class PaymentDoneView: UIViewController {

    @IBOutlet weak var labelInfo1: CustomLabel!
    @IBOutlet weak var labelInfo2: CustomLabel!
    @IBOutlet weak var buttonTrackOrder: CustomButton!
    @IBOutlet weak var buttonOrderSomethingElse: CustomButton!
    
    var orderID: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setup()
    }
    
    func setup() {
        self.labelInfo1.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelInfo1.text = Strings.paymentDone
        self.labelInfo1.textColor = UIColor.init(named: "ButtonColor")
        
        self.labelInfo2.setTextFont(fontWeight: .regular , ofSize: 18)
        self.labelInfo2.text = Strings.trackOrder
        self.labelInfo2.textColor = UIColor.init(named: "HomeTextColor")
        
        self.buttonTrackOrder.setNormalTitle(Strings.trackButton)
        self.buttonTrackOrder.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonTrackOrder.cornerRadius = 10
        self.buttonTrackOrder.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonTrackOrder.setTitleFont(fontWeight: .medium, ofSize: 17.0)
        
        self.buttonOrderSomethingElse.setNormalTitle(Strings.orderSomethingElseButton)
        self.buttonOrderSomethingElse.setTitleColor(UIColor.init(named: "HomeTextColor"), for: .normal)
        self.buttonOrderSomethingElse.setTitleFont(fontWeight: .medium, ofSize: 17.0)
    }
    
    @IBAction func buttonTrackAction(_ sender: Any) {
        let trackOrderView = TrackOrderController.instantiateFromAppStoryboard(appStoryboard: .myOrder)
        trackOrderView.orderId = self.orderID
        self.navigationController?.pushViewController(trackOrderView, animated: true)
    }
    @IBAction func buttonOrderMoreAction(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeView.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
}
