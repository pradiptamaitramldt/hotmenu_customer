//
//  CartPromoCodeTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

protocol CartPromoDelegate {
    func applyPromoCode(_ sender : UIButton)
}

class CartPromoCodeTableCell: UITableViewCell {

    @IBOutlet weak var textFieldPromoCode: CustomTextField!
    @IBOutlet weak var buttonApply: CustomButton!
    
    var delegate : CartPromoDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonApplyPromoAction(_ sender: Any) {
        self.delegate?.applyPromoCode(sender as! UIButton)
    }
    
}
