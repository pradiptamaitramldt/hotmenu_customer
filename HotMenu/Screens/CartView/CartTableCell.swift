//
//  CartTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

protocol CartDelegate {
    func minusCart(_ sender : UIButton)
    func plusCart(_ sender : UIButton)
    func removeCart(_ sender : UIButton)
}

class CartTableCell: UITableViewCell {

    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var imageViewNonVeg: UIImageView!
    @IBOutlet weak var labelMenuTitle: CustomLabel!
    @IBOutlet weak var labelActualPrice: CustomLabel!
    @IBOutlet weak var imageLine: UIImageView!
    @IBOutlet weak var labelDiscountedPrice: CustomLabel!
    @IBOutlet weak var labelItemQuantity: CustomLabel!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var buttonRemove: UIButton!
    
    var delegate : CartDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonMinusAction(_ sender: Any) {
        self.delegate?.minusCart(sender as! UIButton)
    }
    @IBAction func buttonPlusAction(_ sender: Any) {
        self.delegate?.plusCart(sender as! UIButton)
    }
    @IBAction func buttonRemoveCartAction(_ sender: Any) {
        self.delegate?.removeCart(sender as! UIButton)
    }
    
}
