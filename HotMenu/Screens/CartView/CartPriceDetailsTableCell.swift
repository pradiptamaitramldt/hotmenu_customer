//
//  CartPriceDetailsTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class CartPriceDetailsTableCell: UITableViewCell {

    @IBOutlet weak var labelCartTotalTitle: CustomLabel!
    @IBOutlet weak var labelCartTaxTitle: CustomLabel!
    @IBOutlet weak var labelDeliveryTitle: CustomLabel!
    @IBOutlet weak var labelCartTotalValue: CustomLabel!
    @IBOutlet weak var labelTaxValue: CustomLabel!
    @IBOutlet weak var labelDeliveryValue: CustomLabel!
    @IBOutlet weak var labelDash: CustomLabel!
    @IBOutlet weak var labelSubtotalTitle: CustomLabel!
    @IBOutlet weak var labelSubtotalValue: CustomLabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
