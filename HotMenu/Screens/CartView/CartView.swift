//
//  CartView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import Razorpay
import AMShimmer

class CartView: UIViewController {
    
    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var labelAddressTitle: CustomLabel!
    @IBOutlet weak var buttonChangeLocation: CustomButton!
    @IBOutlet weak var labelAddress: CustomLabel!
    @IBOutlet weak var labelTime: CustomLabel!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var labelNoData: CustomLabel!
    @IBOutlet weak var viewLocationBG: UIView!
    @IBOutlet weak var viewAddLocationBG: UIView!
    @IBOutlet weak var imageViewHomeIcon: UIImageView!
    @IBOutlet weak var labelAddDeliveryTitle: CustomLabel!
    @IBOutlet weak var buttonAddDeliveryTitle: CustomButton!
    
    private var webService = CartAPI()
    private var myOrderWebService = MyOrderAPI()
    private var viewCartRequest = ViewCartRequest()
    private var addCartRequest = AddCartRequest()
    private var updateCartRequest = UpdateCartRequest()
    private var removeCartRequest = RemoveCartRequest()
    private var addressWebService = LocationAPI()
    private var addressListRequest = AddressListRequest()
    private var orderSubmitRequest = OrderSubmitRequest()
    private var addAddressRequest = AddAddressRequest()
    var razorpayObj : RazorpayCheckout? = nil
    
    let razorpayKey = "rzp_test_J0jnQejl27mPNe"
    
    var cartMenuArray: [CartMenu] = []
    var addressList: [AddressListResponseData] = []
    var cartTotal: String = ""
    var cartTotalInt: Int = 0
    var cart_id: String = ""
    var deliveryAddressId: String = ""
    var isPaymentDone: Bool = false
    var waitingTime = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelNoData.isHidden = true
        
        self.labelNoData.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelNoData.text = Strings.noCartFound
        self.labelNoData.textColor = UIColor.init(named: "HomeTextColor")
        
        self.isPaymentDone = false
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        
        DispatchQueue.main.async {
            self.table_view.startShimmeringAnimation()
        }
        
        self.setup()
        self.fetchCartList()
        self.fetchAddressList()
    }
    
    func setup() {
        
        self.labelAddDeliveryTitle.setTextFont(fontWeight: .medium, ofSize: 14)
        self.labelAddDeliveryTitle.text = "Delivery Address"
        self.labelAddDeliveryTitle.textColor = UIColor.init(named: "HomeTextColor")
        
        self.buttonAddDeliveryTitle.setNormalTitle(Strings.addLocationButtonTitle)
        self.buttonAddDeliveryTitle.backgroundColor = .white
        self.buttonAddDeliveryTitle.setTitleColor(UIColor.init(named: "ButtonColor"), for: .normal)
        self.buttonAddDeliveryTitle.setTitleFont(fontWeight: .medium, ofSize: 14.0)
        
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.cartTitle
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelAddressTitle.setTextFont(fontWeight: .medium , ofSize: 14)
        self.labelAddressTitle.text = "DELIVER TO HOME"
        self.labelAddressTitle.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelAddress.setTextFont(fontWeight: .medium , ofSize: 14)
        self.labelAddress.text = "Saltlake Sector 5"
        self.labelAddress.textColor = UIColor.init(named: "HomeTextLightColor")
        
        self.labelTime.setTextFont(fontWeight: .regular , ofSize: 14)
        self.labelTime.text = ""
        self.labelTime.textColor = UIColor.init(named: "HomeTextColor")
        
        self.buttonChangeLocation.setNormalTitle(Strings.changeButton)
        self.buttonChangeLocation.backgroundColor = .white
        self.buttonChangeLocation.setTitleColor(UIColor.init(named: "ButtonColor"), for: .normal)
        self.buttonChangeLocation.setTitleFont(fontWeight: .medium, ofSize: 12.0)
    }
    
    func fetchCartList() {
        self.viewCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"
//        self.dataStartedLoading()
        self.webService.viewCart(viewCartRequest: self.viewCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            DispatchQueue.main.async {
                self.table_view.stopShimmeringAnimation()
            }
//            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.cartMenuArray = data.menus ?? []
                self.cartTotal = "₹\(data.cartTotal ?? 0)"
                self.cartTotalInt = data.cartTotal ?? 0
                self.cart_id = data.id ?? ""
              //  self.labelTime.text = (data.)
                DispatchQueue.main.async {
                    if self.cartMenuArray.count > 0 {
                        self.table_view.isHidden = false
                        self.labelNoData.isHidden = true
                        self.viewLocationBG.isHidden = false
                        for index in 0...(self.cartMenuArray.count  - 1) {

                            self.waitingTime = self.waitingTime + (self.cartMenuArray[index].menuID?.itemID?.waitingTime ?? 0)
                        }
                        self.labelTime.text = "WaitingTime : " + String(self.waitingTime) + "  Mins"
                        self.table_view.reloadData()
                    }else{
                        self.table_view.isHidden = true
                        self.labelNoData.isHidden = false
                        self.viewLocationBG.isHidden = true
                        self.viewAddLocationBG.isHidden = true
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: HomeView.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func fetchAddressList() {
//        self.dataStartedLoading()
        self.addressListRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addressWebService.addressList(addressListRequest: self.addressListRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.addressList = data
                
                DispatchQueue.main.async {
                    if self.addressList.count>0 {
                        self.viewLocationBG.isHidden = false
                        self.viewAddLocationBG.isHidden = true
                        var isAnyDefaultExist: Bool = false
                        for item in self.addressList {
                            if item.isDefault == true {
                                isAnyDefaultExist = true
                                self.labelAddress.text = item.fullAddress
                                self.deliveryAddressId = item.id ?? ""
                            }
                        }
                        if isAnyDefaultExist == false {
                            self.labelAddress.text = self.addressList[self.addressList.count - 1].fullAddress
                            self.deliveryAddressId = self.addressList[self.addressList.count - 1].id ?? ""
                        }
                    }else{
                        self.viewLocationBG.isHidden = true
                        self.viewAddLocationBG.isHidden = false
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func addToCart(menuID: String, menuAmount: String, menuQuantity: String, vendorID: String) {
        self.addCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addCartRequest.menuId = menuID
        self.addCartRequest.menuAmount = menuAmount
        self.addCartRequest.menuQuantity = menuQuantity
        self.addCartRequest.vendorId = vendorID
        self.dataStartedLoading()
        self.webService.addToCart(addCartRequest: self.addCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchCartList()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func updateCart(cartID: String, menuID: String, menuAmount: String, menuQuantity: String, vendorId: String) {
        self.updateCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.updateCartRequest.menuId = menuID
        self.updateCartRequest.menuAmount = menuAmount
        self.updateCartRequest.menuQuantity = menuQuantity
        self.updateCartRequest.cartId = cartID
        self.updateCartRequest.vendorId = vendorId
        
        self.dataStartedLoading()
        self.webService.updateCart(updateCartRequest: self.updateCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchCartList()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func removeCart(cartID: String, menuID: String, vendorId: String) {
        self.removeCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.removeCartRequest.menuId = menuID
        self.removeCartRequest.cartId = cartID
        self.removeCartRequest.vendorId = vendorId
        
        self.dataStartedLoading()
        self.webService.removeCart(removeCartRequest: self.removeCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchCartList()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func payWithRazorPay() {
        let email = UserDefaultsManager.shared.getStringValue(forKey: .email)
        let phone = UserDefaultsManager.shared.getStringValue(forKey: .phone)
        razorpayObj = RazorpayCheckout.initWithKey(razorpayKey, andDelegateWithData: self)
        let options: [AnyHashable:Any] = [
            "prefill": [
                "contact": phone,
                "email": email
            ],
            "image": "",
            "amount" : self.cartTotalInt,
            "name": "",
            "theme": [
                "color": "#F37254"
            ]
            // follow link for more options - https://razorpay.com/docs/payment-gateway/web-integration/standard/checkout-form/
        ]
        if let rzp = self.razorpayObj {
            rzp.open(options)
        } else {
            print("Unable to initialize")
        }
    }
    
    func orderSubmit(paymentId: String) {
        self.orderSubmitRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.orderSubmitRequest.cartId = self.cart_id
        self.orderSubmitRequest.finalAmount = "\(self.cartTotalInt)"
        self.orderSubmitRequest.paymentId = paymentId
        self.orderSubmitRequest.deliveryAddressId = self.deliveryAddressId
        self.orderSubmitRequest.paymentStatus = "SUCCESS"
        
        self.dataStartedLoading()
        self.myOrderWebService.orderSubmit(orderSubmitRequest: self.orderSubmitRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
//                self.presentAlert(withTitle: "Success", message: "Payment Succeeded")
//                self.fetchCartList()
                
                DispatchQueue.main.async {
                    let paymentDoneView = PaymentDoneView.instantiateFromAppStoryboard(appStoryboard: .cart)
                    paymentDoneView.orderID = data.id ?? ""
                    self.navigationController?.pushViewController(paymentDoneView, animated: true)
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonChangeLocationAction(_ sender: Any) {
//        let searchLocationView = SearchLocationView.instantiateFromAppStoryboard(appStoryboard: .search)
//        searchLocationView.block = { (address, landmark, houseNo, lattitude, longitude, typeId, type, comingFrom) in
//            self.labelAddress.text = address
//            if type != "" {
//                if type == Strings.addressTypeHome {
//                    self.labelAddressTitle.text = "DELIVER TO HOME"
//                    self.imageViewHomeIcon.image = UIImage(named: "home")
//                }else if type == Strings.addressTypeWork {
//                    self.labelAddressTitle.text = "DELIVER TO WORK"
//                    self.imageViewHomeIcon.image = UIImage(named: "office_icon")
//                }else{
//                    self.labelAddressTitle.text = "DELIVER TO BELOW ADDRESS"
//                    self.imageViewHomeIcon.image = UIImage(named: "other_icon")
//                }
//            }
//        }
//        searchLocationView.selectedPlaceDelegate = self
//        searchLocationView.locationType = "Pickup"
//        searchLocationView.comingFrom = "Cart"
//
//        self.navigationController?.pushViewController(searchLocationView, animated: true)
        
        let addLocationView = AddLocationMainView.instantiateFromAppStoryboard(appStoryboard: .location)
        addLocationView.comingFrom = "Cart2"
        addLocationView.isAddAddress = true
        addLocationView.block2 = { (address, addressID, landmark, houseNo, lattitude, longitude, typeId, type, comingFrom) in
            
            self.viewAddLocationBG.isHidden = true
            self.viewLocationBG.isHidden = false
            self.labelAddress.text = address
            self.deliveryAddressId = addressID
            self.labelAddress.sizeToFit()
            if type != "" {
                if type == Strings.addressTypeHome {
                    self.labelAddressTitle.text = "DELIVER TO HOME"
                    self.imageViewHomeIcon.image = UIImage(named: "home")
                }else if type == Strings.addressTypeWork {
                    self.labelAddressTitle.text = "DELIVER TO WORK"
                    self.imageViewHomeIcon.image = UIImage(named: "office_icon")
                }else{
                    self.labelAddressTitle.text = "DELIVER TO BELOW ADDRESS"
                    self.imageViewHomeIcon.image = UIImage(named: "other_icon")
                }
            }
        }
        self.navigationController?.pushViewController(addLocationView, animated: true)
    }
    @IBAction func buttonAddAddressAction(_ sender: Any) {
        let addLocationView = AddLocationMainView.instantiateFromAppStoryboard(appStoryboard: .location)
        addLocationView.comingFrom = "Cart1"
        addLocationView.isAddAddress = true
        addLocationView.block2 = { (address, addressID, landmark, houseNo, lattitude, longitude, typeId, type, comingFrom) in
            
            self.viewAddLocationBG.isHidden = true
            self.viewLocationBG.isHidden = false
            self.labelAddress.text = address
            self.deliveryAddressId = addressID
            self.labelAddress.sizeToFit()
            if type != "" {
                if type == Strings.addressTypeHome {
                    self.labelAddressTitle.text = "DELIVER TO HOME"
                    self.imageViewHomeIcon.image = UIImage(named: "home")
                }else if type == Strings.addressTypeWork {
                    self.labelAddressTitle.text = "DELIVER TO WORK"
                    self.imageViewHomeIcon.image = UIImage(named: "office_icon")
                }else{
                    self.labelAddressTitle.text = "DELIVER TO BELOW ADDRESS"
                    self.imageViewHomeIcon.image = UIImage(named: "other_icon")
                }
            }
        }
        self.navigationController?.pushViewController(addLocationView, animated: true)
    }
    
}
extension CartView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension CartView : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if self.cartMenuArray.count > 0 {
                return self.cartMenuArray.count
            }else{
                return 2
            }
        }else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cartTableCell", for: indexPath) as! CartTableCell
            
            if self.cartMenuArray.count > 0 {
                cell.labelMenuTitle.setTextFont(fontWeight: .medium , ofSize: 15)
                cell.labelMenuTitle.text = self.cartMenuArray[indexPath.row].menuID?.itemID?.itemName
                cell.labelMenuTitle.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelActualPrice.setTextFont(fontWeight: .regular , ofSize: 11)
                cell.labelActualPrice.text = "₹ \(self.cartMenuArray[indexPath.row].menuID?.itemID?.price ?? 0)"
                cell.labelActualPrice.textColor = UIColor.init(named: "HomeTextLightColor")
                
                cell.imageLine.backgroundColor = .lightGray
                
                let discountType = self.cartMenuArray[indexPath.row].menuID?.itemID?.discountType
                let price = self.cartMenuArray[indexPath.row].menuID?.itemID?.price ?? 0
                let discountPrice = self.cartMenuArray[indexPath.row].menuID?.itemID?.discountAmount ?? 0
                var offerPrice : Float = 0.0
                if discountType == "FLAT" {
                    offerPrice = Float(price-discountPrice)
                }else if discountType == "NONE" {
                    offerPrice = Float(price)
                }else{
                    offerPrice = Float(price-((price*discountPrice)/100))
                }
                
                cell.labelDiscountedPrice.setTextFont(fontWeight: .regular , ofSize: 12)
                cell.labelDiscountedPrice.text = "₹ \(offerPrice)"
                cell.labelDiscountedPrice.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelItemQuantity.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelItemQuantity.text = "\(self.cartMenuArray[indexPath.row].menuQuantity ?? 0)"
                cell.labelItemQuantity.textColor = UIColor.init(named: "HomeTextColor")
                
                let isVegItem = self.cartMenuArray[indexPath.row].menuID?.itemID?.type
                if isVegItem == "NON VEG" {
                    cell.imageViewNonVeg.image = UIImage(named: "nonveg")
                }else{
                    cell.imageViewNonVeg.image = UIImage(named: "veg")
                }
                
                let itemImage = self.cartMenuArray[indexPath.row].menuID?.itemID?.menuImage ?? ""
                if itemImage != "" {
                    cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
                }
                cell.imageViewMenuItem.contentMode = .scaleAspectFill
                cell.buttonMinus.tag = indexPath.row
                cell.buttonPlus.tag = indexPath.row
                cell.buttonRemove.tag = indexPath.row
                cell.delegate = self
            }
            
            return cell
        }else{
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cartPromoCodeTableCell", for: indexPath) as! CartPromoCodeTableCell
                
                cell.textFieldPromoCode.font = UIFont(name: "CeraPro-Regular", size: 16)
                cell.textFieldPromoCode.fontSize =  16
                cell.textFieldPromoCode.placeholder = "Promo Code"
                cell.textFieldPromoCode.textColor = UIColor.init(named: "HomeTextColor")
                cell.textFieldPromoCode.delegate = self
                cell.textFieldPromoCode.backgroundColor = .clear
                cell.textFieldPromoCode.tintColor = UIColor.init(named: "HomeTextColor")
                
                cell.buttonApply.setNormalTitle("Apply")
                cell.buttonApply.backgroundColor = UIColor.init(named: "ButtonColor")
                cell.buttonApply.cornerRadius = 10
                cell.buttonApply.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
                cell.buttonApply.setTitleFont(fontWeight: .regular, ofSize: 12.0)
                
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cartPriceDetailsTableCell", for: indexPath) as! CartPriceDetailsTableCell
                
                cell.labelCartTotalTitle.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelCartTotalTitle.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelCartTotalValue.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelCartTotalValue.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelCartTaxTitle.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelCartTaxTitle.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelTaxValue.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelTaxValue.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelDeliveryTitle.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelDeliveryTitle.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelDeliveryValue.setTextFont(fontWeight: .regular , ofSize: 14)
                cell.labelDeliveryValue.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelSubtotalTitle.setTextFont(fontWeight: .medium , ofSize: 18)
                cell.labelSubtotalTitle.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelSubtotalValue.setTextFont(fontWeight: .medium , ofSize: 18)
                cell.labelSubtotalValue.textColor = UIColor.init(named: "HomeTextColor")
                
                cell.labelCartTotalValue.text = self.cartTotal
                cell.labelTaxValue.text = "₹0"
                cell.labelDeliveryValue.text = "Free"
                cell.labelSubtotalValue.text = self.cartTotal
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cartSubmitTableCell", for: indexPath) as! CartSubmitTableCell
                
//                cell.buttonSubmit.setNormalTitle("PROCESS TO PAY")
//                cell.buttonSubmit.backgroundColor = UIColor.init(named: "ButtonColor")
//                cell.buttonSubmit.cornerRadius = 10
//                cell.buttonSubmit.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
//                cell.buttonSubmit.setTitleFont(fontWeight: .regular, ofSize: 17.0)
                cell.delegate = self
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 150
        }else{
            if indexPath.row == 0 {
                return 80
            } else if indexPath.row == 1 {
                return 200
            } else {
                return 80
            }
        }
    }
}
extension CartView : CartDelegate {
    func minusCart(_ sender: UIButton) {
        let menuID = self.cartMenuArray[sender.tag].menuID?.id ?? ""
        
        let discountType = self.cartMenuArray[sender.tag].menuID?.itemID?.discountType
        let price = self.cartMenuArray[sender.tag].menuID?.itemID?.price ?? 0
        let discountPrice = self.cartMenuArray[sender.tag].menuID?.itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        let menuQuantity: Int = self.cartMenuArray[sender.tag].menuQuantity ?? 0
        let vendorID: String = self.cartMenuArray[sender.tag].menuID?.restaurantID ?? ""
        
        if menuQuantity == 1 {
            self.removeCart(cartID: self.cart_id, menuID: menuID, vendorId: vendorID)
        }else{
            if menuQuantity > 1 {
                self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity-1)", vendorId: vendorID)
            }
        }
    }
    
    func plusCart(_ sender: UIButton) {
        
        if self.cartMenuArray[sender.tag].menuID?.itemID?.isActive == true {
            let menuID = self.cartMenuArray[sender.tag].menuID?.id ?? ""
            
            let discountType = self.cartMenuArray[sender.tag].menuID?.itemID?.discountType
            let price = self.cartMenuArray[sender.tag].menuID?.itemID?.price ?? 0
            let discountPrice = self.cartMenuArray[sender.tag].menuID?.itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice)"
            let menuQuantity: Int = self.cartMenuArray[sender.tag].menuQuantity ?? 0
            let vendorID: String = self.cartMenuArray[sender.tag].menuID?.restaurantID ?? ""
            
            self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)", vendorId: vendorID)
        }
        else {
             self.showErrorAlertWithTitle(title: "Alert", message: "This item is not available now")
        }
    }
    
    func removeCart(_ sender: UIButton) {
        let menuID = self.cartMenuArray[sender.tag].menuID?.id ?? ""
        let vendorID = self.cartMenuArray[sender.tag].menuID?.restaurantID ?? ""
        
        self.removeCart(cartID: self.cart_id, menuID: menuID, vendorId: vendorID)
    }
}
extension CartView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension CartView : SelecetdPlacesDelegate {
    
    func locationSelected(currentLocation: Location, ofType: String) {
        print("location : ",currentLocation)
        self.labelAddress.text = currentLocation.locationName ?? ""
        self.imageViewHomeIcon.image = UIImage(named: "other_icon")
        //        self.lattitude = "\(currentLocation.locationLatitude ?? 0.0)"
        //        self.longitude = "\(currentLocation.locationLongitude ?? 0.0)"
    }
    func selectedLocationType(ofType: String,selectedIndex : Int) {
        
    }
}
extension CartView: CartSubmitDelegate {
    func submitCart(_ sender: UIButton) {
        self.payWithRazorPay()
    }
}
extension CartView : RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String) {
        print("error: ", code, str)
        self.presentAlert(withTitle: "Alert", message: str)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        print("success: ", payment_id)
        self.presentAlert(withTitle: "Success", message: "Payment Succeeded")
    }
}
extension CartView: RazorpayPaymentCompletionProtocolWithData {
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", code)
        self.presentAlert(withTitle: "Alert", message: str)
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", payment_id)
        self.orderSubmit(paymentId: payment_id)
    }
}
extension CartView {
    func presentAlert(withTitle title: String?, message : String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Okay", style: .default)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
