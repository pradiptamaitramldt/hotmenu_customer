//
//  CartModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 24/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct ViewCartRequest {
    
    var customerId: String
    
    
    init() {
        self.customerId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        
        return tempDictionary
    }
}

// MARK: - ViewCartResponseModel
struct ViewCartResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ViewCartResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct ViewCartResponseData: Codable {
    let cartTotal, isCheckout: Int?
    let status, id, customerID, restaurantID: String?
    let menus: [CartMenu]?
    let createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case cartTotal, isCheckout, status
        case id = "_id"
        case customerID = "customerId"
        case restaurantID = "restaurantId"
        case menus, createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - Menu
struct CartMenu: Codable {
    let menuAddedDate, id: String?
    let menuID: CartMenuID?
    let menuAmount, menuQuantity, menuTotal: Int?

    enum CodingKeys: String, CodingKey {
        case menuAddedDate
        case id = "_id"
        case menuID = "menuId"
        case menuAmount, menuQuantity, menuTotal
    }
}

// MARK: - MenuID
struct CartMenuID: Codable {
    let id: String
    let itemID: CartItemID?
    let restaurantID, createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case itemID = "itemId"
        case restaurantID = "restaurantId"
        case createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - ItemID
struct CartItemID: Codable {
    let itemIDDescription: String?
    let discountType: String?
    let discountAmount: Int?
    let validityFrom, validityTo: String?
    let isActive, isApprove: Bool?
    let popularity: Int?
    let id, itemName, categoryID, mealTypeID: String?
    let type: String?
    let price, waitingTime: Int?
    let menuImage: String?
    let itemOptions: [ItemOption]?
    let createdAt, updatedAt: String?
    let v: Int?
    let ingredients, recipe: JSONNull?

    enum CodingKeys: String, CodingKey {
        case itemIDDescription = "description"
        case discountType, discountAmount, validityFrom, validityTo, isActive, isApprove, popularity
        case id = "_id"
        case itemName
        case categoryID = "categoryId"
        case mealTypeID = "mealTypeId"
        case type, price, waitingTime, menuImage, itemOptions, createdAt, updatedAt
        case v = "__v"
        case ingredients, recipe
    }
}

struct UpdateCartRequest {
    
    var customerId: String
    var cartId: String
    var menuId: String
    var menuAmount: String
    var menuQuantity: String
    var vendorId: String
    
    init() {
        self.customerId = ""
        self.cartId = ""
        self.menuId = ""
        self.menuAmount = ""
        self.menuQuantity = ""
        self.vendorId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["cartId"] = self.cartId
        tempDictionary["menuId"] = self.menuId
        tempDictionary["menuAmount"] = self.menuAmount
        tempDictionary["menuQuantity"] = self.menuQuantity
        tempDictionary["vendorId"] = self.vendorId
        
        return tempDictionary
    }
}

struct AddCartRequest {
    
    var customerId: String
    var menuId: String
    var menuAmount: String
    var menuQuantity: String
    var vendorId: String
    
    init() {
        self.customerId = ""
        self.menuId = ""
        self.menuAmount = ""
        self.menuQuantity = ""
        self.vendorId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["menuId"] = self.menuId
        tempDictionary["menuAmount"] = self.menuAmount
        tempDictionary["menuQuantity"] = self.menuQuantity
        tempDictionary["vendorId"] = self.vendorId
        
        return tempDictionary
    }
}

// MARK: - AddCartResponseModel
struct AddCartResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: AddCartResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct AddCartResponseData: Codable {
    let cartTotal, isCheckout: Int?
    let status, id, customerID, restaurantID: String?
    let menus: [AddCartMenu]?
    let createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case cartTotal, isCheckout, status
        case id = "_id"
        case customerID = "customerId"
        case restaurantID = "restaurantId"
        case menus, createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - Menu
struct AddCartMenu: Codable {
    let menuAddedDate, id: String?
    let menuID: CartMenuID?
    let menuAmount, menuQuantity, menuTotal: Int?

    enum CodingKeys: String, CodingKey {
        case menuAddedDate
        case id = "_id"
        case menuID = "menuId"
        case menuAmount, menuQuantity, menuTotal
    }
}

struct RemoveCartRequest {
    
    var customerId: String
    var menuId: String
    var cartId: String
    var vendorId: String
    
    init() {
        self.customerId = ""
        self.menuId = ""
        self.cartId = ""
        self.vendorId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["menuId"] = self.menuId
        tempDictionary["cartId"] = self.cartId
        tempDictionary["vendorId"] = self.vendorId
        
        return tempDictionary
    }
}

struct OrderSubmitRequest {
    
    var customerId: String
    var paymentStatus: String
    var cartId: String
    var paymentId: String
    var finalAmount: String
    var deliveryAddressId: String
    
    init() {
        self.customerId = ""
        self.paymentStatus = ""
        self.cartId = ""
        self.paymentId = ""
        self.finalAmount = ""
        self.deliveryAddressId = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["paymentStatus"] = self.paymentStatus
        tempDictionary["cartId"] = self.cartId
        tempDictionary["paymentId"] = self.paymentId
        tempDictionary["finalAmount"] = self.finalAmount
        tempDictionary["deliveryAddressId"] = self.deliveryAddressId
        
        return tempDictionary
    }
}
