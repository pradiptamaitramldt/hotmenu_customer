//
//  AddLocationMainView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 14/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class AddLocationMainView: UIViewController {
    
    @IBOutlet weak var viewMapBG: UIView!
    @IBOutlet weak var viewAddLocationBG: UIView!
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var labelDeliveryTitle: CustomLabel!
    @IBOutlet weak var buttonChange: CustomButton!
    @IBOutlet weak var houseNumberText: CustomTextField!
    @IBOutlet weak var landmarkTextField: CustomTextField!
    @IBOutlet var collView: UICollectionView!
    @IBOutlet weak var buttonSave: CustomButton!
    @IBOutlet weak var labelSaveAs: CustomLabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var imageViewBack: UIImageView!
    
    //    var addLocationView = AddLocationView()
    var selectedIndex: Int = -1
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    // An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    var addressTypeId: String = ""
    var addressId: String = ""
    var addressType: String = ""
    var lattitude: String = ""
    var longitude: String = ""
    var block: ((_ address: String, _ landmark: String, _ houseNumber: String, _ lat: String, _ long: String, _ typeID: String, _ type: String, _ comingFrom: String)->Void)?
    var block2: ((_ address: String, _ addressID: String, _ landmark: String, _ houseNumber: String, _ lat: String, _ long: String, _ typeID: String, _ type: String, _ comingFrom: String)->Void)?
    var comingFrom: String = ""
    var isAddAddress: Bool = true
    
    // The currently selected place.
    var selectedPlace: GMSPlace?
    var editAddressDataModel = EditAddressDataModel()
    private var webService = LocationAPI()
    var addressTypeArray: [AddressTypesResponseDatum] = []
    private var addAddressRequest = AddAddressRequest()
    private var addressTypeRequest = AddressTypeRequest()
    private var editAddressRequest = EditAddressRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.addAddressRequest.delegate = self
        self.editAddressRequest.delegate = self
        // Create a map.
        let camera = GMSCameraPosition.camera(withLatitude: Utility.lattitude,
                                              longitude: Utility.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: self.viewMapBG.bounds, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        // Add the map to the view, hide it until we've got a location update.
        self.viewMapBG.addSubview(mapView)
        mapView.isHidden = true
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Utility.lattitude, longitude: Utility.longitude)
        marker.title = ""
        marker.snippet = ""
        marker.isDraggable = true
        if self.isAddAddress == true {
            self.reverseGeocoding(marker: marker)
        }
        marker.icon = UIImage(named: "fixed_pin")
        marker.map = mapView
        
        self.view.bringSubviewToFront(self.viewAddLocationBG)
        self.view.bringSubviewToFront(self.imageViewBack)
        self.view.bringSubviewToFront(self.buttonBack)
        
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        
        self.collView.register(UINib(nibName: "LocationCollView", bundle: nil), forCellWithReuseIdentifier: "LocationCollCell")
        self.collView.dataSource = self
        self.collView.delegate = self
        self.getAddressTypes()
        self.setup()
    }
    
    func setup() {
        self.labelDeliveryTitle.setTextFont(fontWeight: .medium , ofSize: 14)
        self.labelDeliveryTitle.text = "Delivery Location"
        self.labelDeliveryTitle.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelSaveAs.setTextFont(fontWeight: .medium , ofSize: 14)
        self.labelSaveAs.text = "Save as"
        self.labelSaveAs.textColor = UIColor.init(named: "HomeTextColor")
        
        self.addressLabel.setTextFont(fontWeight: .regular , ofSize: 15)
        self.addressLabel.text = ""
        self.addressLabel.textColor = UIColor.init(named: "HomeTextColor")
        
        self.buttonChange.setNormalTitle(Strings.changeButton)
        self.buttonChange.backgroundColor = .white
        self.buttonChange.setTitleColor(UIColor.init(named: "ButtonColor"), for: .normal)
        self.buttonChange.setTitleFont(fontWeight: .medium, ofSize: 12.0)
        
        self.houseNumberText.attributedPlaceholder = NSAttributedString(string: Strings.placeholderHouseNo, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.houseNumberText.setFont(fontWeight: .regular, size: 14)
        self.houseNumberText.textColor = UIColor.init(named: "HomeTextColor")
        self.houseNumberText.delegate = self
        self.houseNumberText.backgroundColor = .clear
        self.houseNumberText.tintColor = UIColor.init(named: "HomeTextColor")
        
        self.landmarkTextField.attributedPlaceholder = NSAttributedString(string: Strings.placeholderLandMark, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        self.landmarkTextField.setFont(fontWeight: .regular, size: 14)
        self.landmarkTextField.textColor = UIColor.init(named: "HomeTextColor")
        self.landmarkTextField.delegate = self
        self.landmarkTextField.backgroundColor = .clear
        self.landmarkTextField.tintColor = UIColor.init(named: "HomeTextColor")
        
        self.buttonSave.setNormalTitle(Strings.saveAndProceedButton)
        self.buttonSave.backgroundColor = UIColor.init(named: "ButtonColor")
        self.buttonSave.cornerRadius = 10
        self.buttonSave.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonSave.setTitleFont(fontWeight: .medium, ofSize: 18.0)
        
        if self.isAddAddress == false {
            self.addressLabel.text = self.editAddressDataModel.fullAddress
            self.landmarkTextField.text = self.editAddressDataModel.landmark
            self.houseNumberText.text = self.editAddressDataModel.flatOrHouseOrBuildingOrCompany
            Utility.lattitude = Double(self.editAddressDataModel.latitude)!
            Utility.longitude = Double(self.editAddressDataModel.longitude)!
            
            self.setMarker()
            self.collView.reloadData()
        }
    }
    
    func setMarker() {
        self.mapView.removeFromSuperview()
        let camera = GMSCameraPosition.camera(withLatitude: Utility.lattitude,
                                              longitude: Utility.longitude,
                                              zoom: self.zoomLevel)
        self.mapView = GMSMapView.map(withFrame: self.viewMapBG.bounds, camera: camera)
        self.mapView.settings.myLocationButton = true
        self.mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.mapView.isMyLocationEnabled = true
        self.mapView.delegate = self
        
        // Add the map to the view, hide it until we've got a location update.
        self.viewMapBG.addSubview(self.mapView)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Utility.lattitude, longitude: Utility.longitude)
        marker.title = ""
        marker.snippet = ""
        marker.isDraggable = true
        marker.icon = UIImage(named: "fixed_pin")
        marker.map = self.mapView
    }
    
    func getAddressTypes() {
        self.addressTypeRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.dataStartedLoading()
        self.webService.addressTypes(addressTypeRequest: self.addressTypeRequest) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.addressTypeArray = data
                //self.addressTypeId = self.addressTypeArray[0].id ?? ""
                
                DispatchQueue.main.async {
                    self.collView.reloadData()
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func addAddress() {
        self.dataStartedLoading()
        self.webService.addAddress(addAddressRequest: self.addAddressRequest, imageData: nil) { (success, message, verifyMobileResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successLoginResponse = verifyMobileResponse else {
                    return
                }
                
                let message = successLoginResponse.message
                let addressID = successLoginResponse.responseData?[0].id ?? ""
                if self.comingFrom == "Cart1" {
                    DispatchQueue.main.async {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: CartView.self) {
                                self.block2?(self.addressLabel.text ?? "", addressID, self.landmarkTextField.text ?? "", self.houseNumberText.text ?? "", "\(Utility.lattitude)", "\(Utility.longitude)", self.addressTypeId, self.addressType, self.comingFrom)
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }else if self.comingFrom == "Cart2" {
                   DispatchQueue.main.async {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: CartView.self) {
                                self.block2?(self.addressLabel.text ?? "", addressID, self.landmarkTextField.text ?? "", self.houseNumberText.text ?? "", "\(Utility.lattitude)", "\(Utility.longitude)", self.addressTypeId, self.addressType, self.comingFrom)
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }else if self.comingFrom == "AddressList" {
                    DispatchQueue.main.async {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: AddressListView.self) {
                                self.block?(self.addressLabel.text ?? "", self.landmarkTextField.text ?? "", self.houseNumberText.text ?? "", "\(Utility.lattitude)", "\(Utility.longitude)", self.addressTypeId, self.addressType, self.comingFrom)
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
                self.showSuccessAlertWithTitle(title: Strings.blankString, message: message ?? "")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func editAddress() {
        self.dataStartedLoading()
        self.webService.editAddress(editAddressRequest: self.editAddressRequest, imageData: nil) { (success, message, verifyMobileResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successLoginResponse = verifyMobileResponse else {
                    return
                }
                
                let message = successLoginResponse.message
                DispatchQueue.main.async {
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: AddressListView.self) {
                            self.block?(self.addressLabel.text ?? "", self.landmarkTextField.text ?? "", self.houseNumberText.text ?? "", "\(Utility.lattitude)", "\(Utility.longitude)", self.addressTypeId, self.addressType, self.comingFrom)
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
                self.showSuccessAlertWithTitle(title: Strings.blankString, message: message ?? "")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func reverseGeocoding(marker: GMSMarker) {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                
                print("Response is = \(address)")
                print("Response is = \(lines)")
                
                currentAddress = lines.joined(separator: "\n")
                self.addressLabel.text = currentAddress
                Utility.lattitude = address.coordinate.latitude
                Utility.longitude = address.coordinate.longitude
            }
            marker.title = currentAddress
            marker.map = self.mapView
        }
    }
    
    // Populate the array with the list of likely places.
    func listLikelyPlaces() {
        // Clean up from previous sessions.
        likelyPlaces.removeAll()
        
        placesClient.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: .name) { (placeLikelihoods, error) in
            guard error == nil else {
                // TODO: Handle the error.
                print("Current Place error: \(error!.localizedDescription)")
                return
            }
            
            guard let placeLikelihoods = placeLikelihoods else {
                print("No places found.")
                return
            }
            
            // Get likely places and add to the list.
            for likelihood in placeLikelihoods {
                let place = likelihood.place
                self.likelyPlaces.append(place)
            }
        }
        self.view.bringSubviewToFront(self.viewAddLocationBG)
    }
    @IBAction func buttonChangeLocationAction(_ sender: Any) {
        let searchLocationView = SearchLocationView.instantiateFromAppStoryboard(appStoryboard: .search)
        searchLocationView.block = { (addressID, address, landmark, houseNo, lattitude, longitude, typeId, type, comingFrom) in
            
            print("coming from...\(comingFrom)")
            if comingFrom == "Cart2" {
                DispatchQueue.main.async {
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: CartView.self) {
                            self.block2?(address, addressID, landmark, houseNo, lattitude, longitude, typeId, type, comingFrom)
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
            }else{
                Utility.lattitude = Double(lattitude)!
                Utility.longitude = Double(longitude)!
                self.addressLabel.text = address
//                self.comingFrom = comingFrom
                self.addressTypeId = typeId
                self.addressType = type
                
                self.setMarker()
                
                if landmark != "" {
                    self.landmarkTextField.text = landmark
                }
                if houseNo != "" {
                    self.houseNumberText.text = houseNo
                }
                if type != "" {
                    if type == Strings.addressTypeHome {
                        self.selectedIndex = 0
                        self.collView.reloadData()
                    }else if type == Strings.addressTypeWork {
                        self.selectedIndex = 1
                        self.collView.reloadData()
                    }else{
                        self.selectedIndex = 2
                        self.collView.reloadData()
                    }
                }
            }
        }
        searchLocationView.selectedPlaceDelegate = self
        searchLocationView.locationType = "Pickup"
        searchLocationView.comingFrom = self.comingFrom
        self.navigationController?.pushViewController(searchLocationView, animated: true)
    }
    @IBAction func buttonSaveLocationAction(_ sender: Any) {
        if self.isAddAddress == true {
            self.addAddressRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
            self.addAddressRequest.addressType = self.addressTypeId
            self.addAddressRequest.fullAddress = self.addressLabel.text ?? ""
            self.addAddressRequest.landmark = self.landmarkTextField.text ?? ""
            self.addAddressRequest.latitude = "\(Utility.lattitude)"
            self.addAddressRequest.longitude = "\(Utility.longitude)"
            self.addAddressRequest.flatOrHouseOrBuildingOrCompany = self.houseNumberText.text ?? ""
            self.addAddressRequest.validateData()
        }else{
            
            self.editAddressRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
            self.editAddressRequest.fullAddress = self.addressLabel.text ?? ""
            self.editAddressRequest.landmark = self.landmarkTextField.text ?? ""
            self.editAddressRequest.latitude = "\(Utility.lattitude)"
            self.editAddressRequest.longitude = "\(Utility.longitude)"
            self.editAddressRequest.flatOrHouseOrBuildingOrCompany = self.houseNumberText.text ?? ""
            self.editAddressRequest.addressID = self.addressId
            self.editAddressRequest.validateData()
        }
    }
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension AddLocationMainView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension AddLocationMainView : SelecetdPlacesDelegate {
    
    func locationSelected(currentLocation: Location, ofType: String) {
        print("location : ",currentLocation)
        self.addressLabel.text = currentLocation.locationName ?? ""
        Utility.lattitude = currentLocation.locationLatitude ?? 0.0
        Utility.longitude = currentLocation.locationLongitude ?? 0.0
        
        self.setMarker()
    }
    func selectedLocationType(ofType: String,selectedIndex : Int) {
        
        //        self.selectedIndex = selectedIndex
        self.collView.reloadData()
        
    }
}
extension AddLocationMainView : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.addressTypeArray.count
        
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationCollCell", for: indexPath as IndexPath) as! LocationCollCell
        cell.labelAddressType.setTextFont(fontWeight: .regular, ofSize: 14)
        
        let type = self.addressTypeArray[indexPath.row].type
        if type == Strings.addressTypeHome {
            cell.icon.image = UIImage(named: "home")
        }else if type == Strings.addressTypeWork {
            cell.icon.image = UIImage(named: "office_icon")
        }else{
            cell.icon.image = UIImage(named: "other_icon")
        }
        if self.addressTypeArray[indexPath.row].status == 0 {
            cell.isUserInteractionEnabled = true
        }else{
            cell.isUserInteractionEnabled = false
        }
        if self.selectedIndex != -1 {
            if indexPath.row == self.selectedIndex {
                cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                cell.icon.tintColor = UIColor.white
                cell.bgView.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.1019607843, blue: 0.2588235294, alpha: 1)
                cell.labelAddressType.textColor = .white
            }
            else {
                cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                cell.icon.tintColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
                cell.bgView.backgroundColor = .white
                cell.labelAddressType.textColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
            }
        }else{
            cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
            cell.icon.tintColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
            cell.bgView.backgroundColor = .white
            cell.labelAddressType.textColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
        }
        
        if self.isAddAddress == false {
            cell.isUserInteractionEnabled = false
            let type = self.editAddressDataModel.addressType
            if type == Strings.addressTypeHome {
                if indexPath.row == 0 {
                    cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                    cell.icon.tintColor = UIColor.white
                    cell.bgView.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.1019607843, blue: 0.2588235294, alpha: 1)
                    cell.labelAddressType.textColor = .white
                }else {
                    cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                    cell.icon.tintColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
                    cell.bgView.backgroundColor = .white
                    cell.labelAddressType.textColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
                }
            }else if type == Strings.addressTypeWork {
                if indexPath.row == 1 {
                    cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                    cell.icon.tintColor = UIColor.white
                    cell.bgView.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.1019607843, blue: 0.2588235294, alpha: 1)
                    cell.labelAddressType.textColor = .white
                }else {
                    cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                    cell.icon.tintColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
                    cell.bgView.backgroundColor = .white
                    cell.labelAddressType.textColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
                }
            }else{
                if indexPath.row == 2 {
                    cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                    cell.icon.tintColor = UIColor.white
                    cell.bgView.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.1019607843, blue: 0.2588235294, alpha: 1)
                    cell.labelAddressType.textColor = .white
                }else {
                    cell.icon.image = cell.icon.image?.withRenderingMode(.alwaysTemplate)
                    cell.icon.tintColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
                    cell.bgView.backgroundColor = .white
                    cell.labelAddressType.textColor = #colorLiteral(red: 0.5411764706, green: 0.5960784314, blue: 0.7294117647, alpha: 1)
                }
            }
        }
        
        cell.labelAddressType.text = self.addressTypeArray[indexPath.item].type
        cell.labelAddressType.setTextFont(fontWeight: .regular , ofSize: 14)
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 120, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
        self.addressTypeId = self.addressTypeArray[indexPath.row].id ?? ""
        if indexPath.row == 0 {
            self.addressType = Strings.addressTypeHome
        }else if indexPath.row == 1 {
            self.addressType = Strings.addressTypeWork
        }else{
            self.addressType = Strings.addressTypeOther
        }
        self.collView.reloadData()
    }
}
// Delegates to handle events for the location manager.
extension AddLocationMainView: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        listLikelyPlaces()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        @unknown default:
            fatalError()
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
extension AddLocationMainView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
extension AddLocationMainView : AddAddressRequestDelegate {
    func invalidAddAddressData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func addAddressValidationPassed() {
        self.addAddress()
    }
}
extension AddLocationMainView : EditAddressRequestDelegate {
    func invalidEditAddressData(message: String) {
        self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
    }
    
    func editAddressValidationPassed() {
        self.editAddress()
    }
}
extension AddLocationMainView: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
        if self.isAddAddress == true {
            self.reverseGeocoding(marker: marker)
            print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
            self.houseNumberText.text = ""
            self.landmarkTextField.text = ""
        }
    }
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
}
