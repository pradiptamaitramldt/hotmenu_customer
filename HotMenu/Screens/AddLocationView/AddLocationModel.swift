//
//  AddLocationModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 15/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct AddressTypeRequest {
    
    var userID: String
            
    init() {
        self.userID = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        
        return tempDictionary
    }
}

// MARK: - AddressTypesResponseModel
struct AddressTypesResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [AddressTypesResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct AddressTypesResponseDatum: Codable {
    let id, type: String?
    let status: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type, status
    }
}

struct AddAddressRequest {
    
    var userID: String
    var addressType: String
    var landmark: String
    var latitude: String
    var longitude: String
    var fullAddress: String
    var flatOrHouseOrBuildingOrCompany: String
    
    var delegate: AddAddressRequestDelegate?
        
    init() {
        self.userID = ""
        self.addressType = ""
        self.landmark = ""
        self.latitude = ""
        self.longitude = ""
        self.fullAddress = ""
        self.flatOrHouseOrBuildingOrCompany = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.addressType != "" {
            if self.landmark != "" {
                if self.fullAddress != "" {
                    if self.flatOrHouseOrBuildingOrCompany != "" {
                        delegate.addAddressValidationPassed()
                    }else{
                        delegate.invalidAddAddressData(message: ErrorMessages.invalidHouseNumber)
                    }
                }else{
                    delegate.invalidAddAddressData(message: ErrorMessages.invalidFullAddress)
                }
            }else{
                delegate.invalidAddAddressData(message: ErrorMessages.invalidLandmark)
            }
        }else{
            delegate.invalidAddAddressData(message: ErrorMessages.invalidAddressType)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        tempDictionary["addressType"] = self.addressType
        tempDictionary["landmark"] = self.landmark
        tempDictionary["latitude"] = self.latitude
        tempDictionary["longitude"] = self.longitude
        tempDictionary["fullAddress"] = self.fullAddress
        tempDictionary["flatOrHouseOrBuildingOrCompany"] = self.flatOrHouseOrBuildingOrCompany
        
        return tempDictionary
    }
}

protocol AddAddressRequestDelegate {
    
    func invalidAddAddressData(message: String)
    func addAddressValidationPassed()
}

// MARK: - AddAddressResponseModel
struct AddAddressResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [AddAddressResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct AddAddressResponseDatum: Codable {
    let location: LocationData?
    let isDefault: Bool?
    let landmark, id: String?
    let addressType: AddressType?
    let fullAddress, flatOrHouseOrBuildingOrCompany, userID, createdAt: String?
    let updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case location, isDefault, landmark
        case id = "_id"
        case addressType, fullAddress, flatOrHouseOrBuildingOrCompany
        case userID = "userId"
        case createdAt, updatedAt
        case v = "__v"
    }
}

struct EditAddressRequest {
    
    var userID: String
    var addressID: String
    var landmark: String
    var latitude: String
    var longitude: String
    var fullAddress: String
    var flatOrHouseOrBuildingOrCompany: String
    
    var delegate: EditAddressRequestDelegate?
        
    init() {
        self.userID = ""
        self.addressID = ""
        self.landmark = ""
        self.latitude = ""
        self.longitude = ""
        self.fullAddress = ""
        self.flatOrHouseOrBuildingOrCompany = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.landmark != "" {
            if self.fullAddress != "" {
                if self.flatOrHouseOrBuildingOrCompany != "" {
                    delegate.editAddressValidationPassed()
                }else{
                    delegate.invalidEditAddressData(message: ErrorMessages.invalidHouseNumber)
                }
            }else{
                delegate.invalidEditAddressData(message: ErrorMessages.invalidFullAddress)
            }
        }else{
            delegate.invalidEditAddressData(message: ErrorMessages.invalidLandmark)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        tempDictionary["addressId"] = self.addressID
        tempDictionary["landmark"] = self.landmark
        tempDictionary["latitude"] = self.latitude
        tempDictionary["longitude"] = self.longitude
        tempDictionary["fullAddress"] = self.fullAddress
        tempDictionary["flatOrHouseOrBuildingOrCompany"] = self.flatOrHouseOrBuildingOrCompany
        
        return tempDictionary
    }
}

protocol EditAddressRequestDelegate {
    
    func invalidEditAddressData(message: String)
    func editAddressValidationPassed()
}

struct EditAddressDataModel {
    
    var addressID: String
    var addressType: String
    var landmark: String
    var latitude: String
    var longitude: String
    var fullAddress: String
    var flatOrHouseOrBuildingOrCompany: String
    
        
    init() {
        self.addressID = ""
        self.addressType = ""
        self.landmark = ""
        self.latitude = ""
        self.longitude = ""
        self.fullAddress = ""
        self.flatOrHouseOrBuildingOrCompany = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["addressId"] = self.addressID
        tempDictionary["addressType"] = self.addressType
        tempDictionary["landmark"] = self.landmark
        tempDictionary["latitude"] = self.latitude
        tempDictionary["longitude"] = self.longitude
        tempDictionary["fullAddress"] = self.fullAddress
        tempDictionary["flatOrHouseOrBuildingOrCompany"] = self.flatOrHouseOrBuildingOrCompany
        
        return tempDictionary
    }
}
