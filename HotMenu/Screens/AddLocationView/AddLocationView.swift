//
//  AddLocationView.swift
//  LocationTacking
//
//  Created by BrainiumSSD on 08/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit

protocol ButtonDelegate {
    func  didChangeButtonSelected()
    func didProceedButtonAction()
}

class AddLocationView: UIView {
    
    //MARK:- IBOutlet
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var labelDeliveryTitle: CustomLabel!
    @IBOutlet weak var buttonChange: CustomButton!
    @IBOutlet weak var houseNumberText: CustomTextField!
    @IBOutlet weak var landmarkTextField: CustomTextField!
    @IBOutlet var collView: UICollectionView!
    @IBOutlet weak var buttonSave: CustomButton!
    @IBOutlet weak var labelSaveAs: CustomLabel!
    
    var buttonDelegate : ButtonDelegate?
    
    class func instanceFromNib() -> UIView {
        
        return UINib(nibName: "AddLocationView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AddLocationView
    }
    
    //MARK:- IBAction
    @IBAction func changeButtonAction(_ sender: Any) {
        buttonDelegate?.didChangeButtonSelected()
    }
    
 
    
    @IBAction func proceedButtonAction(_ sender: Any) {
        buttonDelegate?.didProceedButtonAction()
    }
}
