//
//  LocationCollCell.swift
//  LocationTacking
//
//  Created by BrainiumSSD on 11/09/20.
//  Copyright © 2020 BrainiumSSD. All rights reserved.
//

import UIKit

class LocationCollCell: UICollectionViewCell {
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var icon: UIImageView!
    @IBOutlet weak var labelAddressType: CustomLabel!
    
}
