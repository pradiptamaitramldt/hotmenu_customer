//
//  InitialView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 05/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class InitialView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loginView = LoginView.instantiateFromAppStoryboard(appStoryboard: .login)
        self.navigationController?.pushViewController(loginView, animated: true)
    }

}
