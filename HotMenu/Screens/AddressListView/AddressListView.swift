//
//  AddressListView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 17/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class AddressListView: UIViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var buttonAddAddress: CustomButton!
    @IBOutlet weak var labelNoAddressFound: CustomLabel!
    
    private var webService = LocationAPI()
    private var addressListRequest = AddressListRequest()
    private var makeDefaultAddressRequest = MakeDefaultAddressRequest()
    private var deleteAddressRequest = DeleteAddressRequest()
    var addressList: [AddressListResponseData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.labelNoAddressFound.isHidden = true
        self.table_view.isHidden = false
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view.rowHeight = UITableView.automaticDimension
        self.table_view.estimatedRowHeight = 44.0
        self.fetchAddressList()
        self.setup()
    }
    
    func setup() {
        
        self.labelNoAddressFound.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelNoAddressFound.text = Strings.noAddressFound
        self.labelNoAddressFound.textColor = UIColor.init(named: "HomeTextColor")
        
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.myAddressHeader
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
        
        self.buttonAddAddress.setNormalTitle(Strings.addAddressButtonTitle)
        self.buttonAddAddress.backgroundColor = UIColor(named: "ButtonColor")
        self.buttonAddAddress.cornerRadius = 10
        self.buttonAddAddress.setTitleColor(UIColor.init(named: "WhiteColor"), for: .normal)
        self.buttonAddAddress.setTitleFont(fontWeight: .medium, ofSize: 17.0)
    }
    
    func fetchAddressList() {
        self.dataStartedLoading()
        self.addressListRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.webService.addressList(addressListRequest: self.addressListRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.addressList = data
                
                DispatchQueue.main.async {
                    if self.addressList.count>0 {
                        self.labelNoAddressFound.isHidden = true
                        self.table_view.isHidden = false
                        self.table_view.reloadData()
                    }else{
                        self.labelNoAddressFound.isHidden = false
                        self.table_view.isHidden = true
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func makeDefault(addressID: String) {
        self.dataStartedLoading()
        self.makeDefaultAddressRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.makeDefaultAddressRequest.addressID = addressID
        self.makeDefaultAddressRequest.isDefault = true
        self.webService.makeDefault(makeDefaultAddressRequest: self.makeDefaultAddressRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.addressList = data
                
                DispatchQueue.main.async {
                    if self.addressList.count>0 {
                        self.labelNoAddressFound.isHidden = true
                        self.table_view.isHidden = false
                        self.table_view.reloadData()
                    }else{
                        self.labelNoAddressFound.isHidden = false
                        self.table_view.isHidden = true
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func deleteAddress(addressID: String) {
        self.dataStartedLoading()
        self.deleteAddressRequest.userID = UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.deleteAddressRequest.addressID = addressID
        self.webService.deleteAddress(deleteAddressRequest: self.deleteAddressRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.addressList = data
                
                DispatchQueue.main.async {
                    if self.addressList.count>0 {
                        self.labelNoAddressFound.isHidden = true
                        self.table_view.isHidden = false
                        self.table_view.reloadData()
                    }else{
                        self.labelNoAddressFound.isHidden = false
                        self.table_view.isHidden = true
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonAddAddressAction(_ sender: Any) {
        let addLocationView = AddLocationMainView.instantiateFromAppStoryboard(appStoryboard: .location)
        addLocationView.comingFrom = "AddressList"
        addLocationView.isAddAddress = true
        addLocationView.block = { (address, landmark, houseNo, lattitude, longitude, typeId, type, comingFrom) in
            
            self.fetchAddressList()
        }
        self.navigationController?.pushViewController(addLocationView, animated: true)
    }
    
}
extension AddressListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addListTableCell", for: indexPath) as! AddListTableCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.delegate = self
        cell.buttonDefault.tag = indexPath.row
        cell.buttonEdit.tag = indexPath.row
        cell.buttonDelete.tag = indexPath.row
        
        let type = self.addressList[indexPath.row].addressType?.type
        if type == Strings.addressTypeHome {
            cell.imageViewHomeIcon.image = UIImage(named: "home")
        }else if type == Strings.addressTypeWork {
            cell.imageViewHomeIcon.image = UIImage(named: "office_icon")
        }else {
            cell.imageViewHomeIcon.image = UIImage(named: "other_icon")
        }
        
        cell.labelAddressType.setTextFont(fontWeight: .medium, ofSize: 14)
        cell.labelAddressType.textColor = UIColor.init(named: "HomeTextColor")
        cell.labelAddressType.text = self.addressList[indexPath.row].addressType?.type
        
        cell.labelAddress.setTextFont(fontWeight: .light, ofSize: 12)
        cell.labelAddress.textColor = UIColor.init(named: "HomeTextLightColor")
        cell.labelAddress.text = "\(self.addressList[indexPath.row].flatOrHouseOrBuildingOrCompany ?? ""), \(self.addressList[indexPath.row].fullAddress ?? ""), \(self.addressList[indexPath.row].landmark ?? "")"
        cell.labelAddress.sizeToFit()
        
        cell.buttonEdit.setNormalTitle(Strings.editButton)
        cell.buttonEdit.setTitleFont(fontWeight: .medium, ofSize: 12)
        cell.buttonEdit.setTitleColor(UIColor.init(named: "HomeTextColor"), for: .normal)
        
        cell.buttonDelete.setNormalTitle(Strings.deleteButton)
        cell.buttonDelete.setTitleFont(fontWeight: .medium, ofSize: 12)
        cell.buttonDelete.setTitleColor(UIColor.init(named: "ButtonColor"), for: .normal)
        
        let isDefault = self.addressList[indexPath.row].isDefault
        if isDefault == false {
            cell.buttonDefault.setNormalTitle(Strings.setDefaultButton)
            cell.buttonDefault.backgroundColor = .white
            cell.buttonDefault.layer.borderColor = UIColor.init(named: "HomeTextColor")?.cgColor
            cell.buttonDefault.layer.borderWidth = 1.0
            cell.buttonDefault.setTitleFont(fontWeight: .medium, ofSize: 12)
            cell.buttonDefault.setTitleColor(UIColor.init(named: "HomeTextColor"), for: .normal)
            
            cell.buttonDelete.isHidden = false
        }else{
            cell.buttonDefault.setNormalTitle(Strings.defaultButton)
            cell.buttonDefault.backgroundColor = UIColor.init(named: "YellowColor")
            cell.buttonDefault.layer.borderColor = UIColor.clear.cgColor
            cell.buttonDefault.layer.borderWidth = 0.0
            cell.buttonDefault.setTitleFont(fontWeight: .medium, ofSize: 12)
            cell.buttonDefault.setTitleColor(.white, for: .normal)
            
            cell.buttonDelete.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension AddressListView: AddressListDelegate {
    func makeDefault(_ sender: UIButton) {
        let addressID = self.addressList[sender.tag].id ?? ""
        self.makeDefault(addressID: addressID)
    }
    
    func editAddress(_ sender: UIButton) {
        let addressID = self.addressList[sender.tag].id ?? ""
        
        var editAddressModel = EditAddressDataModel()
        editAddressModel.addressID = addressID
        editAddressModel.addressType = self.addressList[sender.tag].addressType?.type ?? "NA"
        editAddressModel.flatOrHouseOrBuildingOrCompany = self.addressList[sender.tag].flatOrHouseOrBuildingOrCompany ?? "NA"
        editAddressModel.fullAddress = self.addressList[sender.tag].fullAddress ?? "NA"
        editAddressModel.landmark = self.addressList[sender.tag].landmark ?? "NA"
        editAddressModel.latitude = "\(self.addressList[sender.tag].location?.coordinates?.last ?? 0.0)"
        editAddressModel.longitude = "\(self.addressList[sender.tag].location?.coordinates?.first ?? 0.0)"
        
        let addLocationView = AddLocationMainView.instantiateFromAppStoryboard(appStoryboard: .location)
        addLocationView.comingFrom = "AddressList"
        addLocationView.isAddAddress = false
        addLocationView.addressId = addressID
        addLocationView.editAddressDataModel = editAddressModel
        addLocationView.block = { (address, landmark, houseNo, lattitude, longitude, typeId, type, comingFrom) in
            
            self.fetchAddressList()
        }
        self.navigationController?.pushViewController(addLocationView, animated: true)
    }
    
    func deleteAddress(_ sender: UIButton) {
        DispatchQueue.main.async {
            let successAlert = UIAlertController(title: "Alert", message: "Do you want to remove this address?", preferredStyle: UIAlertController.Style.alert)
            
            successAlert.addAction(UIAlertAction(title: Strings.delete, style: .default) { (action:UIAlertAction!) in
                let addressID = self.addressList[sender.tag].id ?? ""
                self.deleteAddress(addressID: addressID)
            })
            
            successAlert.addAction(UIAlertAction(title: Strings.cancel, style: .default) { (action:UIAlertAction!) in
                
            })
            
            self.present(successAlert, animated: true)
        }        
    }
}
extension AddressListView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
}
