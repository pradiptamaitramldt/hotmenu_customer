//
//  AddListTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 17/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

protocol AddressListDelegate {
    func makeDefault(_ sender : UIButton)
    func editAddress(_ sender : UIButton)
    func deleteAddress(_ sender : UIButton)
}

class AddListTableCell: UITableViewCell {

    @IBOutlet weak var imageViewHomeIcon: UIImageView!
    @IBOutlet weak var labelAddressType: CustomLabel!
    @IBOutlet weak var buttonDefault: CustomButton!
    @IBOutlet weak var labelAddress: CustomLabel!
    @IBOutlet weak var buttonEdit: CustomButton!
    @IBOutlet weak var buttonDelete: CustomButton!
    
    var delegate : AddressListDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonDefaultAction(_ sender: Any) {
        self.delegate?.makeDefault(sender as! UIButton)
    }
    @IBAction func buttonEditAction(_ sender: Any) {
        self.delegate?.editAddress(sender as! UIButton)
    }
    @IBAction func buttonDeleteAction(_ sender: Any) {
        self.delegate?.deleteAddress(sender as! UIButton)
    }
    
}
