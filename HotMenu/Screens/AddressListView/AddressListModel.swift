//
//  AddressListModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 17/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct MakeDefaultAddressRequest {
    
    var userID: String
    var addressID: String
    var isDefault: Bool
            
    init() {
        self.userID = ""
        self.addressID = ""
        self.isDefault = true
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        tempDictionary["addressId"] = self.addressID
        tempDictionary["isDefault"] = self.isDefault
        
        return tempDictionary
    }
}

struct DeleteAddressRequest {
    
    var userID: String
    var addressID: String
            
    init() {
        self.userID = ""
        self.addressID = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.userID
        tempDictionary["addressId"] = self.addressID
        
        return tempDictionary
    }
}
