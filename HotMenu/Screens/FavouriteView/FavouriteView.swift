//
//  FavouriteView.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 22/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit
import AMShimmer

class FavouriteView: UIViewController {

    @IBOutlet weak var labelHeader: CustomLabel!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var labelNoDataFound: CustomLabel!
    
    private var webService = FavouriteMenuAPI()
    private var dashboardWebService = DashboardAPI()
    private var favouriteRequest = FavouriteMenuRequest()
    private var unFavouriteRequest = MakeFavouriteRequest()
    
    private var cartWebService = CartAPI()
    private var viewCartRequest = ViewCartRequest()
    private var addCartRequest = AddCartRequest()
    private var updateCartRequest = UpdateCartRequest()
    private var removeCartRequest = RemoveCartRequest()
    
    var favouriteMenuArray: [FavouriteList] = []
    var cart_id: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        
        self.labelNoDataFound.isHidden = true
        self.labelNoDataFound.setTextFont(fontWeight: .bold , ofSize: 25)
        self.labelNoDataFound.text = Strings.noFavouriteFound
        self.labelNoDataFound.textColor = UIColor.init(named: "HomeTextColor")
        DispatchQueue.main.async {
//            AMShimmer.start(for: self.table_view)
            
            self.table_view.startShimmeringAnimation()
        }
        
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.fetchFavouriteMenuList()
    }
    
    func setup() {
        self.labelHeader.setTextFont(fontWeight: .medium , ofSize: 18)
        self.labelHeader.text = Strings.favouriteHeader
        self.labelHeader.textColor = UIColor.init(named: "HomeTextColor")
    }
    
    func fetchFavouriteMenuList() {
        self.favouriteRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"
        self.favouriteRequest.lattitude = "\(Utility.lattitude)"
        self.favouriteRequest.longitude = "\(Utility.longitude)"
//        self.dataStartedLoading()
        self.webService.favouriteMenu(favouriteRequest: self.favouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
//            self.dataFinishedLoading()
            DispatchQueue.main.async {
                self.table_view.stopShimmeringAnimation()
            }
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.cart_id = data.cartID ?? ""
                self.favouriteMenuArray = data.list ?? []
                                
                DispatchQueue.main.async {
                    if self.favouriteMenuArray.count>0 {
                        self.table_view.isHidden = false
                        self.labelNoDataFound.isHidden = true
                        self.table_view.reloadData()
                    }else{
                        self.table_view.isHidden = true
                        self.labelNoDataFound.isHidden = false
                    }
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func makeUnfavourite(menuID: String) {
        self.unFavouriteRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.unFavouriteRequest.menuId = menuID
        self.dataStartedLoading()
        self.dashboardWebService.markUnfavourite(favouriteRequest: self.unFavouriteRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchFavouriteMenuList()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func addToCart(menuID: String, menuAmount: String, menuQuantity: String, vendorID: String) {
        self.addCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.addCartRequest.menuId = menuID
        self.addCartRequest.menuAmount = menuAmount
        self.addCartRequest.menuQuantity = menuQuantity
        self.addCartRequest.vendorId = vendorID
        self.dataStartedLoading()
        self.cartWebService.addToCart(addCartRequest: self.addCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                let statusCode = successDashboardResponse.statuscode
                if statusCode == 300 {
                    let message = successDashboardResponse.message
                    self.showErrorAlertWithTitle(title: Strings.blankString, message: message ?? "")
                }else if statusCode == 200 {
                    self.fetchFavouriteMenuList()
                }
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func updateCart(cartID: String, menuID: String, menuAmount: String, menuQuantity: String, vendorId: String) {
        self.updateCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.updateCartRequest.menuId = menuID
        self.updateCartRequest.menuAmount = menuAmount
        self.updateCartRequest.menuQuantity = menuQuantity
        self.updateCartRequest.cartId = cartID
        self.updateCartRequest.vendorId = vendorId
        
        self.dataStartedLoading()
        self.cartWebService.updateCart(updateCartRequest: self.updateCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchFavouriteMenuList()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }
    
    func removeCart(cartID: String, menuID: String, vendorId: String) {
        self.removeCartRequest.customerId = UserDefaultsManager.shared.getStringValue(forKey: .userId)//"5f3a9bde072031285d2e71d3"//UserDefaultsManager.shared.getStringValue(forKey: .userId)
        self.removeCartRequest.menuId = menuID
        self.removeCartRequest.cartId = cartID
        self.removeCartRequest.vendorId = vendorId
        
        self.dataStartedLoading()
        self.cartWebService.removeCart(removeCartRequest: self.removeCartRequest, imageData: nil) { (success, message, dashboardResponse, error) in
            self.dataFinishedLoading()
            if success {
                guard let successDashboardResponse = dashboardResponse, let data = successDashboardResponse.responseData else {
                    return
                }
                
                self.fetchFavouriteMenuList()
                
                Utility.log("Finally it's done")
            } else {
                self.showErrorAlertWithTitle(title: Strings.blankString, message: message)
            }
        }
    }

    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension FavouriteView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.favouriteMenuArray.count > 0 {
            return favouriteMenuArray.count
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteTableCell", for: indexPath) as! FavouriteTableCell
        
        if self.favouriteMenuArray.count > 0 {
            let itemImage = self.favouriteMenuArray[indexPath.row].menuID?.itemID?.menuImage ?? ""
            if itemImage != "" {
                cell.imageViewMenuItem.sd_setImage(with: URL(string: itemImage), placeholderImage: nil, options: .progressiveLoad, completed: nil)
            }
            
            cell.labelTitle.setTextFont(fontWeight: .medium, ofSize: 17)
            cell.labelTitle.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelDescription.setTextFont(fontWeight: .light, ofSize: 14)
            cell.labelDescription.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelActualPrice.setTextFont(fontWeight: .light, ofSize: 11)
            cell.labelActualPrice.textColor = UIColor.init(named: "HomeTextLightColor")
            
            cell.labelRating.setTextFont(fontWeight: .medium, ofSize: 12)
            cell.labelRating.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.labelDiscountedPrice.setTextFont(fontWeight: .regular, ofSize: 12)
            cell.labelDiscountedPrice.textColor = UIColor.init(named: "HomeTextColor")
            
            cell.imageViewMenuItem.contentMode = .scaleAspectFill
            cell.imageViewMenuItem.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
            cell.delegate = self
            cell.labelTitle.text = self.favouriteMenuArray[indexPath.row].menuID?.itemID?.itemName ?? "NA"
            cell.labelDescription.text = self.favouriteMenuArray[indexPath.row].menuID?.itemID?.description ?? "NA"
//            cell.labelActualPrice.text = "₹ \(self.favouriteMenuArray[indexPath.row].menuID?.itemID?.price ?? 0)"
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "₹ \(self.favouriteMenuArray[indexPath.row].menuID?.itemID?.price ?? 0)")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            
            cell.labelActualPrice.attributedText = attributeString
            
            let discountType = self.favouriteMenuArray[indexPath.row].menuID?.itemID?.discountType
            let price = self.favouriteMenuArray[indexPath.row].menuID?.itemID?.price ?? 0
            let discountPrice = self.favouriteMenuArray[indexPath.row].menuID?.itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            cell.labelDiscountedPrice.text = "₹ \(offerPrice)"
            
            let isVegItem = self.favouriteMenuArray[indexPath.row].menuID?.itemID?.type
            if isVegItem == "NON VEG" {
                cell.imageViewVeg.image = UIImage(named: "nonveg")
            }else{
                cell.imageViewVeg.image = UIImage(named: "veg")
            }
            cell.imageViewFavourite.image = UIImage(named: "heart_red")
            cell.labelRating.text = "\(self.favouriteMenuArray[indexPath.row].rating ?? 0.0)"
            cell.buttonFavourite.tag = indexPath.row
            cell.buttonPlus.tag = indexPath.row
            cell.buttonMinus.tag = indexPath.row
            cell.buttonAddCart.tag = indexPath.row
            
            let cartQuantity = self.favouriteMenuArray[indexPath.row].cartMenuQuantity
            if cartQuantity == 0 {
                cell.buttonAddCart.isHidden = false
                cell.viewIncreaseCart.isHidden = true
            }else{
                cell.buttonAddCart.isHidden = true
                cell.viewIncreaseCart.isHidden = false
                cell.labelCartCount.text = "\(cartQuantity ?? 0)"
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 320
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let favDish = self.favouriteMenuArray[indexPath.row]
        
        let discountType = favDish.menuID?.itemID?.discountType
        let price = favDish.menuID?.itemID?.price ?? 0
        let discountPrice = favDish.menuID?.itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let mealType = MealTypeID(id: favDish.menuID?.itemID?.mealTypeID?.id, type: favDish.menuID?.itemID?.mealTypeID?.type)
                
        let favItemID = ItemID(discountType: favDish.menuID?.itemID?.discountType, discountAmount: favDish.menuID?.itemID?.discountAmount, validityFrom: favDish.menuID?.itemID?.validityFrom, validityTo: favDish.menuID?.itemID?.validityTo, isActive: favDish.menuID?.itemID?.isActive ?? true, isApprove: favDish.menuID?.itemID?.isApprove ?? true, popularity: favDish.menuID?.itemID?.popularity, id: favDish.menuID?.itemID?.id, itemName: favDish.menuID?.itemID?.itemName, categoryID: favDish.menuID?.itemID?.categoryID, description: favDish.menuID?.itemID?.description, mealTypeID: mealType, type: favDish.menuID?.itemID?.type, price: favDish.menuID?.itemID?.price, waitingTime: favDish.menuID?.itemID?.waitingTime, menuImage: favDish.menuID?.itemID?.menuImage, itemOptions: [], createdAt: favDish.menuID?.itemID?.createdAt, updatedAt: favDish.menuID?.itemID?.updatedAt, v: favDish.menuID?.itemID?.v, ingredients: "", recipe: "")
        
        let favouriteDishes = TopDish(id: favDish.menuID?.id, itemID: favItemID, restaurantID: favDish.menuID?.restaurantID, createdAt: favDish.menuID?.createdAt, updatedAt: favDish.menuID?.updatedAt, v: favDish.menuID?.v, isFavorite: 1, review: favDish.review, rating: Int(favDish.rating ?? 0.0), cartMenuQuantity: favDish.cartMenuQuantity)
        
        let homeDetailView = HomeDetailsView.instantiateFromAppStoryboard(appStoryboard: .home)
        homeDetailView.menuItem = favouriteDishes
        homeDetailView.cartId = self.cart_id
        homeDetailView.totalCartCount = "\(Utility.cartCount)"
        self.navigationController?.pushViewController(homeDetailView, animated: true)
    }
}
extension FavouriteView: ViewControllerProtocol {
    
    func dataStartedLoading() {
        DispatchQueue.main.async {
            self.showLoader()
        }
    }
    
    func dataFinishedLoading() {
        DispatchQueue.main.async {
            self.hideLoader()
        }
    }
    
    func showErrorAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func showSuccessAlertWithTitle(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlertWithTitleOkAction(Strings.blankString, message: message, okTitle: Strings.ok, okAction: nil)
        }
    }
    
    func userUnauthorized() {
        
    }
    
    func userForbidden() {
        
    }
    
}
extension FavouriteView : TodaysMenuTableDelegate {
//    func updateTable() {
//        
//    }
    
    func markAsFavouriteTable(_ sender: UIButton) {
        let menuID = self.favouriteMenuArray[sender.tag].menuID?.id ?? ""
        if menuID != "" {
            self.makeUnfavourite(menuID: menuID)
        }else{
            self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
        }
    }
    
    func minusCartItemTable(_ sender: UIButton) {
        let menuID = self.favouriteMenuArray[sender.tag].menuID?.id ?? ""
        
        let discountType = self.favouriteMenuArray[sender.tag].menuID?.itemID?.discountType
        let price = self.favouriteMenuArray[sender.tag].menuID?.itemID?.price ?? 0
        let discountPrice = self.favouriteMenuArray[sender.tag].menuID?.itemID?.discountAmount ?? 0
        var offerPrice : Float = 0.0
        if discountType == "FLAT" {
            offerPrice = Float(price-discountPrice)
        }else if discountType == "NONE" {
            offerPrice = Float(price)
        }else{
            offerPrice = Float(price-((price*discountPrice)/100))
        }
        
        let menuAmount = "\(offerPrice)"
        let menuQuantity: Int = self.favouriteMenuArray[sender.tag].cartMenuQuantity ?? 0
        let vendorID: String = self.favouriteMenuArray[sender.tag].menuID?.restaurantID ?? ""
        
        if menuQuantity == 1 {
            self.removeCart(cartID: self.cart_id, menuID: menuID, vendorId: vendorID)
        }else{
            if menuQuantity > 1 {
                self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity-1)", vendorId: vendorID)
            }
        }
    }
    
    func plusCartItemTable(_ sender: UIButton) {
        if self.favouriteMenuArray[sender.tag].menuID?.itemID?.isActive == true {
            let menuID = self.favouriteMenuArray[sender.tag].menuID?.id ?? ""
            
            let discountType = self.favouriteMenuArray[sender.tag].menuID?.itemID?.discountType
            let price = self.favouriteMenuArray[sender.tag].menuID?.itemID?.price ?? 0
            let discountPrice = self.favouriteMenuArray[sender.tag].menuID?.itemID?.discountAmount ?? 0
            var offerPrice : Float = 0.0
            if discountType == "FLAT" {
                offerPrice = Float(price-discountPrice)
            }else if discountType == "NONE" {
                offerPrice = Float(price)
            }else{
                offerPrice = Float(price-((price*discountPrice)/100))
            }
            
            let menuAmount = "\(offerPrice)"
            let menuQuantity: Int = self.favouriteMenuArray[sender.tag].cartMenuQuantity ?? 0
            let vendorID: String = self.favouriteMenuArray[sender.tag].menuID?.restaurantID ?? ""
            
            self.updateCart(cartID: self.cart_id, menuID: menuID, menuAmount: menuAmount, menuQuantity: "\(menuQuantity+1)", vendorId: vendorID)
        }
        else {
             self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
        }
    }
    
    func addToCartItemTable(_ sender: UIButton) {
    
        if self.favouriteMenuArray[sender.tag].menuID?.itemID?.isActive == true {
            let menuID = self.favouriteMenuArray[sender.tag].menuID?.id ?? ""
                   
                   let discountType = self.favouriteMenuArray[sender.tag].menuID?.itemID?.discountType
                   let price = self.favouriteMenuArray[sender.tag].menuID?.itemID?.price ?? 0
                   let discountPrice = self.favouriteMenuArray[sender.tag].menuID?.itemID?.discountAmount ?? 0
                   var offerPrice : Float = 0.0
                   if discountType == "FLAT" {
                       offerPrice = Float(price-discountPrice)
                   }else if discountType == "NONE" {
                       offerPrice = Float(price)
                   }else{
                       offerPrice = Float(price-((price*discountPrice)/100))
                   }
                   
                   let menuAmount = "\(offerPrice)"
                   let vendorID = self.favouriteMenuArray[sender.tag].menuID?.restaurantID ?? ""
                   
                   self.addToCart(menuID: menuID, menuAmount: menuAmount, menuQuantity: "1", vendorID: vendorID)
        }
        else {
             self.showErrorAlertWithTitle(title: Strings.blankString, message: "ID of this item not fond")
        }
     }
}
