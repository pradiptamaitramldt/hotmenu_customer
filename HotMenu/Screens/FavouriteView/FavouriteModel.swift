//
//  FavouriteModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 22/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct FavouriteMenuRequest {
    
    var customerId: String
    var lattitude: String
    var longitude: String
    
    init() {
        self.customerId = ""
        self.lattitude = ""
        self.longitude = ""
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["customerId"] = self.customerId
        tempDictionary["latitude"] = self.lattitude
        tempDictionary ["longitude"] = self.longitude
        
        return tempDictionary
    }
}

// MARK: - FavouriteMenuListResponseModel
struct FavouriteMenuListResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: FavouriteListResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct FavouriteListResponseData: Codable {
    let cartID: String?
    let list: [FavouriteList]?

    enum CodingKeys: String, CodingKey {
        case cartID = "cartId"
        case list
    }
}

// MARK: - List
struct FavouriteList: Codable {
    let id: String?
    let menuID: FavouriteMenuID?
    let customerID, createdAt, updatedAt: String?
    let v, review: Int?
    let rating: Double?
    let cartMenuQuantity: Int?
    let vendorID: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case menuID = "menuId"
        case customerID = "customerId"
        case createdAt, updatedAt
        case v = "__v"
        case review, rating, cartMenuQuantity
        case vendorID = "vendorId"
    }
}

// MARK: - MenuID
struct FavouriteMenuID: Codable {
    let id: String?
    let itemID: ItemID?
    let restaurantID, createdAt, updatedAt: String?
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case itemID = "itemId"
        case restaurantID = "restaurantId"
        case createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - MealTypeID
struct FavouriteMealTypeID: Codable {
    let id, type: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type
    }
}
