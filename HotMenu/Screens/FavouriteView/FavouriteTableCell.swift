//
//  FavouriteTableCell.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 25/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import UIKit

class FavouriteTableCell: UITableViewCell {

    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var imageViewVeg: UIImageView!
    @IBOutlet weak var imageViewFavourite: UIImageView!
    @IBOutlet weak var labelRating: CustomLabel!
    @IBOutlet weak var labelTitle: CustomLabel!
    @IBOutlet weak var labelDescription: CustomLabel!
    @IBOutlet weak var labelActualPrice: CustomLabel!
    @IBOutlet weak var labelDiscountedPrice: CustomLabel!
    @IBOutlet weak var buttonAddCart: UIButton!
    @IBOutlet weak var viewIncreaseCart: UIView!
    @IBOutlet weak var labelCartCount: CustomLabel!
    @IBOutlet weak var imageViewLine: UIImageView!
    @IBOutlet weak var buttonFavourite: UIButton!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    
    var delegate : TodaysMenuTableDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonFavouriteAction(_ sender: Any) {
        self.delegate?.markAsFavouriteTable(sender as! UIButton)
    }
    @IBAction func buttonAddToCartItem(_ sender: Any) {
        self.delegate?.addToCartItemTable(sender as! UIButton)
    }
    @IBAction func buttonDecreaseItemCartAction(_ sender: Any) {
        self.delegate?.minusCartItemTable(sender as! UIButton)
    }
    @IBAction func buttonIncreaseItemCartAction(_ sender: Any) {
        self.delegate?.plusCartItemTable(sender as! UIButton)
    }

}
