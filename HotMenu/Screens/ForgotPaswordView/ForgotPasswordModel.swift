//
//  ForgotPasswordModel.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 14/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct ForgotPasswordRequest {
    
    var email: String
    var userType: String
    
    var delegate: ForgotPasswordRequestDelegate?
    
    init() {
        self.email = ""
        self.userType = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.email.isValidEmail() {
            delegate.passwordValidationPassed()
        }else{
            delegate.invalidPasswordData(message: ErrorMessages.invalidEmail)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["email"] = self.email
        tempDictionary["userType"] = self.userType
        
        return tempDictionary
    }
}

protocol ForgotPasswordRequestDelegate {
    
    func invalidPasswordData(message: String)
    
    func passwordValidationPassed()
}

// MARK: - ForgotPasswordResponseModel
struct ForgotPasswordResponseModel: Codable {
    var success: Bool
    var statuscode: Int
    var message: String?
    var responseData: ForgotPasswordResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
    
    init(from decoder: Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try values.decode(Bool.self, forKey: .success)
        self.statuscode = try values.decode(Int.self, forKey: .statuscode)
        self.message = try values.decode(String.self, forKey: .message)
        if let d = try? values.decode(ForgotPasswordResponseData.self, forKey: .responseData) {
            responseData = d
        }
    }
}

// MARK: - ResponseData
struct ForgotPasswordResponseData: Codable {
    let email, forgotPassOtp: String
}

struct ResendForgotOTPRequest {
    
    var email: String
    var userType: String
    
    var delegate: ForgotPasswordRequestDelegate?
    
    init() {
        self.email = ""
        self.userType = ""
    }
    
    func validateData() {
        guard let delegate = self.delegate else { return }
        if self.email.isValidEmail() {
            delegate.passwordValidationPassed()
        }else{
            delegate.invalidPasswordData(message: ErrorMessages.invalidEmail)
        }
    }
    
    func convertToDictionary() -> [String: Any] {
        var tempDictionary: [String: Any] = [:]
        
        tempDictionary["email"] = self.email
        tempDictionary["userType"] = self.userType
        
        return tempDictionary
    }
}
