//
//  RegistrationAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 12/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct RegistrationAPI {
    
    func register(registrationRequest: RegistrationRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: RegistrationResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.requestAPI(url: WebServiceConstants.registrationAPI, parameter: registrationRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateNormalHeader(), httpMethodType: .post, multipartData: imageData, multipartDataKeyName: "image") { (success, statusCode, response, error) in
                guard let data = response else {
                    return
                }
                if success {
                    JSONResponseDecoder.decodeFrom(data, returningModelType: RegistrationResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                        if parsingError == nil {
                            guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                            completion(true, successMessage, successResponse, nil)
                        } else {
                            completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                        }
                    })
                } else {
                    if error == .httpError {
                        completion(false, WebServiceError.httpError.localizedDescription, nil, .httpError)
                    } else if error == .forbidden {
                        completion(false, WebServiceError.forbidden.localizedDescription, nil, .forbidden)
                    } else {
                        JSONResponseDecoder.decodeFrom(data, returningModelType: ResponseError.self, completion: { (failedUploadProfilePicture, parsingError) in
                            if parsingError == nil {
                                guard let failedResponse = failedUploadProfilePicture, let failedMessage = failedResponse.message else { return }
                                completion(false, failedMessage, nil, nil)
                            } else {
                                completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                            }
                        })
                    }
                }
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func registerWithoutImage(registrationRequest: RegistrationRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: RegistrationResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.registrationAPI)!, params: registrationRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateNormalHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: RegistrationResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}
