//
//  LocationAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 11/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct LocationAPI {
    func addressList(addressListRequest: AddressListRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: AddressListResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.addressListAPI)!, params: addressListRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: AddressListResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func googlePlaces(googleAPI: String, completion: @escaping(_ success: Bool, _ message: String, _ data: GooglePlaceResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.requestAPI(url: googleAPI, parameter: nil, httpHeader: WebServiceHeaderGenerator.generateNormalHeader(), httpMethodType: .get, multipartData: nil, multipartDataKeyName: "") { (success, statusCode, response, error) in
                guard let data = response else {
                    return
                }
                if success {
                    JSONResponseDecoder.decodeFrom(data, returningModelType: GooglePlaceResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                        if parsingError == nil {
                            guard let successResponse = successUploadProfilePicture else { return }
                            completion(true, "", successResponse, nil)
                        } else {
                            completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                        }
                    })
                } else {
                    if error == .httpError {
                        completion(false, WebServiceError.httpError.localizedDescription, nil, .httpError)
                    } else if error == .forbidden {
                        completion(false, WebServiceError.forbidden.localizedDescription, nil, .forbidden)
                    } else {
                        JSONResponseDecoder.decodeFrom(data, returningModelType: ResponseError.self, completion: { (failedUploadProfilePicture, parsingError) in
                            if parsingError == nil {
                                guard let failedResponse = failedUploadProfilePicture, let failedMessage = failedResponse.message else { return }
                                completion(false, failedMessage, nil, nil)
                            } else {
                                completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                            }
                        })
                    }
                }
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func addressTypes(addressTypeRequest: AddressTypeRequest, completion: @escaping(_ success: Bool, _ message: String, _ data: AddressTypesResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.addressTypeAPI)!, params: addressTypeRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: AddressTypesResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func addAddress(addAddressRequest: AddAddressRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: AddAddressResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.addAddressAPI)!, params: addAddressRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: AddAddressResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func makeDefault(makeDefaultAddressRequest: MakeDefaultAddressRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: AddressListResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.addressMakeDefaultAPI)!, params: makeDefaultAddressRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: AddressListResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func deleteAddress(deleteAddressRequest: DeleteAddressRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: AddressListResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.deleteAddressAPI)!, params: deleteAddressRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: AddressListResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func editAddress(editAddressRequest: EditAddressRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: AddressListResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.editAddressAPI)!, params: editAddressRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: AddressListResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}
