//
//  LoginAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 14/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct LoginAPI {
    
    func login(loginRequest: LoginRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: LoginResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.loginAPI)!, params: loginRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateNormalHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: LoginResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func loginWithSocial(socialLoginRequest: RegistrationRequest, completion: @escaping(_ statuscode: Int,_ success: Bool, _ message: String, _ data: RegistrationResponseModel?, _ error: WebServiceError?) -> Void) {

        if Reachability.isConnectedToNetwork() {
            WebServiceManager.shared.requestAPI(url: WebServiceConstants.socialLoginAPI, httpHeader: WebServiceHeaderGenerator.generateNormalHeader(), parameter: socialLoginRequest.convertToDictionary(), httpMethodType: .post) { (success, statusCode, data, error) in
                guard let data = data else {
                    return
                }
                if success {
                    JSONResponseDecoder.decodeFrom(data, returningModelType: RegistrationResponseModel.self, completion: { (successSocialLoginResponse, parsingError) in
                        if parsingError == nil {
                            guard let successResponse = successSocialLoginResponse, let successMessage = successResponse.message else { return }
                            completion(statusCode!, true, successMessage, successResponse, nil)
                        } else {
                            completion(statusCode!, false, ErrorMessages.dataParsingError, nil, .dataParsingError)
                        }
                    })
                } else {
                    if error != .httpError {
                        JSONResponseDecoder.decodeFrom(data, returningModelType: ResponseError.self, completion: { (failedSocialLoginResponse, parsingError) in
                            if parsingError == nil {
                                guard let failedResponse = failedSocialLoginResponse, let failedMessage = failedResponse.message else { return }
                                if statusCode == 401 {
                                    completion(statusCode!, true, failedMessage, nil, error)
                                }else{
                                    completion(statusCode!, false, failedMessage, nil, error)
                                }
                            } else {
                                completion(statusCode!, false, ErrorMessages.dataParsingError, nil, .dataParsingError)
                            }
                        })
                    } else {
                        completion(statusCode!, false, WebServiceError.httpError.localizedDescription, nil, error)
                    }
                }
            }
        } else {
            completion(100, false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}
