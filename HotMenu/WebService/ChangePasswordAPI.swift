//
//  ChangePasswordAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 21/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct ChangePasswordAPI {
    func changePassword(changePasswordRequest: ChangePasswordRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: ChangePasswordResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.changePasswordAPI)!, params: changePasswordRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: ChangePasswordResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}

