//
//  OTPVerifyAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 14/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct OTPVerifyAPI {
    
    func otpVerify(otpVerifyRequest: OTPVerifyRequest, completion: @escaping(_ success: Bool, _ message: String, _ data: OTPVerifyResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.verifyUserAPI)!, params: otpVerifyRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateNormalHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: OTPVerifyResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func resendOTPVerify(otpVerifyRequest: ResendOTPVerifyRequest, completion: @escaping(_ success: Bool, _ message: String, _ data: ResendOTPVerifyResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.resendOTP)!, params: otpVerifyRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateNormalHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: ResendOTPVerifyResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}
