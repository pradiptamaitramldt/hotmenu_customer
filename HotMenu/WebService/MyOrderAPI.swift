//
//  MyOrderAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 18/09/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct MyOrderAPI {
    func myOrderList(myOrderRequest: MyOrderRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: MyOrderResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.orderListAPI)!, params: myOrderRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: MyOrderResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func trackOrder(trackOrderRequest: TrackOrderRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: TrackOrderResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.trackOrderAPI)!, params: trackOrderRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: TrackOrderResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func cancelOrder(trackOrderRequest: TrackOrderRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: CancelOrderResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.cancelOrderAPI)!, params: trackOrderRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: CancelOrderResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func orderSubmit(orderSubmitRequest: OrderSubmitRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: CancelOrderResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.orderSubmitAPI)!, params: orderSubmitRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: CancelOrderResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}

