//
//  FavouriteMenuAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 22/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct FavouriteMenuAPI {
    func favouriteMenu(favouriteRequest: FavouriteMenuRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: FavouriteMenuListResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.favouriteMenuList)!, params: favouriteRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: FavouriteMenuListResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}
