//
//  WebServiceManager.swift
//  Passenger
//
//  Created by Pradipta Maitra on 22/06/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation
import CoreLocation

enum WebServiceError: Error {
    case noInternetConnection
    case unauthorized
    case unprocessableEntity
    case forbidden
    case stripeClientTokenGenerationError
    case httpError
    case nonCategorized
    case dataParsingError
    case serverError
}

extension WebServiceError: LocalizedError {
    
    var errorDescription: String? {
        switch self {
        case .noInternetConnection:
            return NSLocalizedString("No internet connection", comment: "Reachability Error")
        case .unauthorized:
            return NSLocalizedString("Authentication Error", comment: "Authentication Error")
        case .unprocessableEntity:
            return NSLocalizedString("Unprocessable Entity", comment: "Unprocessable Entity")
        case .forbidden:
            return NSLocalizedString("Forbidden", comment: "Forbidden")
        case .stripeClientTokenGenerationError:
            return NSLocalizedString("StripeClientTokenGenerationError", comment: "StripeClientTokenGenerationError")
        case .httpError:
            return NSLocalizedString("HTTP Error", comment: "HTTP Error")
        case .nonCategorized:
            return NSLocalizedString("Some Error Occured", comment: "Some Error Occured")
        case .dataParsingError:
            return NSLocalizedString("Data Parsing Error", comment: "Data Parsing Error")
        case .serverError:
            return NSLocalizedString("Server Error", comment: "Server Error")
        }
    }
}

struct WebServiceConstants {
    
    private static let baseURL = "https://nodeserver.mydevfactory.com:3480/api/customer/"
    
    static let imageBaseURL = "https://mydevfactory.com/~laravel/zoomxoom/public/"
    
    private static let carMakeModelDBURL = "https://carmakemodeldb.com/api/v1/car-lists/get/all/"
    
    private static let paymentGatewayURL = "https://sandbox.gotnpgateway.com/api/"
    
    static var createCustomer: String {
        return self.paymentGatewayURL + "customer"
    }
    
    static var loginAPI: String {
        return self.baseURL + "login"
    }
    
    static var registrationAPI: String {
        return self.baseURL + "register"
    }
    
    static var socialLoginAPI: String {
        return self.baseURL + "login"
    }
    
    static var addPhoneAPI: String {
        return self.baseURL + "mobile"
    }
    
    static var forgotPasswordAPI: String {
        return self.baseURL + "forgotPassword"
    }
    
    static var resetPasswordAPI: String {
        return self.baseURL + "resetPassword"
    }
    
    static var getProfileAPI: String {
        return self.baseURL + "viewProfile"
    }
    
    static var saveProfileAPI: String {
        return self.baseURL + "editProfile"
    }
    
    static var changePasswordAPI: String {
        return self.baseURL + "changePassword"
    }
    
    static var favouriteMenuList: String {
        return self.baseURL + "favoriteMenuList"
    }
    
    static var viewCartList: String {
        return self.baseURL + "cartList"
    }
    
    static var addToCart: String {
        return self.baseURL + "addToCart"
    }
    
    static var updateCart: String {
        return self.baseURL + "updateCartItem"
    }
    
    static var removeCart: String {
        return self.baseURL + "removeCartItem"
    }
    
    static var categoryWiseMenuList: String {
        return self.baseURL + "categoryWiseMenu"
    }
    
    static var foodSearchAPI: String {
        return self.baseURL + "searchMenu"
    }
    
    static var addressListAPI: String {
        return self.baseURL + "addressList"
    }
    
    static var addAddressAPI: String {
        return self.baseURL + "addAddress"
    }
    
    static var addressTypeAPI: String {
        return self.baseURL + "addressType"
    }
    
    static var addressMakeDefaultAPI: String {
        return self.baseURL + "addressMarkedAsDefault"
    }
    
    static var deleteAddressAPI: String {
        return self.baseURL + "addressDelete"
    }
    
    static var editAddressAPI: String {
        return self.baseURL + "addressEdit"
    }
    
    static var orderListAPI: String {
        return self.baseURL + "orderList"
    }
    
    static var trackOrderAPI: String {
        return self.baseURL + "trackOrder"
    }
    
    static var cancelOrderAPI: String {
        return self.baseURL + "cancelledOrder"
    }
    
    static var orderSubmitAPI: String {
        return self.baseURL + "orderSubmit"
    }
    
    static var profilePictureUploadAPI: String {
        return self.baseURL + "profileImageUpload"
    }
    
    static var viewRatingAPI: String {
        return self.baseURL + "ratingList"
    }
    
    static var addRatingAPI: String {
        return self.baseURL + "addRating"
    }
    
    static var onboardingProfilePictureUploadAPI: String {
        return self.baseURL + "profile/image/update"
    }
    
    static var markFavouriteAPI: String {
        return self.baseURL + "markAsFavorite"
    }
    
    static var markUnfavouriteAPI: String {
        return self.baseURL + "markAsUnFavorite"
    }
    
    static var verifyUserAPI: String {
        return self.baseURL + "verifyUser"
    }
    
    static var resendOTP: String {
        return self.baseURL + "sendVerificationCode"
    }
    
    static var dashboardAPI: String {
        return self.baseURL + "dashboard"
    }
    
    static var rideNow: String {
        return self.baseURL + "request/ride"
    }
    
    static var serviceTypesAPI: String {
        return self.baseURL + "get/service/list"
    }
    
    static var vehicleModelAPI: String {
        return self.baseURL + "get/model/list"
    }
    
    static var getDriverBackgroundAPI: String {
        return self.baseURL + "background/data"
    }
    
    /*
    static var checkSocialAPI: String {
        return self.baseURL + "driver/social/login"
    }
    
    static var sendEmailOTPAPI: String {
        return self.baseURL + "driver/forgot/password"
    }
    
    static var serviceTypesAPI: String {
        return self.baseURL + "service/type"
    }
    
    static var vehicleMakesAPI: String {
        return self.carMakeModelDBURL + "makes?api_token=" + AppConstant.carMakeModelDBAPIKey
    }
    
    static var vehicleModelsAPI: String {
        return self.carMakeModelDBURL + "models/"
    }
    
    static var driverProfileCompletionPercentageAPI: String {
        return self.baseURL + "driver/profile/completion/percentage"
    }
    
    static var passengerRegistrationAsDriverAPI: String {
        return self.baseURL + "driver/passenger/signup"
    }
    
    static var driverBidPointPackagesAPI: String {
        return self.baseURL + "driver/packages"
    }
    
    static var purchaseBidPointPackageAPI: String {
        return self.baseURL + "driver/buy/packages"
    }
    
    static var addCardAPI: String {
        return self.baseURL + "driver/add/card"
    }
    
    static var cardListAPI: String {
        return self.baseURL + "driver/cards"
    }
    
    static var deleteCardAPI: String {
        return self.baseURL + "driver/cards"
    }
    
    static var setDefaultCardAPI: String {
        return self.baseURL + "driver/cards"
    }
    
    static var uploadProfilePictureAPI: String {
        return self.baseURL + "driver/profile/image/update"
    }
    
    static var carImagesAPI: String {
        return self.baseURL + "driver/car/image"
    }
    
    static var purchasedBidPointsPackageAPI: String {
        return self.baseURL + "driver/bought/packages"
    }
    
    static var changeDriverStatusAPI: String {
        return self.baseURL + "driver/toggle/online/status"
    }
    
    static var placeYourBidAPI: String {
        return self.baseURL + "driver/place/bid"
    }
    
    static var submitRatingReviewAPI: String {
        return self.baseURL + "driver/submit/rating/remarks"
    }
    
    static func pastBookingAPI(nextPageUrl: String?) -> String {
        let url = (nextPageUrl == nil) ? (self.baseURL + "driver/completed/ride?page=1") : nextPageUrl
        return url ?? ""
    }
    
    static func upcomingBookingAPI(nextPageUrl: String?) -> String {
        let url = (nextPageUrl == nil) ? (self.baseURL + "driver/shedule/ride?page=1") : nextPageUrl
        return url ?? ""
    }
    
    static var cancelUpcomingBookingAPI: String {
        return self.baseURL + "driver/cancel/request"
    }
    
    static var getBookingDetailsAPI: String {
        return self.baseURL + "driver/ride/details"
    }
    
    static func myBidsAPI(nextPageUrl: String?) -> String {
        let url = (nextPageUrl == nil) ? (self.baseURL + "driver/my/bid?page=1") : nextPageUrl
        return url ?? ""
    }
    
    static func myEarningAPI(nextPageUrl: String?) -> String {
        let url = (nextPageUrl == nil) ? (self.baseURL + "driver/total/earning?page=1") : nextPageUrl
        return url ?? ""
    }
    
    static var reportIssueAPI: String {
        return self.baseURL + "driver/report/issue"
    }
    */
    
//    static func getDirectionUrl(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) -> String {
//        let sourceLat = "\(source.latitude)"
//        let sourceLong = "\(source.longitude)"
//        let destinationLat = "\(destination.latitude)"
//        let destinationLong = "\(destination.longitude)"
//        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLat),\(sourceLong)&destination=\(destinationLat),\(destinationLong)&mode=driving&key=\(Strings.googleMapsAPIKey)"
//        return urlString
//    }
//
//    static func getGoogleMapsStaticAPIUrl(source: CLLocationCoordinate2D?, destination: CLLocationCoordinate2D?) -> String {
//        guard let source = source, let destination = destination else { return "" }
//        let sourceLatLongString = "\(source.latitude),\(source.longitude)"
//        let destinationLatLongString = "\(destination.latitude),\(destination.longitude)"
//        let urlString = "http://maps.googleapis.com/maps/api/staticmap?path=color:0x0000ff|weight:5|\(sourceLatLongString)|\(destinationLatLongString)&size=320x60&scale=1&key=\(Strings.googleMapsAPIKey)"
//        Utility.log(urlString)
//        return urlString
//    }
    
}

enum HTTPMethodType: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

/// Responsible for generating common headers for requests.
struct WebServiceHeaderGenerator {
    /// Generates common headers specific to APIs. Can also accept additional headers if demanded by specific APIs.
    ///
    /// - Returns: A configured header JSON dictionary which includes both common and additional params.
    static func generateHeader() -> [String: String] {
        var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
        var headerDict = [String: String]()
        headerDict["Content-Type"] = "application/json"
        headerDict["Accept"] = "application/json"
        headerDict["Version"] = Strings.appVersion
        headerDict["timeZone"] = localTimeZoneAbbreviation
        return headerDict
    }
    
    static func generatePaymentHeader() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["Content-Type"] = "application/json"
        headerDict["Authorization"] = Strings.paymentGatewayAPIKey
        return headerDict
    }
    
    static func generateNormalHeader() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["Content-Type"] = "application/x-www-form-urlencoded"
        headerDict["Accept"] = "application/json"
        return headerDict
    }
    
    static func generateAuthorizedHeader() -> [String: String] {
        var headerDict = [String: String]()
        let accessToken = "\(UserDefaultsManager.shared.getStringValue(forKey: .acccessToken))"
        headerDict["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0IjoiNWYzYTliZGUwNzIwMzEyODVkMmU3MWQzIiwidXNlciI6IkNVU1RPTUVSIiwiaWF0IjoxNjAwMjQzMzExLCJleHAiOjE0NTYwMjQzMzExfQ.UIIr-9BXc106To9jSpf7K0svF6PyUwQ7h1Khoss7WsM"//"Bearer \(accessToken)"
        headerDict["Content-Type"] = "application/x-www-form-urlencoded"
        headerDict["Accept"] = "application/json"
        return headerDict
    }
    
    
    static func generateMultipartAuthorizedHeader() -> [String: String] {
        var headerDict = [String: String]()
        let accessToken = "\(UserDefaultsManager.shared.getStringValue(forKey: .acccessToken))"
        //define the multipart request type
        headerDict["Content-Type"] = "application/x-www-form-urlencoded"
        headerDict["Accept"] = "application/json"
        headerDict["Authorization"] = "Bearer \(accessToken)"//"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0IjoiNWYzYTliZGUwNzIwMzEyODVkMmU3MWQzIiwidXNlciI6IkNVU1RPTUVSIiwiaWF0IjoxNTk3NzMzNTQ2LCJleHAiOjE0NTU3NzMzNTQ2fQ.H3JKcO8U6IalB--oYVVXwB3otK3wfS_izPPLkpA-e9k"//"Bearer \(accessToken)"
        return headerDict
    }
    /*
    private static func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    */
}

class WebServiceManager: NSObject {
    
    static let shared = WebServiceManager()
    
    typealias WebServiceCompletionBlock = (_ success: Bool,_ statusCode: Int?,_ responseData: Data?,_ error: WebServiceError?) -> Void
    
    func requestAPI(url: String, httpHeader: [String: String], parameter: [String: Any]?, httpMethodType: HTTPMethodType, completion: @escaping WebServiceCompletionBlock) {
        
        let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var request = URLRequest(url: URL(string: escapedAddress!)!)
        request.httpMethod = httpMethodType.rawValue
        request.allHTTPHeaderFields = httpHeader
        
        Utility.log("URL: \(url)")
        Utility.log("Header: \(httpHeader)")
        Utility.log("Parameter: ")
        Utility.log(parameter)
        
        if parameter != nil {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameter as Any, options: [])
            } catch let error {
                Utility.log(error.localizedDescription)
                return
            }
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                //http error
                completion(false, nil, data, .httpError)
                return
            } else {
                //if httpMethodType != .get {
                    Utility.log("Response: \(String(decoding: data ?? Data(), as: UTF8.self))")
                //}
                guard let httpResponse = response as? HTTPURLResponse else {
                    completion(false, nil, data, .httpError)
                    return
                }
                if (200...299).contains(httpResponse.statusCode) {
                    //all ok, check for data and then proceed
                    completion(true, httpResponse.statusCode, data, nil)
                } else if httpResponse.statusCode == 401 {
                    //unauthorized
                    completion(false, httpResponse.statusCode, data, .unauthorized)
                } else if httpResponse.statusCode == 403 {
                    //forbidden
                    completion(false, httpResponse.statusCode, data, .forbidden)
                } else {
                    //handle other error
                    //success = false ; because there is no http error but server or data processing error is there; need to handle this error in respective calling classes
                    completion(false, httpResponse.statusCode, data, nil)
                }
            }
        }
        task.resume()
    }
    
    //Function with Data
    /*
    func requestAPI(url: String, httpHeader: [String: String], parameter: Data?, httpMethodType: HTTPMethodType, completion: @escaping WebServiceCompletionBlock) {
        
        let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var request = URLRequest(url: URL(string: escapedAddress!)!)
        request.httpMethod = httpMethodType.rawValue
        request.allHTTPHeaderFields = httpHeader
        
        Utility.log("URL: \(url)")
        Utility.log("Header: \(httpHeader)")
        Utility.log("Parameter: ")
        Utility.log(parameter)
        
        if parameter != nil {
            request.httpBody = parameter
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(nil, error)
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                Utility.log("Error in fetching response")
                completion(nil, nil)
            }
            Utility.log("Response: \(String(decoding: data, as: UTF8.self))")
            completion(data, nil)
        }
        task.resume()
    }
    */
    
    func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }

    func callPost(url:URL, params:[String:Any], httpHeader: [String: String], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeader
        
        Utility.log("URL: \(url)")
        Utility.log("Header: \(httpHeader)")
        Utility.log("Parameter: ")
        Utility.log(params)
        
        let postString = self.getPostString(params: params)
        request.httpBody = postString.data(using: .utf8)

        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }

            finish(result)
        }
        task.resume()
    }
     
    //Multipart data send function
    func requestAPI(url: String, parameter: [String: Any]?, httpHeader: [String: String], httpMethodType: HTTPMethodType, multipartData: Data? = nil, multipartDataKeyName: String = "file", completion: @escaping WebServiceCompletionBlock) {
        
        let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var request = URLRequest(url: URL(string: escapedAddress!)!)
        request.httpMethod = httpMethodType.rawValue
        request.allHTTPHeaderFields = httpHeader
        
        Utility.log("URL: \(url)")
        Utility.log("Header: \(httpHeader)")
        Utility.log("Parameter: ")
        Utility.log(parameter)
        
//        if parameter != nil {
//            do {
//                request.httpBody = try JSONSerialization.data(withJSONObject: parameter as Any, options: [])
//            } catch let error {
//                Utility.log(error.localizedDescription)
//                return
//            }
//        }
        let body = NSMutableData()
        let boundary = generateBoundaryString()
        if parameter != nil {
            for (key, value) in parameter! {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                //let val = "\(value)".addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: ""))!
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        
        if( multipartData != nil ) {
            
            //define the multipart request type
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let fname = "file.jpg"
            let mimetype = "image/jpeg"
            
            //define the data post parameter
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"\(multipartDataKeyName)\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(multipartData!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        }
        request.httpBody = body as Data
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                //http error
                completion(false, nil, data, .httpError)
                return
            } else {
                if httpMethodType != .get {
                    Utility.log("Response: \(String(decoding: data ?? Data(), as: UTF8.self))")
                }
                guard let httpResponse = response as? HTTPURLResponse else {
                    completion(false, nil, data, .httpError)
                    return
                }
                if (200...299).contains(httpResponse.statusCode) {
                    //all ok, check for data and then proceed
                    completion(true, httpResponse.statusCode, data, nil)
                } else if httpResponse.statusCode == 401 {
                    //unauthorized
                    completion(false, httpResponse.statusCode, data, WebServiceError.unauthorized)
                } else if httpResponse.statusCode == 403 {
                    //forbidden
                    completion(false, httpResponse.statusCode, data, WebServiceError.forbidden)
                } else {
                    //handle other error
                    //success = false ; because there is no http error but server or data processing error is there; need to handle this error in respective calling classes
                    completion(false, httpResponse.statusCode, data, .serverError)
                }
            }
        }
        task.resume()
    }
    
    private func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
}
