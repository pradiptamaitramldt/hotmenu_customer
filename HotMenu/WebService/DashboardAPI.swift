//
//  DashboardAPI.swift
//  HotMenu
//
//  Created by Pradipta Maitra on 20/08/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

import Foundation

struct DashboardAPI {
    
    func fetchDashboardData(dashboardRequest: DashboardRequest, completion: @escaping(_ success: Bool, _ message: String, _ data: DashboardResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.dashboardAPI)!, params: dashboardRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: DashboardResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func markFavourite(favouriteRequest: MakeFavouriteRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: FavouriteResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.markFavouriteAPI)!, params: favouriteRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: FavouriteResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
    
    func markUnfavourite(favouriteRequest: MakeFavouriteRequest, imageData: Data?, completion: @escaping(_ success: Bool, _ message: String, _ data: UnfavouriteResponseModel?, _ error: WebServiceError?) -> Void) {
        
        if Reachability.isConnectedToNetwork() {
            
            WebServiceManager.shared.callPost(url: URL(string: WebServiceConstants.markUnfavouriteAPI)!, params: favouriteRequest.convertToDictionary(), httpHeader: WebServiceHeaderGenerator.generateMultipartAuthorizedHeader()) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: UnfavouriteResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(true, successMessage, successResponse, nil)
                    } else {
                        completion(false, WebServiceError.dataParsingError.localizedDescription, nil, .dataParsingError)
                    }
                })
            }
        } else {
            completion(false, WebServiceError.noInternetConnection.localizedDescription, nil, WebServiceError.noInternetConnection)
        }
    }
}
