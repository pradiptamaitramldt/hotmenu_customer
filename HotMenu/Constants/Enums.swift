//
//  Enums.swift
//  Driver
//
//  Created by Pallab on 05/05/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

enum FontWeight {
    case bold, light, medium, regular ,semibold, thin
}
